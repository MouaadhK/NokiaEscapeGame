#include <string.h>
#include <cstdlib>
#include <iostream>

#include <md5.hpp>

int main(int argc, char** argv) {


    if (argc == 2) {
        const char* BYTES = argv[1];

        md5::md5_t md5;
        md5.process(BYTES, strlen(BYTES));
        md5.finish();

	    char str[MD5_STRING_SIZE];

	    md5.get_string(str);
        std::cout << std::string(str) << std::endl;
        return 0;
    } 
    else {
        std::cout << "Bad parameters : Please use " << argv[0] << " <sentence>" << std::endl;
    }
}
