#!/bin/bash
set -e
PROJECT_BASE_DIR="./"
# cd ${PROJECT_BASE_DIR}
find "." -name "*.o" | xargs rm -f
OPTIONS="-w"
INCLUDES="-I ."
GCC="g++"
$GCC $OPTIONS ${INCLUDES} -c guessanumber.cpp
# echo "$GCC $OPTIONS ${INCLUDES} -c guessanumber.cpp."
$GCC $OPTIONS ${INCLUDES} -o encrypt *.o
