#!/bin/bash
set -e
PROJECT_BASE_DIR="./"
# cd ${PROJECT_BASE_DIR}
find "." -name "*.o" | xargs rm -f
OPTIONS="-w"
INCLUDES="-I ."
GCC="g++"
$GCC $OPTIONS ${INCLUDES} -c main.cpp
# echo "$GCC $OPTIONS ${INCLUDES} -c md5.cpp main.cpp."
$GCC $OPTIONS ${INCLUDES} -o encrypt *.o
