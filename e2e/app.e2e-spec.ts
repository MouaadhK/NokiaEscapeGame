import { EscapeGameClientPage } from './app.po';

describe('escape-game-client App', () => {
  let page: EscapeGameClientPage;

  beforeEach(() => {
    page = new EscapeGameClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
