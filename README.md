# Running the app
Here’s the list of commands to execute, in the repo’s directory:
- npm install (you should see some warnings but it should work fine)
- ng build --prod --aot false
- tsc
- node deploy/server/index.js
