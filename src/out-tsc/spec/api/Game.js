"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Time = /** @class */ (function () {
    function Time(minutes, seconds) {
        this.minutes = 0;
        this.seconds = 0;
        var secondsCycle = 60;
        this.minutes = minutes + Math.floor(seconds / secondsCycle);
        this.seconds = (seconds % secondsCycle + secondsCycle) % secondsCycle;
    }
    Time.prototype.toString = function () {
        var formattedMinutes = "";
        var formattedSeconds = "";
        formattedMinutes = this.minutes < 10 ? '0' + this.minutes : '' + this.minutes;
        formattedSeconds = this.seconds < 10 ? '0' + this.seconds : '' + this.seconds;
        return formattedMinutes + ":" + formattedSeconds;
    };
    Time.decrementByOneSecond = function (time) {
        time.seconds--;
        if (time.seconds == -1) {
            time.seconds = 59;
            time.minutes--;
        }
    };
    Time.prototype.valueOf = function () {
        return (this.minutes * 60 + this.seconds);
    };
    Time.prototype.substract = function (time1, time2) {
        return new Time(time1.minutes - time2.minutes, time1.seconds - time2.seconds);
        //time1.minutes > time2.minutes ?  
    };
    return Time;
}());
exports.Time = Time;
;
var Game = /** @class */ (function () {
    function Game() {
        this.name = "";
        this.id = "";
        this.description = "";
        this.status = "";
        this.maxPlayerCount = 0;
        this.currentPlayerCount = 0;
        this.isJoinable = false;
        this.isModeratorConnected = false;
    }
    return Game;
}());
exports.Game = Game;
;
var GameTypes;
(function (GameTypes) {
    GameTypes["Regular"] = "Regular";
    GameTypes["Junior"] = "Junior";
})(GameTypes = exports.GameTypes || (exports.GameTypes = {}));
var UserType;
(function (UserType) {
    UserType[UserType["moderator"] = 0] = "moderator";
    UserType[UserType["player"] = 1] = "player";
})(UserType = exports.UserType || (exports.UserType = {}));
var User = /** @class */ (function () {
    function User() {
        return {
            name: "",
            email: "",
            id: "",
            type: UserType.player
        };
    }
    return User;
}());
exports.User = User;
var ChatMessage = /** @class */ (function () {
    function ChatMessage() {
        this.gameId = "";
        this.content = "";
    }
    return ChatMessage;
}());
exports.ChatMessage = ChatMessage;
var Code = /** @class */ (function () {
    function Code() {
        this.gameId = "";
        this.value = "";
    }
    return Code;
}());
exports.Code = Code;
var CompilationResult = /** @class */ (function () {
    function CompilationResult() {
        this.gameId = "";
        this.binaryWasCreated = false;
        this.stdout = "";
        this.stderr = "";
        this.gameId = "";
        this.player = new User();
        this.binaryWasCreated = false;
        this.stderr = "";
        this.stdout = "";
    }
    return CompilationResult;
}());
exports.CompilationResult = CompilationResult;
var ExecutionLog = /** @class */ (function () {
    function ExecutionLog() {
        this.gameId = "";
        this.stderr = "";
        this.stdout = "";
    }
    return ExecutionLog;
}());
exports.ExecutionLog = ExecutionLog;
var QuizAnswer = /** @class */ (function () {
    function QuizAnswer() {
        this.answer = "";
        this.isCorrect = false;
        this.answer = "";
        this.isCorrect = false;
    }
    return QuizAnswer;
}());
exports.QuizAnswer = QuizAnswer;
var QuizQuestion = /** @class */ (function () {
    function QuizQuestion() {
        this.question = "";
        this.question = "";
        this.answers = [];
    }
    return QuizQuestion;
}());
exports.QuizQuestion = QuizQuestion;
var QuizPopupAssignment = /** @class */ (function () {
    function QuizPopupAssignment() {
    }
    return QuizPopupAssignment;
}());
exports.QuizPopupAssignment = QuizPopupAssignment;
var QuizPopupType;
(function (QuizPopupType) {
    QuizPopupType["Regular"] = "regular";
    QuizPopupType["Junior"] = "junior";
})(QuizPopupType = exports.QuizPopupType || (exports.QuizPopupType = {}));
var SokobanLevelDifficulty;
(function (SokobanLevelDifficulty) {
    SokobanLevelDifficulty["Easy"] = "Easy";
    SokobanLevelDifficulty["Medium"] = "Medium";
    SokobanLevelDifficulty["Hard"] = "Hard";
    SokobanLevelDifficulty["Expert"] = "Expert";
})(SokobanLevelDifficulty = exports.SokobanLevelDifficulty || (exports.SokobanLevelDifficulty = {}));
var SokobanLevel = /** @class */ (function () {
    function SokobanLevel() {
        this.name = "";
        this.game = "";
        this.passphraseSegment = "";
        this.index = 0;
    }
    return SokobanLevel;
}());
exports.SokobanLevel = SokobanLevel;
var FubukiLevelDifficulty;
(function (FubukiLevelDifficulty) {
    FubukiLevelDifficulty["Easy"] = "Easy";
    FubukiLevelDifficulty["Medium"] = "Medium";
    FubukiLevelDifficulty["Hard"] = "Hard";
    FubukiLevelDifficulty["Expert"] = "Expert";
})(FubukiLevelDifficulty = exports.FubukiLevelDifficulty || (exports.FubukiLevelDifficulty = {}));
var FubukiLevel = /** @class */ (function () {
    function FubukiLevel() {
        this.name = "";
        this.game = 1;
        this.passphraseSegment = "";
        this.index = 0;
    }
    return FubukiLevel;
}());
exports.FubukiLevel = FubukiLevel;
var PlayerStatus = /** @class */ (function () {
    function PlayerStatus() {
        this.x = 0;
        this.y = 0;
        this.name = "";
        this.color = "";
    }
    return PlayerStatus;
}());
exports.PlayerStatus = PlayerStatus;
var CheckType = /** @class */ (function () {
    function CheckType() {
        this.passphrase = "";
        this.player = new User();
        this.gameId = "";
    }
    return CheckType;
}());
exports.CheckType = CheckType;
//# sourceMappingURL=Game.js.map