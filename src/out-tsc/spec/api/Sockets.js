"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketEvent = {
    getAvailableGames: "getAvailableGames",
    createNewGame: "createNewGame",
    joinGameAsPlayerReq: "joinGameAsPlayerReq",
    joinGameAsPlayerResp: "joinGameAsPlayerResp",
    availableGames: "availableGames",
    joinGameAsModerator: "joinGameAsModerator",
    updateGameInfo: "updateGameInfo",
    getPlayerListInGame: "getPlayerListInGame",
    playerList: "updatePlayerList",
    newChatMessage: "newChatMessage",
    getCodeFileContent: "getCodeFileContent",
    codeFileContent: "codeFileContent",
    compileCode: "compileCode",
    compilationResult: "compilationResult",
    runCode: "runCode",
    executionLog: "executionLog",
    startOrResumeGame: "startOrResumeGame",
    gameStarted: "gameStarted",
    pauseGame: "pauseGame",
    gamePaused: "gamePaused",
    abandonGame: "abandonGame",
    timeSync: "timeSync",
    getQuizPopups: "getQuizPopups",
    updateQuizPopups: "updateQuizPopups",
    quizPopups: "quizPopups",
    triggerQuizPopup: "triggerQuizPopup",
    gameEnded: "gameEnded",
    sokobanLevelComplete: "sokobanLevelComplete",
    sokobanLevelCompletePlayer: "sokobanLevelCompletePlayer",
    sokobanLevelCompleteModerator: "sokobanLevelCompleteModerator",
    newPlayerForModerator: "newPlayerForModerator",
    leavingPlayerForModerator: "leavingPlayerForModerator",
    checkPassphrase: "checkPassphrase",
    wrongPassphrase: "wrongPassphrase",
    startIntroForEveryone: "startIntroForEveryone"
};
var SocketData;
(function (SocketData) {
    var JoinGameAsPlayerRespData = /** @class */ (function () {
        function JoinGameAsPlayerRespData() {
            this.gameJoined = false;
            this.passphraseMask = "";
        }
        return JoinGameAsPlayerRespData;
    }());
    SocketData.JoinGameAsPlayerRespData = JoinGameAsPlayerRespData;
    var JoinGameAsPlayerReqData = /** @class */ (function () {
        function JoinGameAsPlayerReqData() {
            this.gameId = "";
        }
        return JoinGameAsPlayerReqData;
    }());
    SocketData.JoinGameAsPlayerReqData = JoinGameAsPlayerReqData;
    var GameEndData = /** @class */ (function () {
        function GameEndData() {
            this.success = false;
        }
        return GameEndData;
    }());
    SocketData.GameEndData = GameEndData;
    var SokobanLevelData = /** @class */ (function () {
        function SokobanLevelData() {
            this.gameId = "";
            this.index = 0;
        }
        return SokobanLevelData;
    }());
    SocketData.SokobanLevelData = SokobanLevelData;
})(SocketData = exports.SocketData || (exports.SocketData = {}));
//# sourceMappingURL=Sockets.js.map