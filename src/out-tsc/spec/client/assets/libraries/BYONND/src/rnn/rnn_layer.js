"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var point_1 = require("../point");
var connectorArrow_1 = require("../connector/connectorArrow");
var connectorArrowDash_1 = require("../connector/connectorArrowDash");
var orientation_1 = require("../orientation");
var RNN_Layer = /** @class */ (function () {
    function RNN_Layer(size) {
        this.size = size;
        this.parentLayer = null;
        this.neurons = new Array();
        this.y = 150;
        this.connectors = new Array();
    }
    RNN_Layer.prototype.getRadius = function () {
        return this.neurons[0].radius;
    };
    RNN_Layer.prototype.setRadius = function (newRadius) {
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].radius = newRadius;
        }
    };
    RNN_Layer.prototype.getX = function () {
        return this.neurons[0].x;
    };
    RNN_Layer.prototype.getY = function () {
        return this.neurons[0].y;
    };
    RNN_Layer.prototype.getNbNeurons = function () {
        return this.size;
    };
    RNN_Layer.prototype.display = function (canvas, neuralNetwork) {
        for (var i = 0; i < this.size; i++) {
            this.neurons[i].display(canvas);
        }
    };
    RNN_Layer.prototype.createConnectors = function (indexStep, indexLayer, neuralNetwork) {
        if (this.connectors.length === 0) {
            if (indexLayer > 0) {
                switch (neuralNetwork.orientation) {
                    case orientation_1.Orientation.HORIZONTAL:
                        this.connectors.push(new connectorArrow_1.ConnectorArrow(new point_1.Point(this.parentLayer.neurons[this.parentLayer.neurons.length - 1].x, this.parentLayer.neurons[this.parentLayer.neurons.length - 1].y + this.parentLayer.getRadius()), new point_1.Point(this.neurons[0].x, this.getY() - this.getRadius())));
                        if (indexLayer < neuralNetwork.nbLayers - 1 && indexStep < neuralNetwork.nbSteps - 1) {
                            var next_nn = neuralNetwork.steps[indexStep + 1][indexLayer];
                            this.connectors.push(new connectorArrow_1.ConnectorArrow(new point_1.Point(this.neurons[this.neurons.length - 1].x + this.getRadius(), this.neurons[this.neurons.length - 1].y), new point_1.Point(next_nn.neurons[next_nn.neurons.length - 1].x - this.getRadius(), next_nn.neurons[next_nn.neurons.length - 1].y)));
                        }
                        break;
                    case orientation_1.Orientation.VERTICAL:
                        this.connectors.push(new connectorArrow_1.ConnectorArrow(new point_1.Point(this.parentLayer.neurons[this.parentLayer.neurons.length - 1].x + this.parentLayer.getRadius(), this.parentLayer.neurons[this.parentLayer.neurons.length - 1].y), new point_1.Point(this.neurons[this.neurons.length - 1].x - this.getRadius(), this.neurons[this.neurons.length - 1].y)));
                        if (indexStep < neuralNetwork.nbSteps - 1 && indexLayer < neuralNetwork.nbLayers - 1) {
                            var next_nn = neuralNetwork.steps[indexStep + 1][indexLayer];
                            this.connectors.push(new connectorArrow_1.ConnectorArrow(new point_1.Point(this.neurons[0].x, this.neurons[0].y + this.getRadius()), new point_1.Point(next_nn.neurons[0].x, next_nn.neurons[0].y - next_nn.getRadius())));
                        }
                        break;
                }
            }
            if (indexLayer === neuralNetwork.nbLayers - 1 && indexStep < neuralNetwork.nbSteps - 1) {
                var cur_nn = neuralNetwork.steps[indexStep][indexLayer];
                var next_nn = neuralNetwork.steps[indexStep + 1][0];
                switch (neuralNetwork.orientation) {
                    case orientation_1.Orientation.HORIZONTAL:
                        this.connectors.push(new connectorArrowDash_1.ConnectorArrowDash(new point_1.Point(cur_nn.neurons[0].x + cur_nn.getRadius() / 3, cur_nn.getY() - cur_nn.getRadius()), new point_1.Point(next_nn.neurons[next_nn.neurons.length - 1].x - next_nn.getRadius() / 3, next_nn.neurons[next_nn.neurons.length - 1].y + next_nn.getRadius())));
                        break;
                    case orientation_1.Orientation.VERTICAL:
                        this.connectors.push(new connectorArrowDash_1.ConnectorArrowDash(new point_1.Point(cur_nn.neurons[0].x - cur_nn.getRadius(), cur_nn.getY() + cur_nn.getRadius()), new point_1.Point(next_nn.neurons[next_nn.neurons.length - 1].x + next_nn.getRadius(), next_nn.neurons[next_nn.neurons.length - 1].y - next_nn.getRadius())));
                        break;
                }
            }
        }
    };
    RNN_Layer.prototype.activateNeuron = function (index) {
        index = index % this.neurons.length;
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
        this.neurons[index].activate();
    };
    RNN_Layer.prototype.disable = function () {
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
    };
    return RNN_Layer;
}());
exports.RNN_Layer = RNN_Layer;
//# sourceMappingURL=rnn_layer.js.map