"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mlp_1 = require("./mlp/mlp");
var cnn_1 = require("./cnn/cnn");
var rnn_1 = require("./rnn/rnn");
var fubuki_1 = require("./fubuki/fubuki");
var NeuralNetworkBuilder = /** @class */ (function () {
    function NeuralNetworkBuilder(canvas) {
        this.canvas = canvas;
    }
    NeuralNetworkBuilder.prototype.build = function (type) {
        switch (type) {
            case "mlp":
                return new mlp_1.MLP(this.canvas);
                break;
            case "cnn":
                return new cnn_1.CNN(this.canvas);
                break;
            case "rnn":
                return new rnn_1.RNN(this.canvas);
                break;
            case "fubuki":
                return new fubuki_1.Fubuki(this.canvas);
                break;
            default:
                return null;
                break;
        }
    };
    return NeuralNetworkBuilder;
}());
exports.NeuralNetworkBuilder = NeuralNetworkBuilder;
//# sourceMappingURL=neuralNeworkBuilder.js.map