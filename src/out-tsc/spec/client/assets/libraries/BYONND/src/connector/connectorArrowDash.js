"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var connector_1 = require("./connector");
var ConnectorArrowDash = /** @class */ (function (_super) {
    __extends(ConnectorArrowDash, _super);
    function ConnectorArrowDash(p1, p2) {
        return _super.call(this, p1, p2) || this;
    }
    // Override
    ConnectorArrowDash.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        context.save();
        context.beginPath();
        var headlen = 10;
        var angle = Math.atan2(this.p2.y - this.p1.y, this.p2.x - this.p1.x);
        context.setLineDash([5, 15]);
        context.moveTo(this.p1.x, this.p1.y);
        context.lineTo(this.p2.x, this.p2.y);
        context.stroke();
        context.restore();
        context.save();
        context.beginPath();
        context.moveTo(this.p2.x, this.p2.y);
        context.lineTo(this.p2.x - headlen * Math.cos(angle - Math.PI / 6), this.p2.y - headlen * Math.sin(angle - Math.PI / 6));
        context.moveTo(this.p2.x, this.p2.y);
        context.lineTo(this.p2.x - headlen * Math.cos(angle + Math.PI / 6), this.p2.y - headlen * Math.sin(angle + Math.PI / 6));
        context.stroke();
        context.restore();
    };
    return ConnectorArrowDash;
}(connector_1.Connector));
exports.ConnectorArrowDash = ConnectorArrowDash;
//# sourceMappingURL=connectorArrowDash.js.map