"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Quadrilateral = /** @class */ (function () {
    function Quadrilateral(p1, p2, p4, p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }
    Quadrilateral.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        context.beginPath();
        context.lineWidth = 1;
        context.moveTo(this.p1.x, this.p1.y);
        context.lineTo(this.p2.x, this.p2.y);
        context.lineTo(this.p3.x, this.p3.y);
        context.lineTo(this.p4.x, this.p4.y);
        context.lineTo(this.p1.x, this.p1.y);
        context.strokeStyle = "#124191";
        context.stroke();
        context.fillStyle = "#239DF9";
        context.fill();
    };
    return Quadrilateral;
}());
exports.Quadrilateral = Quadrilateral;
//# sourceMappingURL=quadrilateral.js.map