"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var neuron_1 = require("../neuron");
var rnn_layer_1 = require("./rnn_layer");
var neuralNetwork_1 = require("../neuralNetwork");
var orientation_1 = require("../orientation");
var RNN = /** @class */ (function (_super) {
    __extends(RNN, _super);
    function RNN(canvas) {
        var _this = _super.call(this) || this;
        _this.canvas = canvas;
        _this.steps = new Array();
        _this.layers = new Array();
        _this.orientation = orientation_1.Orientation.HORIZONTAL;
        _this.error = "";
        return _this;
    }
    // Override
    RNN.prototype.init = function () {
        this.initLayers(this.canvas);
        this.createConnectors();
    };
    // Override
    RNN.prototype.addLayer = function (nbNeurons) {
        try {
            var layer = new rnn_layer_1.RNN_Layer(nbNeurons);
            this.layers.push(layer);
        }
        catch (_a) {
            this.error = "Error: can't add an empty layer";
        }
    };
    // Override
    RNN.prototype.define = function (nbSteps, nbLayers, serie, orientation) {
        this.nbLayers = nbLayers + 2;
        this.contracted = nbSteps > 15;
        this.nbSteps = this.isContracted() ? 15 : nbSteps;
        for (var i = 0; i < this.nbLayers; i++) {
            this.addLayer(3);
        }
        switch (orientation.toLowerCase()) {
            case "horizontal":
                this.orientation = orientation_1.Orientation.HORIZONTAL;
                break;
            case "vertical":
                this.orientation = orientation_1.Orientation.VERTICAL;
                break;
        }
        this.init();
        this.serie = serie;
    };
    // Override
    RNN.prototype.draw = function () {
        var context = this.canvas.getContext("2d");
        context.save();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.display();
    };
    RNN.prototype.initLayers = function (canvas) {
        var honrizontalConnectorSize = 50;
        var gap_step = 4 + honrizontalConnectorSize;
        var max_radius = 30;
        var min_radius = 1;
        var displaySize = this.nbSteps;
        var canvas_width = canvas.width;
        var neuron_radius = canvas_width / ((displaySize * 2)) - ((gap_step) / 2) - 1;
        neuron_radius = neuron_radius > max_radius ? max_radius : neuron_radius;
        neuron_radius = neuron_radius < min_radius ? min_radius : neuron_radius;
        var free_space = canvas_width - displaySize * (neuron_radius * 2 + gap_step);
        for (var i = 0; i < this.nbSteps; i++) {
            for (var j = 0; j < this.layers.length; j++) {
                if (this.layers[j].neurons.length === 0) {
                    if (j > 0) {
                        this.layers[j].parentLayer = this.layers[j - 1];
                        this.layers[j].y = this.layers[j].parentLayer.y + 120;
                    }
                    var gap_layer = 0;
                    for (var k = 0; k < this.layers[j].size; k++) {
                        switch (this.orientation) {
                            case orientation_1.Orientation.HORIZONTAL:
                                this.layers[j].neurons.push(new neuron_1.Neuron(free_space / 2 + neuron_radius + (i * neuron_radius * 2) + gap_step - 30, this.layers[j].y + gap_layer, neuron_radius, false, ""));
                                break;
                            case orientation_1.Orientation.VERTICAL:
                                this.layers[j].neurons.push(new neuron_1.Neuron(this.layers[j].y, free_space / 2 + neuron_radius + (i * neuron_radius * 2) + gap_step + gap_layer - 30, neuron_radius, false, ""));
                                break;
                        }
                        gap_layer += neuron_radius / 3;
                    }
                }
            }
            gap_step += 4 + honrizontalConnectorSize;
            this.steps.push(this.layers);
            this.layers = new Array();
            for (var i_1 = 0; i_1 < this.nbLayers; i_1++) {
                this.addLayer(3);
            }
        }
    };
    RNN.prototype.createConnectors = function () {
        for (var i = 0; i < this.nbSteps; i++) {
            for (var j = 0; j < this.layers.length; j++) {
                this.steps[i][j].createConnectors(i, j, this);
            }
        }
    };
    RNN.prototype.displaySteps = function () {
        for (var i = 0; i < this.nbSteps; i++) {
            for (var j = 0; j < this.layers.length; j++) {
                this.steps[i][j].display(this.canvas, this);
            }
        }
    };
    // Override
    RNN.prototype.display = function () {
        var context = this.canvas.getContext("2d");
        for (var i = 0; i < this.nbSteps; i++) {
            for (var j = 0; j < this.steps[i].length; j++) {
                for (var k = 0; k < this.steps[i][j].connectors.length; k++) {
                    this.steps[i][j].connectors[k].display(this.canvas);
                }
            }
        }
        this.displaySteps();
        if (this.serie != null) {
            if (this.nbSteps + 1 === this.serie.length) {
                switch (this.orientation) {
                    case orientation_1.Orientation.HORIZONTAL:
                        for (var i = 0; i < this.nbSteps; i++) {
                            context.font = "30px Arial";
                            context.fillText(this.serie[i], this.steps[i][0].neurons[0].x - 10, this.steps[i][0].neurons[0].y - 40);
                            context.fillText("t=" + i, this.steps[i][0].neurons[0].x - 20, this.steps[i][0].neurons[0].y - 90);
                        }
                        for (var i = 1; i <= this.nbSteps; i++) {
                            context.font = "30px Arial";
                            context.fillText(this.serie[i], this.steps[i - 1][0].neurons[0].x - 10, this.steps[i - 1][this.nbLayers - 1].neurons[0].y + 80);
                        }
                        break;
                    case orientation_1.Orientation.VERTICAL:
                        for (var i = 0; i < this.nbSteps; i++) {
                            context.font = "30px Arial";
                            var lastNeuron = this.steps[i][0].neurons[this.steps[i][0].neurons.length - 1];
                            context.fillText(this.serie[i], lastNeuron.x - 60, lastNeuron.y);
                            context.fillText("t=" + i, lastNeuron.x - 120, lastNeuron.y);
                        }
                        for (var i = 1; i <= this.nbSteps; i++) {
                            context.font = "30px Arial";
                            var lastNeuron = this.steps[i - 1][this.nbLayers - 1].neurons[this.steps[i - 1][this.nbLayers - 1].neurons.length - 1];
                            context.fillText(this.serie[i], lastNeuron.x + 60, lastNeuron.y);
                        }
                        break;
                }
            }
            else {
                if (this.isContracted()) {
                    console.error("Can't display serie data in a contracted view (serie length > 15)");
                }
                else {
                    console.error("The (nb steps+1) and the serie length must be equal");
                }
            }
        }
    };
    // Override
    RNN.prototype.getNbNeurons = function () {
        var nbNeurons = 0;
        for (var i = 0; i < this.layers.length; i++) {
            nbNeurons += this.layers[i].getNbNeurons();
        }
        return nbNeurons;
    };
    RNN.prototype.activateLayerNeuron = function (layerIndex, neuronIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].activateNeuron(neuronIndex);
    };
    RNN.prototype.activateOutputNeuron = function (neuronIndex) {
        this.layers[this.layers.length - 1].activateNeuron(neuronIndex);
    };
    RNN.prototype.disableLayer = function (layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].disable();
    };
    // Override
    RNN.prototype.getNbLayers = function () {
        return this.layers.length;
    };
    // Override
    RNN.prototype.isContracted = function () {
        return this.contracted;
    };
    // Override
    RNN.prototype.getError = function () {
        return this.error;
    };
    return RNN;
}(neuralNetwork_1.NeuralNetwork));
exports.RNN = RNN;
//# sourceMappingURL=rnn.js.map