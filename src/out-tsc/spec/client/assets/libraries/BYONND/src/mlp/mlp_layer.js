"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var point_1 = require("../point");
var connectorSimple_1 = require("../connector/connectorSimple");
var MLP_Layer = /** @class */ (function () {
    function MLP_Layer(size) {
        this.size = size;
        this.contracted = this.size > 60 ? true : false;
        this.parentLayer = null;
        this.neurons = new Array();
        this.y = 100;
        this.connectors = new Array();
    }
    MLP_Layer.prototype.getRadius = function () {
        return this.neurons[0].radius;
    };
    MLP_Layer.prototype.setRadius = function (newRadius) {
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].radius = newRadius;
        }
    };
    MLP_Layer.prototype.getX = function () {
        return this.neurons[0].x;
    };
    MLP_Layer.prototype.getY = function () {
        return this.neurons[0].y;
    };
    MLP_Layer.prototype.getNbNeurons = function () {
        return this.size;
    };
    MLP_Layer.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        if (!this.contracted) {
            for (var i = 0; i < this.size; i++) {
                this.neurons[i].display(canvas);
            }
        }
        else {
            var canvas_width = canvas.width;
            for (var i = 0; i < this.neurons.length; i++) {
                this.neurons[i].display(canvas);
            }
            context.font = "30px Arial";
            var text = ". . . . . " + this.size + " neurons . . . . .";
            context.fillText(text, canvas_width / 2 - text.length * 6, this.getY() + this.getRadius() / 2);
        }
    };
    MLP_Layer.prototype.createConnectors = function () {
        if (this.connectors.length === 0) {
            if (!this.contracted && !this.parentLayer.contracted) {
                this.connectors = Array();
                for (var i = 0; i < this.parentLayer.size; i++) {
                    for (var j = 0; j < this.size; j++) {
                        this.connectors.push(new connectorSimple_1.ConnectorSimple(new point_1.Point(this.neurons[j].x, this.getY() - this.getRadius()), new point_1.Point(this.parentLayer.neurons[i].x, this.parentLayer.getY() + this.parentLayer.getRadius())));
                    }
                }
            }
            else {
                for (var i = 0; i < 11; i++) {
                    for (var j = 0; j < 11; j++) {
                        this.connectors.push(new connectorSimple_1.ConnectorSimple(new point_1.Point(50 + i * 101, this.getY() - this.getRadius() - 5), new point_1.Point(50 + j * 101, this.parentLayer.getY() - this.parentLayer.getRadius() + 35)));
                    }
                }
            }
        }
    };
    MLP_Layer.prototype.activateNextNeuron = function () {
        if (!this.contracted) {
            var neuronIndex = -1;
            var i = 0;
            var found = false;
            while (i < this.neurons.length && !found) {
                if (this.neurons[i].active) {
                    neuronIndex = i;
                    found = true;
                }
                i++;
            }
            if (neuronIndex === -1) {
                this.neurons[0].activate();
                return true;
            }
            else {
                this.neurons[neuronIndex].disable();
                if (neuronIndex === this.neurons.length - 1) {
                    this.neurons[0].activate();
                    return false;
                }
                else {
                    this.neurons[neuronIndex + 1].activate();
                    return true;
                }
            }
        }
        return true;
    };
    MLP_Layer.prototype.activateNeuron = function (index) {
        index = index % this.neurons.length;
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
        this.neurons[index].activate();
    };
    MLP_Layer.prototype.disable = function () {
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
    };
    return MLP_Layer;
}());
exports.MLP_Layer = MLP_Layer;
//# sourceMappingURL=mlp_layer.js.map