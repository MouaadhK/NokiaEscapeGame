"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Orientation;
(function (Orientation) {
    Orientation["HORIZONTAL"] = "Horizontal";
    Orientation["VERTICAL"] = "Vertical";
})(Orientation = exports.Orientation || (exports.Orientation = {}));
//# sourceMappingURL=orientation.js.map