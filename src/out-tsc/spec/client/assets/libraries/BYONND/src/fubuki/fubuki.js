"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var neuron_1 = require("../neuron");
var fubuki_layer_1 = require("./fubuki_layer");
var neuralNetwork_1 = require("../neuralNetwork");
var Fubuki = /** @class */ (function (_super) {
    __extends(Fubuki, _super);
    function Fubuki(canvas) {
        var _this = _super.call(this) || this;
        _this.canvas = canvas;
        _this.layers = new Array();
        _this.error = "";
        return _this;
    }
    // Override
    Fubuki.prototype.init = function () {
        this.initLayers(this.canvas);
        this.balanceRadius();
        this.createConnectors();
    };
    // Override
    Fubuki.prototype.addLayer = function (nbNeurons) {
        try {
            var layer = new fubuki_layer_1.Fubuki_layer(nbNeurons);
            this.layers.push(layer);
            this.init();
        }
        catch (_a) {
            this.error = "Error: can't add an empty layer";
        }
    };
    // Override
    Fubuki.prototype.define = function (nbNeurons) {
        for (var i = 0; i < nbNeurons.length; i++) {
            this.addLayer(nbNeurons[i]);
        }
    };
    // Override
    Fubuki.prototype.draw = function () {
        var context = this.canvas.getContext("2d");
        context.save();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.display();
    };
    Fubuki.prototype.initLayers = function (canvas) {
        for (var j = 0; j < this.layers.length; j++) {
            if (this.layers[j].neurons.length === 0) {
                if (j > 0) {
                    this.layers[j].parentLayer = this.layers[j - 1];
                    this.layers[j].y = this.layers[j].parentLayer.y + 150;
                }
                var max_radius = 30;
                var min_radius = 1;
                var displaySize = this.layers[j].contracted ? max_radius : this.layers[j].size;
                var canvas_width = canvas.width;
                var gap = 0;
                var neuron_radius = canvas_width / ((displaySize * 2)) - (gap / 2) - 1;
                neuron_radius = neuron_radius > max_radius ? max_radius : neuron_radius;
                neuron_radius = neuron_radius < min_radius ? min_radius : neuron_radius;
                for (var i = 0; i < displaySize; i++) {
                    this.layers[j].neurons.push(new neuron_1.Neuron(100 + neuron_radius + (i * neuron_radius * 2) + gap, this.layers[j].y, neuron_radius, false, ""));
                    gap += 150;
                }
            }
        }
    };
    Fubuki.prototype.balanceRadius = function () {
        var min_radius = this.layers[0].getRadius();
        for (var i = 1; i < this.layers.length; i++) {
            if (min_radius > this.layers[i].getRadius()) {
                min_radius = this.layers[i].getRadius();
            }
        }
        for (var i = 0; i < this.layers.length; i++) {
            this.layers[i].setRadius(min_radius);
        }
    };
    Fubuki.prototype.createConnectors = function () {
        for (var i = 0; i < this.layers.length; i++) {
            this.layers[i].createConnectors();
        }
    };
    // Override
    Fubuki.prototype.display = function () {
        for (var i = 0; i < this.layers.length; i++) {
            for (var j = 0; j < this.layers[i].connectors.length; j++) {
                this.layers[i].connectors[j].display(this.canvas);
            }
        }
        for (var i = 0; i < this.layers.length; i++) {
            this.layers[i].display(this.canvas);
        }
    };
    // Override
    Fubuki.prototype.getNbNeurons = function () {
        var nbNeurons = 0;
        for (var i = 0; i < this.layers.length; i++) {
            nbNeurons += this.layers[i].getNbNeurons();
        }
        return nbNeurons;
    };
    Fubuki.prototype.activateLayerNextNeuron = function (layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        return this.layers[layerIndex].activateNextNeuron();
    };
    Fubuki.prototype.activateLayerNeuron = function (layerIndex, neuronIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].activateNeuron(neuronIndex);
    };
    Fubuki.prototype.activateOutputNeuron = function (neuronIndex) {
        this.layers[this.layers.length - 1].activateNeuron(neuronIndex);
    };
    Fubuki.prototype.disableLayer = function (layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].disable();
    };
    // Override
    Fubuki.prototype.getNbLayers = function () {
        return this.layers.length;
    };
    // Override
    Fubuki.prototype.isContracted = function () {
        for (var i = 0; i < this.layers.length; i++) {
            if (this.layers[i].contracted) {
                return true;
            }
        }
        return false;
    };
    // Override
    Fubuki.prototype.getError = function () {
        return this.error;
    };
    return Fubuki;
}(neuralNetwork_1.NeuralNetwork));
exports.Fubuki = Fubuki;
//# sourceMappingURL=fubuki.js.map