"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var connectorSimple_1 = require("../connector/connectorSimple");
var point_1 = require("../point");
var quadrilateral_1 = require("../quadrilateral");
var CNN_Layer = /** @class */ (function () {
    function CNN_Layer(size_x, size_y, nbChannels) {
        this.size_x = size_x;
        this.size_y = size_y == null ? 1 : size_y;
        this.nbChannels = nbChannels == null ? 0 : nbChannels;
        this.isLeaf = this.size_y === 1 && this.nbChannels === 0 ? true : false;
        this.contracted = (this.size_x > 60 ||
            this.size_y > 60) ? true : false;
        this.parentLayer = null;
        this.neurons = new Array();
        this.y = 100;
        this.connectors = new Array();
    }
    CNN_Layer.prototype.createConnectors = function () {
        if (this.connectors.length === 0) {
            this.connectors = Array();
            if (this.size_y > 1 || this.parentLayer.size_y > 1) {
                for (var i = 0; i < 3; i++) {
                    for (var j = 0; j < 3; j++) {
                        this.connectors.push(new connectorSimple_1.ConnectorSimple(new point_1.Point((this.pos_x + (this.width / 2)), (this.pos_y + this.height / 2)), new point_1.Point((this.parentLayer.pos_x + (this.parentLayer.width / 2) - this.parentLayer.width / 4 + this.parentLayer.width / 4 * i), (this.parentLayer.y + this.parentLayer.height / 2) - this.parentLayer.height / 4 + this.parentLayer.height / 4 * j)));
                    }
                }
            }
            else {
                if (this.parentLayer.getNbNeurons() > 25 || this.getNbNeurons() > 25) {
                    for (var i = 0; i < 11; i++) {
                        for (var j = 0; j < 11; j++) {
                            this.connectors.push(new connectorSimple_1.ConnectorSimple(new point_1.Point(50 + i * 95, this.getY() - this.getRadius() - 5), new point_1.Point(50 + j * 95, this.parentLayer.getY() - this.parentLayer.getRadius() + 35)));
                        }
                    }
                }
                else {
                    for (var i = 0; i < this.parentLayer.getNbNeurons(); i++) {
                        for (var j = 0; j < this.getNbNeurons(); j++) {
                            this.connectors.push(new connectorSimple_1.ConnectorSimple(new point_1.Point(this.neurons[0][j].x, this.getY() - this.getRadius()), new point_1.Point(this.parentLayer.neurons[0][i].x, this.parentLayer.getY() + this.parentLayer.getRadius())));
                        }
                    }
                }
            }
        }
    };
    CNN_Layer.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        if (!this.contracted) {
            for (var j = 0; j < this.size_y; j++) {
                for (var i = 0; i < this.size_x; i++) {
                    this.neurons[j][i].display(canvas);
                }
            }
        }
        else {
            var condensed_layer = new quadrilateral_1.Quadrilateral(new point_1.Point(this.pos_x, this.pos_y), new point_1.Point(this.pos_x + this.width, this.pos_y), new point_1.Point(this.pos_x + 60, this.pos_y + this.height), new point_1.Point(this.pos_x + this.width + 60, this.pos_y + this.height));
            condensed_layer.display(canvas);
        }
        if (!this.isLeaf) {
            context.font = "20px Arial";
            context.fillStyle = "#124191";
            var text = this.size_x + " x " + this.size_y;
            context.fillText(text, this.pos_x - 100, this.pos_y + 30);
            context.font = "30px Arial";
            context.fillStyle = "#124191";
            text = "X " + this.nbChannels;
            context.fillText(text, this.pos_x + this.width + 50, this.pos_y + (this.height / 2));
        }
    };
    CNN_Layer.prototype.getNbNeurons = function () {
        return this.size_x * this.size_y;
    };
    CNN_Layer.prototype.getX = function () {
        return this.neurons[0][0].x;
    };
    CNN_Layer.prototype.getY = function () {
        return this.neurons[0][0].y;
    };
    CNN_Layer.prototype.getRadius = function () {
        return this.neurons[0][0].radius;
    };
    CNN_Layer.prototype.setRadius = function (newRadius) {
        for (var i = 0; i < this.size_y; i++) {
            for (var j = 0; j < this.size_x; j++) {
                this.neurons[i][j].radius = newRadius;
            }
        }
    };
    return CNN_Layer;
}());
exports.CNN_Layer = CNN_Layer;
//# sourceMappingURL=cnn_layer.js.map