"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var cnn_layer_1 = require("../cnn/cnn_layer");
var neuron_1 = require("../neuron");
var neuralNetwork_1 = require("../neuralNetwork");
var CNN = /** @class */ (function (_super) {
    __extends(CNN, _super);
    function CNN(canvas) {
        var _this = _super.call(this) || this;
        _this.canvas = canvas;
        _this.layers = new Array();
        _this.error = "";
        return _this;
    }
    // Override
    CNN.prototype.init = function () {
        this.initLayers(this.canvas);
        this.createConnectors();
    };
    // Override
    CNN.prototype.draw = function () {
        var context = this.canvas.getContext("2d");
        context.save();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.display();
    };
    // Override
    CNN.prototype.display = function () {
        for (var i = this.layers.length - 1; i > 0; i--) {
            this.layers[i].display(this.canvas);
            for (var j = 0; j < this.layers[i].connectors.length; j++) {
                this.layers[i].connectors[j].display(this.canvas);
            }
        }
        this.layers[0].display(this.canvas);
    };
    // Override
    CNN.prototype.addLayer = function (nbX, nbY, nbChannels) {
        try {
            var layer = new cnn_layer_1.CNN_Layer(nbX, nbY, nbChannels);
            this.layers.push(layer);
            this.init();
        }
        catch (_a) {
            this.error = "Error: can't add an empty layer";
        }
    };
    // Override
    CNN.prototype.define = function (nbNeuronsChannel) {
        for (var i = 0; i < nbNeuronsChannel.length; i++) {
            this.addLayer(nbNeuronsChannel[i][0], nbNeuronsChannel[i][1], nbNeuronsChannel[i][2]);
        }
    };
    CNN.prototype.createConnectors = function () {
        for (var i = 1; i < this.layers.length; i++) {
            this.layers[i].createConnectors();
        }
    };
    CNN.prototype.initLayers = function (canvas) {
        for (var iLayer = 0; iLayer < this.layers.length; iLayer++) {
            if (iLayer > 0) {
                this.layers[iLayer].parentLayer = this.layers[iLayer - 1];
                if (this.layers[iLayer].parentLayer.size_y === 1) {
                    this.layers[iLayer].y = this.layers[iLayer].parentLayer.y + this.layers[iLayer].parentLayer.height + 150;
                }
                else {
                    this.layers[iLayer].y = this.layers[iLayer].parentLayer.y + this.layers[iLayer].parentLayer.height + 60;
                }
            }
            if (!this.layers[iLayer].contracted) {
                for (var j = 0; j < this.layers[iLayer].size_y; j++) {
                    this.layers[iLayer].neurons.push(new Array());
                    for (var i = 0; i < this.layers[iLayer].size_x; i++) {
                        var max_radius = 20;
                        var min_radius = 1;
                        var displaySize = this.layers[iLayer].contracted ? max_radius : this.layers[iLayer].size_x;
                        var canvas_width = canvas.width;
                        var gap = 4;
                        var neuron_radius = this.layers[iLayer].isLeaf ?
                            canvas_width / (this.layers[iLayer].size_x * 2.5) :
                            canvas_width / (this.layers[iLayer].size_x * 5);
                        neuron_radius = neuron_radius > max_radius ? max_radius : neuron_radius;
                        neuron_radius = neuron_radius < min_radius ? min_radius : neuron_radius;
                        var free_space = canvas_width - displaySize * (neuron_radius * 2 + gap);
                        var offset = this.layers[iLayer].isLeaf ? 2.2 : 1.5;
                        this.layers[iLayer].neurons[j].push(new neuron_1.Neuron(free_space / 2 + neuron_radius + (i * neuron_radius * offset) + gap + j * (neuron_radius / 4), this.layers[iLayer].y + (neuron_radius / 2) * j, neuron_radius, false, ""));
                        gap += 4;
                    }
                }
                this.layers[iLayer].pos_x = this.layers[iLayer].neurons[0][0].x;
                this.layers[iLayer].pos_y = this.layers[iLayer].neurons[0][0].y;
                this.layers[iLayer].width = this.layers[iLayer].neurons[this.layers[iLayer].size_y - 1][this.layers[iLayer].size_x - 1].x - this.layers[iLayer].pos_x;
                this.layers[iLayer].height = this.layers[iLayer].neurons[this.layers[iLayer].size_y - 1][this.layers[iLayer].size_x - 1].y - this.layers[iLayer].pos_y;
            }
            else {
                this.layers[iLayer].pos_x = 300;
                this.layers[iLayer].pos_y = this.layers[iLayer].y;
                this.layers[iLayer].width = 350;
                this.layers[iLayer].height = 150;
            }
        }
        this.layers[0].isRoot = true;
    };
    // Override
    CNN.prototype.getNbNeurons = function () {
        var nbNeurons = 0;
        for (var i = 0; i < this.layers.length; i++) {
            nbNeurons += this.layers[i].getNbNeurons();
        }
        return nbNeurons;
    };
    // Override
    CNN.prototype.getNbLayers = function () {
        return this.layers.length;
    };
    // Override
    CNN.prototype.isContracted = function () {
        for (var i = 0; i < this.layers.length; i++) {
            if (this.layers[i].contracted) {
                return true;
            }
        }
        return false;
    };
    // Override
    CNN.prototype.getError = function () {
        return this.error;
    };
    return CNN;
}(neuralNetwork_1.NeuralNetwork));
exports.CNN = CNN;
//# sourceMappingURL=cnn.js.map