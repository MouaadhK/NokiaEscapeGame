"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var connector_1 = require("./connector");
var ConnectorSimple = /** @class */ (function (_super) {
    __extends(ConnectorSimple, _super);
    function ConnectorSimple(p1, p2) {
        return _super.call(this, p1, p2) || this;
    }
    // Override
    ConnectorSimple.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        context.save();
        context.beginPath();
        context.lineWidth = 1;
        context.moveTo(this.p1.x, this.p1.y);
        context.lineTo(this.p2.x, this.p2.y);
        context.strokeStyle = "#000000";
        context.stroke();
        context.restore();
    };
    return ConnectorSimple;
}(connector_1.Connector));
exports.ConnectorSimple = ConnectorSimple;
//# sourceMappingURL=connectorSimple.js.map