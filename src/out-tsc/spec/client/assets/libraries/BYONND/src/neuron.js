"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Neuron = /** @class */ (function () {
    function Neuron(x, y, radius, active, value, hovered) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.active = active;
        this.value = value;
        this.hovered = hovered;
    }
    Neuron.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        context.beginPath();
        context.fillStyle = this.active ? "#FFBF02" : "#239DF9";
        context.lineWidth = 2;
        context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
        context.strokeStyle = this.active ? "#FF7900" : "#124191";
        context.stroke();
        context.fill();
        context.font = (this.radius) + "px Arial";
        context.fillStyle = "#124191";
        var value = this.value === "" ? "" : this.value;
        context.fillText(value, this.x - this.radius / 3, this.y + this.radius / 4);
    };
    Neuron.prototype.activate = function () {
        this.active = true;
    };
    Neuron.prototype.disable = function () {
        this.active = false;
    };
    Neuron.prototype.collision = function (x, y) {
        var dx = x - this.x;
        var dy = y - this.y;
        return ((dx * dx) / (this.radius * this.radius) + (dy * dy) /
            (this.radius * this.radius) <= 1);
    };
    Neuron.prototype.setValue = function (value) {
        this.value = value;
    };
    return Neuron;
}());
exports.Neuron = Neuron;
//# sourceMappingURL=neuron.js.map