"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var point_1 = require("../point");
var connectorSimple_1 = require("../connector/connectorSimple");
var Fubuki_layer = /** @class */ (function () {
    function Fubuki_layer(size) {
        this.size = size;
        this.contracted = this.size > 60 ? true : false;
        this.parentLayer = null;
        this.neurons = new Array();
        this.y = 100;
        this.connectors = new Array();
    }
    Fubuki_layer.prototype.getRadius = function () {
        return this.neurons[0].radius;
    };
    Fubuki_layer.prototype.setRadius = function (newRadius) {
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].radius = newRadius;
        }
    };
    Fubuki_layer.prototype.getX = function () {
        return this.neurons[0].x;
    };
    Fubuki_layer.prototype.getY = function () {
        return this.neurons[0].y;
    };
    Fubuki_layer.prototype.getNbNeurons = function () {
        return this.size;
    };
    Fubuki_layer.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        for (var i = 0; i < this.size; i++) {
            this.neurons[i].display(canvas);
        }
    };
    Fubuki_layer.prototype.createConnectors = function () {
        this.connectors = Array();
        for (var i = 1; i < this.neurons.length; i++) {
            this.connectors.push(new connectorSimple_1.ConnectorSimple(new point_1.Point(this.neurons[i - 1].x + this.getRadius(), this.getY()), new point_1.Point(this.neurons[i].x - this.getRadius(), this.getY())));
        }
        if (this.parentLayer != null) {
            for (var i = 0; i < 4; i++) {
                for (var j = 0; j < this.size; j++) {
                    if (i === j) {
                        this.connectors.push(new connectorSimple_1.ConnectorSimple(new point_1.Point(this.neurons[j].x, this.getY() - this.getRadius()), new point_1.Point(this.parentLayer.neurons[i].x, this.parentLayer.getY() + this.parentLayer.getRadius())));
                    }
                }
            }
        }
    };
    Fubuki_layer.prototype.activateNextNeuron = function () {
        if (!this.contracted) {
            var neuronIndex = -1;
            var i = 0;
            var found = false;
            while (i < this.neurons.length && !found) {
                if (this.neurons[i].active) {
                    neuronIndex = i;
                    found = true;
                }
                i++;
            }
            if (neuronIndex === -1) {
                this.neurons[0].activate();
                return true;
            }
            else {
                this.neurons[neuronIndex].disable();
                if (neuronIndex === this.neurons.length - 1) {
                    this.neurons[0].activate();
                    return false;
                }
                else {
                    this.neurons[neuronIndex + 1].activate();
                    return true;
                }
            }
        }
        return true;
    };
    Fubuki_layer.prototype.activateNeuron = function (index) {
        index = index % this.neurons.length;
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
        this.neurons[index].activate();
    };
    Fubuki_layer.prototype.disable = function () {
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
    };
    return Fubuki_layer;
}());
exports.Fubuki_layer = Fubuki_layer;
//# sourceMappingURL=fubuki_layer.js.map