var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var NeuralNetwork = /** @class */ (function () {
    function NeuralNetwork() {
    }
    return NeuralNetwork;
}());
var NeuralNetworkBuilder = /** @class */ (function () {
    function NeuralNetworkBuilder(canvas) {
        this.canvas = canvas;
    }
    NeuralNetworkBuilder.prototype.build = function (type) {
        switch (type) {
            case "mlp":
                return new MLP(this.canvas);
                break;
            case "cnn":
                return new CNN(this.canvas);
                break;
            case "rnn":
                return new RNN(this.canvas);
                break;
            case "fubuki":
                return new Fubuki(this.canvas);
                break;
            default:
                return null;
                break;
        }
    };
    return NeuralNetworkBuilder;
}());
var Neuron = /** @class */ (function () {
    function Neuron(x, y, radius, active, value, hovered) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.active = active;
        this.value = value;
        this.hovered = hovered;
    }
    Neuron.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        context.beginPath();
        context.fillStyle = this.active ? "#FFBF02" : "#239DF9";
        context.lineWidth = 2;
        context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
        context.strokeStyle = this.active ? "#FF7900" : "#124191";
        context.stroke();
        context.fill();
        context.font = (this.radius) + "px Arial";
        context.fillStyle = "#124191";
        var value = this.value === "" ? "" : this.value;
        context.fillText(value, this.x - this.radius / 3, this.y + this.radius / 4);
    };
    Neuron.prototype.activate = function () {
        this.active = true;
    };
    Neuron.prototype.disable = function () {
        this.active = false;
    };
    Neuron.prototype.collision = function (x, y) {
        var dx = x - this.x;
        var dy = y - this.y;
        return ((dx * dx) / (this.radius * this.radius) + (dy * dy) /
            (this.radius * this.radius) <= 1);
    };
    Neuron.prototype.setValue = function (value) {
        this.value = value;
    };
    return Neuron;
}());
var Orientation;
(function (Orientation) {
    Orientation["HORIZONTAL"] = "Horizontal";
    Orientation["VERTICAL"] = "Vertical";
})(Orientation || (Orientation = {}));
var Point = /** @class */ (function () {
    function Point(x, y) {
        this.x = x;
        this.y = y;
    }
    return Point;
}());
var Quadrilateral = /** @class */ (function () {
    function Quadrilateral(p1, p2, p4, p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }
    Quadrilateral.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        context.beginPath();
        context.lineWidth = 1;
        context.moveTo(this.p1.x, this.p1.y);
        context.lineTo(this.p2.x, this.p2.y);
        context.lineTo(this.p3.x, this.p3.y);
        context.lineTo(this.p4.x, this.p4.y);
        context.lineTo(this.p1.x, this.p1.y);
        context.strokeStyle = "#124191";
        context.stroke();
        context.fillStyle = "#239DF9";
        context.fill();
    };
    return Quadrilateral;
}());
var Connector = /** @class */ (function () {
    function Connector(p1, p2) {
        this.p1 = p1;
        this.p2 = p2;
    }
    Connector.prototype.display = function (canvas) { };
    return Connector;
}());
var ConnectorArrow = /** @class */ (function (_super) {
    __extends(ConnectorArrow, _super);
    function ConnectorArrow(p1, p2) {
        return _super.call(this, p1, p2) || this;
    }
    // Override
    ConnectorArrow.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        context.beginPath();
        var headlen = 10;
        var angle = Math.atan2(this.p2.y - this.p1.y, this.p2.x - this.p1.x);
        context.save();
        context.moveTo(this.p1.x, this.p1.y);
        context.lineTo(this.p2.x, this.p2.y);
        context.lineTo(this.p2.x - headlen * Math.cos(angle - Math.PI / 6), this.p2.y - headlen * Math.sin(angle - Math.PI / 6));
        context.moveTo(this.p2.x, this.p2.y);
        context.lineTo(this.p2.x - headlen * Math.cos(angle + Math.PI / 6), this.p2.y - headlen * Math.sin(angle + Math.PI / 6));
        context.stroke();
        context.restore();
    };
    return ConnectorArrow;
}(Connector));
var ConnectorArrowDash = /** @class */ (function (_super) {
    __extends(ConnectorArrowDash, _super);
    function ConnectorArrowDash(p1, p2) {
        return _super.call(this, p1, p2) || this;
    }
    // Override
    ConnectorArrowDash.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        context.save();
        context.beginPath();
        var headlen = 10;
        var angle = Math.atan2(this.p2.y - this.p1.y, this.p2.x - this.p1.x);
        context.setLineDash([5, 15]);
        context.moveTo(this.p1.x, this.p1.y);
        context.lineTo(this.p2.x, this.p2.y);
        context.stroke();
        context.restore();
        context.save();
        context.beginPath();
        context.moveTo(this.p2.x, this.p2.y);
        context.lineTo(this.p2.x - headlen * Math.cos(angle - Math.PI / 6), this.p2.y - headlen * Math.sin(angle - Math.PI / 6));
        context.moveTo(this.p2.x, this.p2.y);
        context.lineTo(this.p2.x - headlen * Math.cos(angle + Math.PI / 6), this.p2.y - headlen * Math.sin(angle + Math.PI / 6));
        context.stroke();
        context.restore();
    };
    return ConnectorArrowDash;
}(Connector));
var ConnectorSimple = /** @class */ (function (_super) {
    __extends(ConnectorSimple, _super);
    function ConnectorSimple(p1, p2) {
        return _super.call(this, p1, p2) || this;
    }
    // Override
    ConnectorSimple.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        context.save();
        context.beginPath();
        context.lineWidth = 1;
        context.moveTo(this.p1.x, this.p1.y);
        context.lineTo(this.p2.x, this.p2.y);
        context.strokeStyle = "#000000";
        context.stroke();
        context.restore();
    };
    return ConnectorSimple;
}(Connector));
var CNN = /** @class */ (function (_super) {
    __extends(CNN, _super);
    function CNN(canvas) {
        var _this = _super.call(this) || this;
        _this.canvas = canvas;
        _this.layers = new Array();
        _this.error = "";
        return _this;
    }
    // Override
    CNN.prototype.init = function () {
        this.initLayers(this.canvas);
        this.createConnectors();
    };
    // Override
    CNN.prototype.draw = function () {
        var context = this.canvas.getContext("2d");
        context.save();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.display();
    };
    // Override
    CNN.prototype.display = function () {
        for (var i = this.layers.length - 1; i > 0; i--) {
            this.layers[i].display(this.canvas);
            for (var j = 0; j < this.layers[i].connectors.length; j++) {
                this.layers[i].connectors[j].display(this.canvas);
            }
        }
        this.layers[0].display(this.canvas);
    };
    // Override
    CNN.prototype.addLayer = function (nbX, nbY, nbChannels) {
        try {
            var layer = new CNN_Layer(nbX, nbY, nbChannels);
            this.layers.push(layer);
            this.init();
        }
        catch (_a) {
            this.error = "Error: can't add an empty layer";
        }
    };
    // Override
    CNN.prototype.define = function (nbNeuronsChannel) {
        for (var i = 0; i < nbNeuronsChannel.length; i++) {
            this.addLayer(nbNeuronsChannel[i][0], nbNeuronsChannel[i][1], nbNeuronsChannel[i][2]);
        }
    };
    CNN.prototype.createConnectors = function () {
        for (var i = 1; i < this.layers.length; i++) {
            this.layers[i].createConnectors();
        }
    };
    CNN.prototype.initLayers = function (canvas) {
        for (var iLayer = 0; iLayer < this.layers.length; iLayer++) {
            if (iLayer > 0) {
                this.layers[iLayer].parentLayer = this.layers[iLayer - 1];
                if (this.layers[iLayer].parentLayer.size_y === 1) {
                    this.layers[iLayer].y = this.layers[iLayer].parentLayer.y + this.layers[iLayer].parentLayer.height + 150;
                }
                else {
                    this.layers[iLayer].y = this.layers[iLayer].parentLayer.y + this.layers[iLayer].parentLayer.height + 60;
                }
            }
            if (!this.layers[iLayer].contracted) {
                for (var j = 0; j < this.layers[iLayer].size_y; j++) {
                    this.layers[iLayer].neurons.push(new Array());
                    for (var i = 0; i < this.layers[iLayer].size_x; i++) {
                        var max_radius = 20;
                        var min_radius = 1;
                        var displaySize = this.layers[iLayer].contracted ? max_radius : this.layers[iLayer].size_x;
                        var canvas_width = canvas.width;
                        var gap = 4;
                        var neuron_radius = this.layers[iLayer].isLeaf ?
                            canvas_width / (this.layers[iLayer].size_x * 2.5) :
                            canvas_width / (this.layers[iLayer].size_x * 5);
                        neuron_radius = neuron_radius > max_radius ? max_radius : neuron_radius;
                        neuron_radius = neuron_radius < min_radius ? min_radius : neuron_radius;
                        var free_space = canvas_width - displaySize * (neuron_radius * 2 + gap);
                        var offset = this.layers[iLayer].isLeaf ? 2.2 : 1.5;
                        this.layers[iLayer].neurons[j].push(new Neuron(free_space / 2 + neuron_radius + (i * neuron_radius * offset) + gap + j * (neuron_radius / 4), this.layers[iLayer].y + (neuron_radius / 2) * j, neuron_radius, false, ""));
                        gap += 4;
                    }
                }
                this.layers[iLayer].pos_x = this.layers[iLayer].neurons[0][0].x;
                this.layers[iLayer].pos_y = this.layers[iLayer].neurons[0][0].y;
                this.layers[iLayer].width = this.layers[iLayer].neurons[this.layers[iLayer].size_y - 1][this.layers[iLayer].size_x - 1].x - this.layers[iLayer].pos_x;
                this.layers[iLayer].height = this.layers[iLayer].neurons[this.layers[iLayer].size_y - 1][this.layers[iLayer].size_x - 1].y - this.layers[iLayer].pos_y;
            }
            else {
                this.layers[iLayer].pos_x = 300;
                this.layers[iLayer].pos_y = this.layers[iLayer].y;
                this.layers[iLayer].width = 350;
                this.layers[iLayer].height = 150;
            }
        }
        this.layers[0].isRoot = true;
    };
    // Override
    CNN.prototype.getNbNeurons = function () {
        var nbNeurons = 0;
        for (var i = 0; i < this.layers.length; i++) {
            nbNeurons += this.layers[i].getNbNeurons();
        }
        return nbNeurons;
    };
    // Override
    CNN.prototype.getNbLayers = function () {
        return this.layers.length;
    };
    // Override
    CNN.prototype.isContracted = function () {
        for (var i = 0; i < this.layers.length; i++) {
            if (this.layers[i].contracted) {
                return true;
            }
        }
        return false;
    };
    // Override
    CNN.prototype.getError = function () {
        return this.error;
    };
    return CNN;
}(NeuralNetwork));
var CNN_Layer = /** @class */ (function () {
    function CNN_Layer(size_x, size_y, nbChannels) {
        this.size_x = size_x;
        this.size_y = size_y == null ? 1 : size_y;
        this.nbChannels = nbChannels == null ? 0 : nbChannels;
        this.isLeaf = this.size_y === 1 && this.nbChannels === 0 ? true : false;
        this.contracted = (this.size_x > 60 ||
            this.size_y > 60) ? true : false;
        this.parentLayer = null;
        this.neurons = new Array();
        this.y = 100;
        this.connectors = new Array();
    }
    CNN_Layer.prototype.createConnectors = function () {
        if (this.connectors.length === 0) {
            this.connectors = Array();
            if (this.size_y > 1 || this.parentLayer.size_y > 1) {
                for (var i = 0; i < 3; i++) {
                    for (var j = 0; j < 3; j++) {
                        this.connectors.push(new ConnectorSimple(new Point((this.pos_x + (this.width / 2)), (this.pos_y + this.height / 2)), new Point((this.parentLayer.pos_x + (this.parentLayer.width / 2) - this.parentLayer.width / 4 + this.parentLayer.width / 4 * i), (this.parentLayer.y + this.parentLayer.height / 2) - this.parentLayer.height / 4 + this.parentLayer.height / 4 * j)));
                    }
                }
            }
            else {
                if (this.parentLayer.getNbNeurons() > 25 || this.getNbNeurons() > 25) {
                    for (var i = 0; i < 11; i++) {
                        for (var j = 0; j < 11; j++) {
                            this.connectors.push(new ConnectorSimple(new Point(50 + i * 95, this.getY() - this.getRadius() - 5), new Point(50 + j * 95, this.parentLayer.getY() - this.parentLayer.getRadius() + 35)));
                        }
                    }
                }
                else {
                    for (var i = 0; i < this.parentLayer.getNbNeurons(); i++) {
                        for (var j = 0; j < this.getNbNeurons(); j++) {
                            this.connectors.push(new ConnectorSimple(new Point(this.neurons[0][j].x, this.getY() - this.getRadius()), new Point(this.parentLayer.neurons[0][i].x, this.parentLayer.getY() + this.parentLayer.getRadius())));
                        }
                    }
                }
            }
        }
    };
    CNN_Layer.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        if (!this.contracted) {
            for (var j = 0; j < this.size_y; j++) {
                for (var i = 0; i < this.size_x; i++) {
                    this.neurons[j][i].display(canvas);
                }
            }
        }
        else {
            var condensed_layer = new Quadrilateral(new Point(this.pos_x, this.pos_y), new Point(this.pos_x + this.width, this.pos_y), new Point(this.pos_x + 60, this.pos_y + this.height), new Point(this.pos_x + this.width + 60, this.pos_y + this.height));
            condensed_layer.display(canvas);
        }
        if (!this.isLeaf) {
            context.font = "20px Arial";
            context.fillStyle = "#124191";
            var text = this.size_x + " x " + this.size_y;
            context.fillText(text, this.pos_x - 100, this.pos_y + 30);
            context.font = "30px Arial";
            context.fillStyle = "#124191";
            text = "X " + this.nbChannels;
            context.fillText(text, this.pos_x + this.width + 50, this.pos_y + (this.height / 2));
        }
    };
    CNN_Layer.prototype.getNbNeurons = function () {
        return this.size_x * this.size_y;
    };
    CNN_Layer.prototype.getX = function () {
        return this.neurons[0][0].x;
    };
    CNN_Layer.prototype.getY = function () {
        return this.neurons[0][0].y;
    };
    CNN_Layer.prototype.getRadius = function () {
        return this.neurons[0][0].radius;
    };
    CNN_Layer.prototype.setRadius = function (newRadius) {
        for (var i = 0; i < this.size_y; i++) {
            for (var j = 0; j < this.size_x; j++) {
                this.neurons[i][j].radius = newRadius;
            }
        }
    };
    return CNN_Layer;
}());
var MLP = /** @class */ (function (_super) {
    __extends(MLP, _super);
    function MLP(canvas) {
        var _this = _super.call(this) || this;
        _this.canvas = canvas;
        _this.layers = new Array();
        _this.error = "";
        return _this;
    }
    // Override
    MLP.prototype.init = function () {
        this.initLayers(this.canvas);
        this.balanceRadius();
        this.createConnectors();
    };
    // Override
    MLP.prototype.addLayer = function (nbNeurons) {
        try {
            var layer = new MLP_Layer(nbNeurons);
            this.layers.push(layer);
            this.init();
        }
        catch (_a) {
            this.error = "Error: can't add an empty layer";
        }
    };
    // Override
    MLP.prototype.define = function (nbNeurons) {
        for (var i = 0; i < nbNeurons.length; i++) {
            this.addLayer(nbNeurons[i]);
        }
    };
    // Override
    MLP.prototype.draw = function () {
        var context = this.canvas.getContext("2d");
        context.save();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.display();
    };
    MLP.prototype.initLayers = function (canvas) {
        for (var j = 0; j < this.layers.length; j++) {
            if (this.layers[j].neurons.length === 0) {
                if (j > 0) {
                    this.layers[j].parentLayer = this.layers[j - 1];
                    this.layers[j].y = this.layers[j].parentLayer.y + 150;
                }
                var max_radius = 30;
                var min_radius = 1;
                var displaySize = this.layers[j].contracted ? max_radius : this.layers[j].size;
                var canvas_width = canvas.width;
                var gap = 4;
                var neuron_radius = canvas_width / ((displaySize * 2)) - (gap / 2) - 1;
                neuron_radius = neuron_radius > max_radius ? max_radius : neuron_radius;
                neuron_radius = neuron_radius < min_radius ? min_radius : neuron_radius;
                var free_space = canvas_width - displaySize * (neuron_radius * 2 + gap);
                if (!this.layers[j].contracted) {
                    for (var i = 0; i < displaySize; i++) {
                        this.layers[j].neurons.push(new Neuron(free_space / 2 + neuron_radius + (i * neuron_radius * 2) + gap, this.layers[j].y, neuron_radius, false, ""));
                        gap += 4;
                    }
                }
                else {
                    for (var i = 0; i < 4; i++) {
                        this.layers[j].neurons.push(new Neuron(free_space / 2 + neuron_radius + (i * neuron_radius * 2) + gap, this.layers[j].y, neuron_radius, false, ""));
                        gap += 8;
                    }
                    for (var i = 0; i < 4; i++) {
                        this.layers[j].neurons.push(new Neuron(canvas_width - free_space / 2 - neuron_radius - (i * neuron_radius * 2) - gap + 40, this.layers[j].y, neuron_radius, false, ""));
                        gap += 8;
                    }
                }
            }
        }
    };
    MLP.prototype.balanceRadius = function () {
        var min_radius = this.layers[0].getRadius();
        for (var i = 1; i < this.layers.length; i++) {
            if (min_radius > this.layers[i].getRadius()) {
                min_radius = this.layers[i].getRadius();
            }
        }
        for (var i = 0; i < this.layers.length; i++) {
            this.layers[i].setRadius(min_radius);
        }
    };
    MLP.prototype.createConnectors = function () {
        for (var i = 1; i < this.layers.length; i++) {
            this.layers[i].createConnectors();
        }
    };
    // Override
    MLP.prototype.display = function () {
        for (var i = 1; i < this.layers.length; i++) {
            for (var j = 0; j < this.layers[i].connectors.length; j++) {
                this.layers[i].connectors[j].display(this.canvas);
            }
        }
        for (var i = 0; i < this.layers.length; i++) {
            this.layers[i].display(this.canvas);
        }
    };
    // Override
    MLP.prototype.getNbNeurons = function () {
        var nbNeurons = 0;
        for (var i = 0; i < this.layers.length; i++) {
            nbNeurons += this.layers[i].getNbNeurons();
        }
        return nbNeurons;
    };
    MLP.prototype.activateLayerNextNeuron = function (layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        return this.layers[layerIndex].activateNextNeuron();
    };
    MLP.prototype.activateLayerNeuron = function (layerIndex, neuronIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].activateNeuron(neuronIndex);
    };
    MLP.prototype.activateOutputNeuron = function (neuronIndex) {
        this.layers[this.layers.length - 1].activateNeuron(neuronIndex);
    };
    MLP.prototype.disableLayer = function (layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].disable();
    };
    // Override
    MLP.prototype.getNbLayers = function () {
        return this.layers.length;
    };
    // Override
    MLP.prototype.isContracted = function () {
        for (var i = 0; i < this.layers.length; i++) {
            if (this.layers[i].contracted) {
                return true;
            }
        }
        return false;
    };
    // Override
    MLP.prototype.getError = function () {
        return this.error;
    };
    return MLP;
}(NeuralNetwork));
var MLP_Layer = /** @class */ (function () {
    function MLP_Layer(size) {
        this.size = size;
        this.contracted = this.size > 60 ? true : false;
        this.parentLayer = null;
        this.neurons = new Array();
        this.y = 100;
        this.connectors = new Array();
    }
    MLP_Layer.prototype.getRadius = function () {
        return this.neurons[0].radius;
    };
    MLP_Layer.prototype.setRadius = function (newRadius) {
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].radius = newRadius;
        }
    };
    MLP_Layer.prototype.getX = function () {
        return this.neurons[0].x;
    };
    MLP_Layer.prototype.getY = function () {
        return this.neurons[0].y;
    };
    MLP_Layer.prototype.getNbNeurons = function () {
        return this.size;
    };
    MLP_Layer.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        if (!this.contracted) {
            for (var i = 0; i < this.size; i++) {
                this.neurons[i].display(canvas);
            }
        }
        else {
            var canvas_width = canvas.width;
            for (var i = 0; i < this.neurons.length; i++) {
                this.neurons[i].display(canvas);
            }
            context.font = "30px Arial";
            var text = ". . . . . " + this.size + " neurons . . . . .";
            context.fillText(text, canvas_width / 2 - text.length * 6, this.getY() + this.getRadius() / 2);
        }
    };
    MLP_Layer.prototype.createConnectors = function () {
        if (this.connectors.length === 0) {
            if (!this.contracted && !this.parentLayer.contracted) {
                this.connectors = Array();
                for (var i = 0; i < this.parentLayer.size; i++) {
                    for (var j = 0; j < this.size; j++) {
                        this.connectors.push(new ConnectorSimple(new Point(this.neurons[j].x, this.getY() - this.getRadius()), new Point(this.parentLayer.neurons[i].x, this.parentLayer.getY() + this.parentLayer.getRadius())));
                    }
                }
            }
            else {
                for (var i = 0; i < 11; i++) {
                    for (var j = 0; j < 11; j++) {
                        this.connectors.push(new ConnectorSimple(new Point(50 + i * 101, this.getY() - this.getRadius() - 5), new Point(50 + j * 101, this.parentLayer.getY() - this.parentLayer.getRadius() + 35)));
                    }
                }
            }
        }
    };
    MLP_Layer.prototype.activateNextNeuron = function () {
        if (!this.contracted) {
            var neuronIndex = -1;
            var i = 0;
            var found = false;
            while (i < this.neurons.length && !found) {
                if (this.neurons[i].active) {
                    neuronIndex = i;
                    found = true;
                }
                i++;
            }
            if (neuronIndex === -1) {
                this.neurons[0].activate();
                return true;
            }
            else {
                this.neurons[neuronIndex].disable();
                if (neuronIndex === this.neurons.length - 1) {
                    this.neurons[0].activate();
                    return false;
                }
                else {
                    this.neurons[neuronIndex + 1].activate();
                    return true;
                }
            }
        }
        return true;
    };
    MLP_Layer.prototype.activateNeuron = function (index) {
        index = index % this.neurons.length;
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
        this.neurons[index].activate();
    };
    MLP_Layer.prototype.disable = function () {
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
    };
    return MLP_Layer;
}());
var Fubuki = /** @class */ (function (_super) {
    __extends(Fubuki, _super);
    function Fubuki(canvas) {
        var _this = _super.call(this) || this;
        _this.canvas = canvas;
        _this.layers = new Array();
        _this.error = "";
        return _this;
    }
    // Override
    Fubuki.prototype.init = function () {
        this.initLayers(this.canvas);
        this.balanceRadius();
        this.createConnectors();
    };
    // Override
    Fubuki.prototype.addLayer = function (nbNeurons) {
        try {
            var layer = new Fubuki_layer(nbNeurons);
            this.layers.push(layer);
            this.init();
        }
        catch (_a) {
            this.error = "Error: can't add an empty layer";
        }
    };
    // Override
    Fubuki.prototype.define = function (nbNeurons) {
        for (var i = 0; i < nbNeurons.length; i++) {
            this.addLayer(nbNeurons[i]);
        }
    };
    // Override
    Fubuki.prototype.draw = function () {
        var context = this.canvas.getContext("2d");
        context.save();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.display();
    };
    Fubuki.prototype.initLayers = function (canvas) {
        for (var j = 0; j < this.layers.length; j++) {
            if (this.layers[j].neurons.length === 0) {
                if (j > 0) {
                    this.layers[j].parentLayer = this.layers[j - 1];
                    this.layers[j].y = this.layers[j].parentLayer.y + 150;
                }
                var max_radius = 30;
                var min_radius = 1;
                var displaySize = this.layers[j].contracted ? max_radius : this.layers[j].size;
                var canvas_width = canvas.width;
                var gap = 0;
                var neuron_radius = canvas_width / ((displaySize * 2)) - (gap / 2) - 1;
                neuron_radius = neuron_radius > max_radius ? max_radius : neuron_radius;
                neuron_radius = neuron_radius < min_radius ? min_radius : neuron_radius;
                for (var i = 0; i < displaySize; i++) {
                    this.layers[j].neurons.push(new Neuron(100 + neuron_radius + (i * neuron_radius * 2) + gap, this.layers[j].y, neuron_radius, false, ""));
                    gap += 150;
                }
            }
        }
    };
    Fubuki.prototype.balanceRadius = function () {
        var min_radius = this.layers[0].getRadius();
        for (var i = 1; i < this.layers.length; i++) {
            if (min_radius > this.layers[i].getRadius()) {
                min_radius = this.layers[i].getRadius();
            }
        }
        for (var i = 0; i < this.layers.length; i++) {
            this.layers[i].setRadius(min_radius);
        }
    };
    Fubuki.prototype.createConnectors = function () {
        for (var i = 0; i < this.layers.length; i++) {
            this.layers[i].createConnectors();
        }
    };
    // Override
    Fubuki.prototype.display = function () {
        for (var i = 0; i < this.layers.length; i++) {
            for (var j = 0; j < this.layers[i].connectors.length; j++) {
                this.layers[i].connectors[j].display(this.canvas);
            }
        }
        for (var i = 0; i < this.layers.length; i++) {
            this.layers[i].display(this.canvas);
        }
    };
    // Override
    Fubuki.prototype.getNbNeurons = function () {
        var nbNeurons = 0;
        for (var i = 0; i < this.layers.length; i++) {
            nbNeurons += this.layers[i].getNbNeurons();
        }
        return nbNeurons;
    };
    Fubuki.prototype.activateLayerNextNeuron = function (layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        return this.layers[layerIndex].activateNextNeuron();
    };
    Fubuki.prototype.activateLayerNeuron = function (layerIndex, neuronIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].activateNeuron(neuronIndex);
    };
    Fubuki.prototype.activateOutputNeuron = function (neuronIndex) {
        this.layers[this.layers.length - 1].activateNeuron(neuronIndex);
    };
    Fubuki.prototype.disableLayer = function (layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].disable();
    };
    // Override
    Fubuki.prototype.getNbLayers = function () {
        return this.layers.length;
    };
    // Override
    Fubuki.prototype.isContracted = function () {
        for (var i = 0; i < this.layers.length; i++) {
            if (this.layers[i].contracted) {
                return true;
            }
        }
        return false;
    };
    // Override
    Fubuki.prototype.getError = function () {
        return this.error;
    };
    return Fubuki;
}(NeuralNetwork));
var Fubuki_layer = /** @class */ (function () {
    function Fubuki_layer(size) {
        this.size = size;
        this.contracted = this.size > 60 ? true : false;
        this.parentLayer = null;
        this.neurons = new Array();
        this.y = 100;
        this.connectors = new Array();
    }
    Fubuki_layer.prototype.getRadius = function () {
        return this.neurons[0].radius;
    };
    Fubuki_layer.prototype.setRadius = function (newRadius) {
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].radius = newRadius;
        }
    };
    Fubuki_layer.prototype.getX = function () {
        return this.neurons[0].x;
    };
    Fubuki_layer.prototype.getY = function () {
        return this.neurons[0].y;
    };
    Fubuki_layer.prototype.getNbNeurons = function () {
        return this.size;
    };
    Fubuki_layer.prototype.display = function (canvas) {
        var context = canvas.getContext("2d");
        for (var i = 0; i < this.size; i++) {
            this.neurons[i].display(canvas);
        }
    };
    Fubuki_layer.prototype.createConnectors = function () {
        this.connectors = Array();
        for (var i = 1; i < this.neurons.length; i++) {
            this.connectors.push(new ConnectorSimple(new Point(this.neurons[i - 1].x + this.getRadius(), this.getY()), new Point(this.neurons[i].x - this.getRadius(), this.getY())));
        }
        if (this.parentLayer != null) {
            for (var i = 0; i < 4; i++) {
                for (var j = 0; j < this.size; j++) {
                    if (i === j) {
                        this.connectors.push(new ConnectorSimple(new Point(this.neurons[j].x, this.getY() - this.getRadius()), new Point(this.parentLayer.neurons[i].x, this.parentLayer.getY() + this.parentLayer.getRadius())));
                    }
                }
            }
        }
    };
    Fubuki_layer.prototype.activateNextNeuron = function () {
        if (!this.contracted) {
            var neuronIndex = -1;
            var i = 0;
            var found = false;
            while (i < this.neurons.length && !found) {
                if (this.neurons[i].active) {
                    neuronIndex = i;
                    found = true;
                }
                i++;
            }
            if (neuronIndex === -1) {
                this.neurons[0].activate();
                return true;
            }
            else {
                this.neurons[neuronIndex].disable();
                if (neuronIndex === this.neurons.length - 1) {
                    this.neurons[0].activate();
                    return false;
                }
                else {
                    this.neurons[neuronIndex + 1].activate();
                    return true;
                }
            }
        }
        return true;
    };
    Fubuki_layer.prototype.activateNeuron = function (index) {
        index = index % this.neurons.length;
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
        this.neurons[index].activate();
    };
    Fubuki_layer.prototype.disable = function () {
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
    };
    return Fubuki_layer;
}());
var RNN = /** @class */ (function (_super) {
    __extends(RNN, _super);
    function RNN(canvas) {
        var _this = _super.call(this) || this;
        _this.canvas = canvas;
        _this.steps = new Array();
        _this.layers = new Array();
        _this.orientation = Orientation.HORIZONTAL;
        _this.error = "";
        return _this;
    }
    // Override
    RNN.prototype.init = function () {
        this.initLayers(this.canvas);
        this.createConnectors();
    };
    // Override
    RNN.prototype.addLayer = function (nbNeurons) {
        try {
            var layer = new RNN_Layer(nbNeurons);
            this.layers.push(layer);
        }
        catch (_a) {
            this.error = "Error: can't add an empty layer";
        }
    };
    // Override
    RNN.prototype.define = function (nbSteps, nbLayers, serie, orientation) {
        this.nbLayers = nbLayers + 2;
        this.contracted = nbSteps > 15;
        this.nbSteps = this.isContracted() ? 15 : nbSteps;
        for (var i = 0; i < this.nbLayers; i++) {
            this.addLayer(3);
        }
        switch (orientation.toLowerCase()) {
            case "horizontal":
                this.orientation = Orientation.HORIZONTAL;
                break;
            case "vertical":
                this.orientation = Orientation.VERTICAL;
                break;
        }
        this.init();
        this.serie = serie;
    };
    // Override
    RNN.prototype.draw = function () {
        var context = this.canvas.getContext("2d");
        context.save();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.display();
    };
    RNN.prototype.initLayers = function (canvas) {
        var honrizontalConnectorSize = 50;
        var gap_step = 4 + honrizontalConnectorSize;
        var max_radius = 30;
        var min_radius = 1;
        var displaySize = this.nbSteps;
        var canvas_width = canvas.width;
        var neuron_radius = canvas_width / ((displaySize * 2)) - ((gap_step) / 2) - 1;
        neuron_radius = neuron_radius > max_radius ? max_radius : neuron_radius;
        neuron_radius = neuron_radius < min_radius ? min_radius : neuron_radius;
        var free_space = canvas_width - displaySize * (neuron_radius * 2 + gap_step);
        for (var i = 0; i < this.nbSteps; i++) {
            for (var j = 0; j < this.layers.length; j++) {
                if (this.layers[j].neurons.length === 0) {
                    if (j > 0) {
                        this.layers[j].parentLayer = this.layers[j - 1];
                        this.layers[j].y = this.layers[j].parentLayer.y + 120;
                    }
                    var gap_layer = 0;
                    for (var k = 0; k < this.layers[j].size; k++) {
                        switch (this.orientation) {
                            case Orientation.HORIZONTAL:
                                this.layers[j].neurons.push(new Neuron(free_space / 2 + neuron_radius + (i * neuron_radius * 2) + gap_step - 30, this.layers[j].y + gap_layer, neuron_radius, false, ""));
                                break;
                            case Orientation.VERTICAL:
                                this.layers[j].neurons.push(new Neuron(this.layers[j].y, free_space / 2 + neuron_radius + (i * neuron_radius * 2) + gap_step + gap_layer - 30, neuron_radius, false, ""));
                                break;
                        }
                        gap_layer += neuron_radius / 3;
                    }
                }
            }
            gap_step += 4 + honrizontalConnectorSize;
            this.steps.push(this.layers);
            this.layers = new Array();
            for (var i_1 = 0; i_1 < this.nbLayers; i_1++) {
                this.addLayer(3);
            }
        }
    };
    RNN.prototype.createConnectors = function () {
        for (var i = 0; i < this.nbSteps; i++) {
            for (var j = 0; j < this.layers.length; j++) {
                this.steps[i][j].createConnectors(i, j, this);
            }
        }
    };
    RNN.prototype.displaySteps = function () {
        for (var i = 0; i < this.nbSteps; i++) {
            for (var j = 0; j < this.layers.length; j++) {
                this.steps[i][j].display(this.canvas, this);
            }
        }
    };
    // Override
    RNN.prototype.display = function () {
        var context = this.canvas.getContext("2d");
        for (var i = 0; i < this.nbSteps; i++) {
            for (var j = 0; j < this.steps[i].length; j++) {
                for (var k = 0; k < this.steps[i][j].connectors.length; k++) {
                    this.steps[i][j].connectors[k].display(this.canvas);
                }
            }
        }
        this.displaySteps();
        if (this.serie != null) {
            if (this.nbSteps + 1 === this.serie.length) {
                switch (this.orientation) {
                    case Orientation.HORIZONTAL:
                        for (var i = 0; i < this.nbSteps; i++) {
                            context.font = "30px Arial";
                            context.fillText(this.serie[i], this.steps[i][0].neurons[0].x - 10, this.steps[i][0].neurons[0].y - 40);
                            context.fillText("t=" + i, this.steps[i][0].neurons[0].x - 20, this.steps[i][0].neurons[0].y - 90);
                        }
                        for (var i = 1; i <= this.nbSteps; i++) {
                            context.font = "30px Arial";
                            context.fillText(this.serie[i], this.steps[i - 1][0].neurons[0].x - 10, this.steps[i - 1][this.nbLayers - 1].neurons[0].y + 80);
                        }
                        break;
                    case Orientation.VERTICAL:
                        for (var i = 0; i < this.nbSteps; i++) {
                            context.font = "30px Arial";
                            var lastNeuron = this.steps[i][0].neurons[this.steps[i][0].neurons.length - 1];
                            context.fillText(this.serie[i], lastNeuron.x - 60, lastNeuron.y);
                            context.fillText("t=" + i, lastNeuron.x - 120, lastNeuron.y);
                        }
                        for (var i = 1; i <= this.nbSteps; i++) {
                            context.font = "30px Arial";
                            var lastNeuron = this.steps[i - 1][this.nbLayers - 1].neurons[this.steps[i - 1][this.nbLayers - 1].neurons.length - 1];
                            context.fillText(this.serie[i], lastNeuron.x + 60, lastNeuron.y);
                        }
                        break;
                }
            }
            else {
                if (this.isContracted()) {
                    console.error("Can't display serie data in a contracted view (serie length > 15)");
                }
                else {
                    console.error("The (nb steps+1) and the serie length must be equal");
                }
            }
        }
    };
    // Override
    RNN.prototype.getNbNeurons = function () {
        var nbNeurons = 0;
        for (var i = 0; i < this.layers.length; i++) {
            nbNeurons += this.layers[i].getNbNeurons();
        }
        return nbNeurons;
    };
    RNN.prototype.activateLayerNeuron = function (layerIndex, neuronIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].activateNeuron(neuronIndex);
    };
    RNN.prototype.activateOutputNeuron = function (neuronIndex) {
        this.layers[this.layers.length - 1].activateNeuron(neuronIndex);
    };
    RNN.prototype.disableLayer = function (layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].disable();
    };
    // Override
    RNN.prototype.getNbLayers = function () {
        return this.layers.length;
    };
    // Override
    RNN.prototype.isContracted = function () {
        return this.contracted;
    };
    // Override
    RNN.prototype.getError = function () {
        return this.error;
    };
    return RNN;
}(NeuralNetwork));
var RNN_Layer = /** @class */ (function () {
    function RNN_Layer(size) {
        this.size = size;
        this.parentLayer = null;
        this.neurons = new Array();
        this.y = 150;
        this.connectors = new Array();
    }
    RNN_Layer.prototype.getRadius = function () {
        return this.neurons[0].radius;
    };
    RNN_Layer.prototype.setRadius = function (newRadius) {
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].radius = newRadius;
        }
    };
    RNN_Layer.prototype.getX = function () {
        return this.neurons[0].x;
    };
    RNN_Layer.prototype.getY = function () {
        return this.neurons[0].y;
    };
    RNN_Layer.prototype.getNbNeurons = function () {
        return this.size;
    };
    RNN_Layer.prototype.display = function (canvas, neuralNetwork) {
        for (var i = 0; i < this.size; i++) {
            this.neurons[i].display(canvas);
        }
    };
    RNN_Layer.prototype.createConnectors = function (indexStep, indexLayer, neuralNetwork) {
        if (this.connectors.length === 0) {
            if (indexLayer > 0) {
                switch (neuralNetwork.orientation) {
                    case Orientation.HORIZONTAL:
                        this.connectors.push(new ConnectorArrow(new Point(this.parentLayer.neurons[this.parentLayer.neurons.length - 1].x, this.parentLayer.neurons[this.parentLayer.neurons.length - 1].y + this.parentLayer.getRadius()), new Point(this.neurons[0].x, this.getY() - this.getRadius())));
                        if (indexLayer < neuralNetwork.nbLayers - 1 && indexStep < neuralNetwork.nbSteps - 1) {
                            var next_nn = neuralNetwork.steps[indexStep + 1][indexLayer];
                            this.connectors.push(new ConnectorArrow(new Point(this.neurons[this.neurons.length - 1].x + this.getRadius(), this.neurons[this.neurons.length - 1].y), new Point(next_nn.neurons[next_nn.neurons.length - 1].x - this.getRadius(), next_nn.neurons[next_nn.neurons.length - 1].y)));
                        }
                        break;
                    case Orientation.VERTICAL:
                        this.connectors.push(new ConnectorArrow(new Point(this.parentLayer.neurons[this.parentLayer.neurons.length - 1].x + this.parentLayer.getRadius(), this.parentLayer.neurons[this.parentLayer.neurons.length - 1].y), new Point(this.neurons[this.neurons.length - 1].x - this.getRadius(), this.neurons[this.neurons.length - 1].y)));
                        if (indexStep < neuralNetwork.nbSteps - 1 && indexLayer < neuralNetwork.nbLayers - 1) {
                            var next_nn = neuralNetwork.steps[indexStep + 1][indexLayer];
                            this.connectors.push(new ConnectorArrow(new Point(this.neurons[0].x, this.neurons[0].y + this.getRadius()), new Point(next_nn.neurons[0].x, next_nn.neurons[0].y - next_nn.getRadius())));
                        }
                        break;
                }
            }
            if (indexLayer === neuralNetwork.nbLayers - 1 && indexStep < neuralNetwork.nbSteps - 1) {
                var cur_nn = neuralNetwork.steps[indexStep][indexLayer];
                var next_nn = neuralNetwork.steps[indexStep + 1][0];
                switch (neuralNetwork.orientation) {
                    case Orientation.HORIZONTAL:
                        this.connectors.push(new ConnectorArrowDash(new Point(cur_nn.neurons[0].x + cur_nn.getRadius() / 3, cur_nn.getY() - cur_nn.getRadius()), new Point(next_nn.neurons[next_nn.neurons.length - 1].x - next_nn.getRadius() / 3, next_nn.neurons[next_nn.neurons.length - 1].y + next_nn.getRadius())));
                        break;
                    case Orientation.VERTICAL:
                        this.connectors.push(new ConnectorArrowDash(new Point(cur_nn.neurons[0].x - cur_nn.getRadius(), cur_nn.getY() + cur_nn.getRadius()), new Point(next_nn.neurons[next_nn.neurons.length - 1].x + next_nn.getRadius(), next_nn.neurons[next_nn.neurons.length - 1].y - next_nn.getRadius())));
                        break;
                }
            }
        }
    };
    RNN_Layer.prototype.activateNeuron = function (index) {
        index = index % this.neurons.length;
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
        this.neurons[index].activate();
    };
    RNN_Layer.prototype.disable = function () {
        for (var i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
    };
    return RNN_Layer;
}());
//# sourceMappingURL=bundle.js.map