"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Game_1 = require("../../../api/Game");
var Sockets_1 = require("../../../api/Sockets");
var socket_service_1 = require("../services/socket.service");
var router_1 = require("@angular/router");
var QuizEditorComponent = /** @class */ (function () {
    function QuizEditorComponent(socketService, route) {
        this.socketService = socketService;
        this.route = route;
        this.popups = [];
    }
    QuizEditorComponent.prototype.ngOnInit = function () {
        var _this = this;
        var activatedRoutes = new router_1.ActivatedRoute();
        this.route.params.subscribe(function (params) {
            _this.type = params["type"];
            console.log(params);
            console.log(_this.type);
            _this.socketService.emit(Sockets_1.SocketEvent.getQuizPopups, _this.type);
            _this.socketService.onReceive(Sockets_1.SocketEvent.quizPopups, function (popups) {
                _this.popups = popups;
                console.log(_this.popups);
            });
        });
    };
    QuizEditorComponent.prototype.addPopup = function () {
        var newPopup = [];
        this.popups.push(newPopup);
    };
    QuizEditorComponent.prototype.addQuestion = function (popup) {
        popup.push(new Game_1.QuizQuestion());
    };
    QuizEditorComponent.prototype.addAnswer = function (question) {
        question.answers.push(new Game_1.QuizAnswer());
    };
    QuizEditorComponent.prototype.deletePopup = function (index) {
        this.popups.splice(index, 1);
    };
    QuizEditorComponent.prototype.deleteQuestion = function (popup, index) {
        popup.splice(index, 1);
    };
    QuizEditorComponent.prototype.deleteAnswer = function (question, index) {
        question.answers.splice(index, 1);
    };
    QuizEditorComponent.prototype.updatePopups = function () {
        try {
            this.popups = JSON.parse(this.popupsText);
            this.popupsText = JSON.stringify(this.popups, null, 4);
        }
        catch (e) { }
    };
    QuizEditorComponent.prototype.save = function () {
        this.socketService.emit(Sockets_1.SocketEvent.updateQuizPopups, this.type, this.popups);
    };
    QuizEditorComponent = __decorate([
        core_1.Component({
            selector: 'quiz-editor',
            templateUrl: './quiz-editor.component.html',
            styleUrls: ['./quiz-editor.component.scss']
        }),
        __metadata("design:paramtypes", [socket_service_1.SocketService,
            router_1.ActivatedRoute])
    ], QuizEditorComponent);
    return QuizEditorComponent;
}());
exports.QuizEditorComponent = QuizEditorComponent;
//# sourceMappingURL=quiz-editor.component.js.map