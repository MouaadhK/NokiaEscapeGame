"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SocketIo = require("socket.io-client");
var SocketService = /** @class */ (function () {
    function SocketService() {
        var _this = this;
        this.globalSocket = SocketIo.connect(); //http://192.168.1.26:3000
        window.addEventListener("beforeunload", function (ev) {
            _this.globalSocket.close();
            //console.log("Socket disconnected");
        });
    }
    SocketService.prototype.onReceive = function (socketEvent, callback) {
        return this.globalSocket.on(socketEvent, function (data) {
            //console.log("Received: " + socketEvent);
            callback(data);
        });
    };
    SocketService.prototype.emit = function (socketEvent) {
        var data = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            data[_i - 1] = arguments[_i];
        }
        (_a = this.globalSocket).emit.apply(_a, [socketEvent].concat(data));
        var _a;
        //console.log("Event emitted: " + socketEvent);
    };
    SocketService.prototype.getSocketId = function () {
        return this.globalSocket.id;
    };
    SocketService.prototype.removeListener = function (socketEvent) {
        this.globalSocket.removeEventListener(socketEvent);
    };
    SocketService.prototype.getGlobalSocket = function () {
        return this.globalSocket;
    };
    SocketService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], SocketService);
    return SocketService;
}());
exports.SocketService = SocketService;
//# sourceMappingURL=socket.service.js.map