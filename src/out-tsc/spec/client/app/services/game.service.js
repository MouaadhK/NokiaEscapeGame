"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Game_1 = require("../../../api/Game");
var socket_service_1 = require("../services/socket.service");
var Sockets_1 = require("../../../api/Sockets");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
var routing_service_1 = require("./routing.service");
var routing_module_1 = require("../routing/routing.module");
var sokoban_service_1 = require("./sokoban.service");
var GameService = /** @class */ (function () {
    function GameService(socketService, routingService, sokobanService) {
        var _this = this;
        this.socketService = socketService;
        this.routingService = routingService;
        this.sokobanService = sokobanService;
        this.availableGames = new Map();
        this.availableGameList = [];
        this.myPlayerProfile = new Game_1.User();
        this.currentGameId = "";
        this.playersInCurrentGame = [];
        this.passphraseMask = "";
        this.myPlayerProfile.type = Game_1.UserType.player;
        this.socketService.emit(Sockets_1.SocketEvent.getAvailableGames);
        this.socketService.onReceive(Sockets_1.SocketEvent.availableGames, function (availableGames) {
            _this.updateAvailableGames(availableGames);
        });
        this.socketService.onReceive(Sockets_1.SocketEvent.playerList, function (players) {
            _this.updatePlayersInCurrentGame(players);
        });
        this.socketService.onReceive(Sockets_1.SocketEvent.timeSync, function (remainingTime) {
            _this.getCurrentGame().remainingTime = remainingTime;
        });
        //Game pause
        this.socketService.onReceive(Sockets_1.SocketEvent.gamePaused, function () {
            _this.showPauseScreen();
        });
        //Game start or resume
        this.socketService.onReceive(Sockets_1.SocketEvent.gameStarted, function (time) {
            if (_this.getCurrentGame().duration.minutes == _this.getCurrentGame().remainingTime.minutes) {
                _this.routingService.closeModal();
            }
            setTimeout((function () {
                _this.routingService.closeSecondLevelModal();
            }).bind(_this), 100);
            setTimeout((_this.sokobanService.focusSokoban).bind(_this.sokobanService), 1000);
        });
        //Game end
        this.socketService.onReceive(Sockets_1.SocketEvent.gameEnded, function (data) {
            if (!_this.isModerator()) {
                if (data.success) {
                    _this.openSuccessScreen();
                }
                else {
                    _this.openFailureScreen();
                }
            }
        });
        //QuizPopup Triggered
        this.socketService.onReceive(Sockets_1.SocketEvent.triggerQuizPopup, function (popup) {
            _this.quizPopup = popup;
            _this.showQuizPopupScreen(_this.quizPopup);
        });
        this.socketService.onReceive(Sockets_1.SocketEvent.startIntroForEveryone, function () {
            _this.routingService.openModal(routing_module_1.AppPaths.modalPaths.intro);
        });
    }
    GameService.prototype.openSuccessScreen = function () {
        this.routingService.openModal(routing_module_1.AppPaths.modalPaths.success);
    };
    GameService.prototype.openFailureScreen = function () {
        this.routingService.openModal(routing_module_1.AppPaths.modalPaths.gameOver);
    };
    GameService.prototype.updateAvailableGames = function (gameList) {
        var _this = this;
        this.availableGameList = this.sortGames(gameList);
        gameList.forEach(function (game, index) {
            _this.availableGames.set(game.id, game);
        });
    };
    GameService.prototype.listAvailableGames = function () {
        return this.availableGameList;
    };
    GameService.prototype.sortGames = function (gameList) {
        gameList.sort(function (game1, game2) {
            if (game2.isJoinable && !game1.isJoinable)
                return 1;
            else
                return -1;
        });
        return gameList;
    };
    GameService.prototype.createNewGame = function (newGame) {
        newGame.id = this.socketService.getSocketId();
        // newGame.duration.seconds = 0;
        // newGame.remainingTime = new Time(newGame.duration.minutes,newGame.duration.seconds);
        newGame.moderator.id = newGame.id;
        newGame.moderator.name = "Moderator";
        newGame.moderator.type = Game_1.UserType.moderator;
        newGame.isModeratorConnected = true;
        this.socketService.emit(Sockets_1.SocketEvent.createNewGame, newGame);
    };
    GameService.prototype.defaultGame = function () {
        var duration = new Game_1.Time(15, 0);
        return {
            name: "",
            id: "",
            description: "",
            type: Game_1.GameTypes.Regular,
            status: "Staging",
            maxPlayerCount: 5,
            currentPlayerCount: 0,
            isJoinable: true,
            duration: duration,
            remainingTime: duration,
            isModeratorConnected: false,
            moderator: new Game_1.User(),
            players: new Map()
        };
    };
    GameService.prototype.updateGameInfo = function (updatedGame) {
        this.socketService.emit(Sockets_1.SocketEvent.updateGameInfo, updatedGame);
    };
    GameService.prototype.updatePlayersInCurrentGame = function (players) {
        this.playersInCurrentGame = players;
    };
    GameService.prototype.getPlayersInCurrentGame = function () {
        return this.playersInCurrentGame;
    };
    GameService.prototype.getCurrentGame = function () {
        return this.availableGames.get(this.currentGameId);
    };
    GameService.prototype.printRemainingTime = function () {
        if (this.getCurrentGame()) {
            return this.formatTime(this.getCurrentGame().remainingTime.minutes, this.getCurrentGame().remainingTime.seconds);
        }
    };
    GameService.prototype.formatTime = function (minutes, seconds) {
        var min = "", sec = "";
        minutes < 10 ? min += '0' + minutes : min += minutes;
        seconds < 10 ? sec += '0' + seconds : sec += seconds;
        return min + ':' + sec;
    };
    GameService.prototype.showPauseScreen = function () {
        this.routingService.openSecondLevelModal(routing_module_1.AppPaths.modalPaths.pauseGame);
    };
    GameService.prototype.showQuizPopupScreen = function (popup) {
        this.routingService.openModal(routing_module_1.AppPaths.modalPaths.quizPopup, { quiz: popup });
    };
    GameService.prototype.hideQuizPopupScreen = function () {
        this.routingService.closeModal();
    };
    GameService.prototype.isJuniorGame = function () {
        return this.getCurrentGame().type === Game_1.GameTypes.Junior;
    };
    GameService.prototype.isModerator = function () {
        return this.myPlayerProfile.type === Game_1.UserType.moderator;
    };
    GameService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [socket_service_1.SocketService,
            routing_service_1.RoutingService,
            sokoban_service_1.SokobanService])
    ], GameService);
    return GameService;
}());
exports.GameService = GameService;
//# sourceMappingURL=game.service.js.map