"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var modal_service_1 = require("./modal.service");
var routing_module_1 = require("../routing/routing.module");
var RoutingService = /** @class */ (function () {
    function RoutingService(router, activatedRoute, modalService) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.modalService = modalService;
        this.baseUrl = "/";
        this.isOnNewGameScreen = false;
        this.isModalOpen = false;
        this.isSecondLevelModalOpen = false;
        history.pushState(null, null, location.href);
        window.onpopstate = function () {
            history.go(1);
        };
    }
    RoutingService.prototype.navigateTo = function (url) {
        this.isOnNewGameScreen = (url == routing_module_1.AppPaths.newGame);
        this.router.navigateByUrl(this.baseUrl + url, { skipLocationChange: true });
    };
    RoutingService.prototype.openModal = function (target, modalData) {
        if (modalData === void 0) { modalData = null; }
        this.modalService.data = modalData;
        this.router.navigate([{ outlets: { modal: [target] } }], { skipLocationChange: true });
        this.isModalOpen = true;
    };
    RoutingService.prototype.closeModal = function () {
        this.router.navigate([{ outlets: { modal: null } }], { skipLocationChange: true });
        this.modalService.data = null;
        this.isModalOpen = false;
    };
    RoutingService.prototype.openSecondLevelModal = function (target, modalData) {
        if (modalData === void 0) { modalData = null; }
        this.modalService.secondLevelData = modalData;
        this.router.navigate([{ outlets: { secondLevelModal: target } }], { skipLocationChange: true });
        this.isSecondLevelModalOpen = true;
    };
    RoutingService.prototype.closeSecondLevelModal = function () {
        this.modalService.secondLevelData = null;
        this.router.navigate([{ outlets: { secondLevelModal: null } }], { skipLocationChange: true });
        this.isSecondLevelModalOpen = false;
    };
    RoutingService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            router_1.ActivatedRoute,
            modal_service_1.ModalService])
    ], RoutingService);
    return RoutingService;
}());
exports.RoutingService = RoutingService;
//# sourceMappingURL=routing.service.js.map