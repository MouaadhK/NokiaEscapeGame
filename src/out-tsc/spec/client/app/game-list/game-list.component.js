"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var game_service_1 = require("../services/game.service");
var routing_service_1 = require("../services/routing.service");
var GameListComponent = /** @class */ (function () {
    function GameListComponent(gameService, routingService) {
        this.gameService = gameService;
        this.routingService = routingService;
    }
    GameListComponent.prototype.ngOnInit = function () {
        if (this.gameService.myPlayerProfile.name == '') {
            this.routingService.navigateTo('/');
        }
    };
    GameListComponent = __decorate([
        core_1.Component({
            selector: 'app-game-list',
            templateUrl: './game-list.component.html',
            styleUrls: ['./game-list.component.scss']
        }),
        __metadata("design:paramtypes", [game_service_1.GameService,
            routing_service_1.RoutingService])
    ], GameListComponent);
    return GameListComponent;
}());
exports.GameListComponent = GameListComponent;
//# sourceMappingURL=game-list.component.js.map