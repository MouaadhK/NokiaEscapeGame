"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Game_1 = require("../../../api/Game");
var Sockets_1 = require("../../../api/Sockets");
var socket_service_1 = require("../services/socket.service");
var game_service_1 = require("../services/game.service");
var ChatBoxComponent = /** @class */ (function () {
    function ChatBoxComponent(socketService, gameService, http) {
        this.socketService = socketService;
        this.gameService = gameService;
        this.http = http;
        this.messages = [];
        this.newMessage = new Game_1.ChatMessage();
    }
    ChatBoxComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.newMessage.content = "";
        this.newMessage.sender = this.gameService.myPlayerProfile;
        this.newMessage.gameId = this.gameService.currentGameId;
        this.socketService.onReceive(Sockets_1.SocketEvent.newChatMessage, function (message) {
            _this.displayMessage(message);
        });
    };
    ChatBoxComponent.prototype.ngAfterViewChecked = function () {
        //this.scrollMessagesToBottom(); 
    };
    ChatBoxComponent.prototype.sendMessage = function () {
        this.newMessage.time = new Date();
        if (this.newMessage.content.trim().length > 0) {
            this.displayMessage(JSON.parse(JSON.stringify(this.newMessage)));
            this.socketService.emit(Sockets_1.SocketEvent.newChatMessage, this.newMessage);
            if (!this.gameService.isModerator()) {
                this.sendToSingularity(this.newMessage.content);
            }
            this.newMessage.content = "";
        }
    };
    ChatBoxComponent.prototype.isFromMe = function (message) {
        return message.sender.id == this.socketService.getSocketId();
        //this.scrollMessagesToBottom();
    };
    ChatBoxComponent.prototype.isFromModerator = function (message) {
        return message.sender.type == Game_1.UserType.moderator;
        //this.scrollMessagesToBottom();
    };
    ChatBoxComponent.prototype.scrollMessagesToBottom = function () {
        this.discussion.nativeElement.scrollTop = this.discussion.nativeElement.scrollHeight;
    };
    ChatBoxComponent.prototype.sendToSingularity = function (message) {
        message = this.replaceAll(message, ' ', '+');
        message = this.replaceAll(message, '%20', '+');
        this.socketService.emit(Sockets_1.SocketEvent.newChatMessage, message);
        // this.http.get('http://139.54.131.105:8000/internal?service=SMARTCHAT&text=' + message).
        // 	map((res) => res.text())
        // 	.subscribe(response => {
        // 		let chatMsg: ChatMessage = {
        // 			gameId: this.gameService.currentGameId,
        // 			content: response.toString(),
        // 			sender: { name: "Singularity", id: "", email: "", type: 1 },
        // 			time: new Date()
        // 		};
        // 		this.displayMessage(chatMsg);
        // 	});
    };
    ChatBoxComponent.prototype.replaceAll = function (str, search, replacement) {
        var target = str;
        return target.replace(new RegExp(search, 'g'), replacement);
    };
    ChatBoxComponent.prototype.displayMessage = function (chatMsg) {
        this.messages.push(chatMsg);
        setTimeout((this.scrollMessagesToBottom).bind(this), 200);
    };
    __decorate([
        core_1.ViewChild("discussion"),
        __metadata("design:type", core_1.ElementRef)
    ], ChatBoxComponent.prototype, "discussion", void 0);
    ChatBoxComponent = __decorate([
        core_1.Component({
            selector: 'chat-box',
            templateUrl: './chat-box.component.html',
            styleUrls: ['./chat-box.component.scss']
        }),
        __metadata("design:paramtypes", [socket_service_1.SocketService,
            game_service_1.GameService,
            http_1.Http])
    ], ChatBoxComponent);
    return ChatBoxComponent;
}());
exports.ChatBoxComponent = ChatBoxComponent;
//# sourceMappingURL=chat-box.component.js.map