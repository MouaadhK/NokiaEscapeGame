"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var moderator_dashboard_component_1 = require("../moderator-dashboard/moderator-dashboard.component");
var player_dashboard_component_1 = require("../player-dashboard/player-dashboard.component");
var landingpage_component_1 = require("../landingpage/landingpage.component");
var quiz_editor_component_1 = require("../quiz-editor/quiz-editor.component");
var intro_component_1 = require("../intro/intro.component");
var success_screen_component_1 = require("../success-screen/success-screen.component");
var failure_screen_component_1 = require("../failure-screen/failure-screen.component");
var pause_screen_component_1 = require("../pause-screen/pause-screen.component");
var quiz_popup_component_1 = require("../quiz-popup/quiz-popup.component");
var user_name_component_1 = require("../user-name/user-name.component");
var game_list_component_1 = require("../game-list/game-list.component");
var new_game_component_1 = require("../new-game/new-game.component");
var waiting_screen_component_1 = require("../waiting-screen/waiting-screen.component");
var modalsPath = "modal:";
exports.AppPaths = {
    moderatorPath: "dashboard",
    playerPath: "play/",
    quizEditor: "editQuiz/:type",
    home: "",
    gameList: "games",
    newGame: "newGame",
    modalPaths: {
        success: modalsPath + "success",
        gameOver: modalsPath + "gameOver",
        intro: modalsPath + "intro",
        quizPopup: modalsPath + "quizPopup",
        pauseGame: modalsPath + "pauseGame",
        waitingScreen: modalsPath + "waitingScreen"
    }
};
var modalOutlet = 'modal';
var secondLevelModalOutlet = 'secondLevelModal';
var appRoutes = [
    {
        path: exports.AppPaths.moderatorPath,
        component: moderator_dashboard_component_1.ModeratorDashboardComponent
    },
    {
        path: exports.AppPaths.playerPath,
        component: player_dashboard_component_1.PlayerDashboardComponent
    },
    {
        path: exports.AppPaths.quizEditor,
        component: quiz_editor_component_1.QuizEditorComponent
    },
    //Modals
    {
        path: exports.AppPaths.modalPaths.intro,
        component: intro_component_1.IntroComponent,
        outlet: modalOutlet
    },
    {
        path: exports.AppPaths.modalPaths.success,
        component: success_screen_component_1.SuccessScreenComponent,
        outlet: modalOutlet
    },
    {
        path: exports.AppPaths.modalPaths.gameOver,
        component: failure_screen_component_1.FailureScreenComponent,
        outlet: modalOutlet
    },
    {
        path: exports.AppPaths.modalPaths.quizPopup,
        component: quiz_popup_component_1.QuizPopupComponent,
        outlet: modalOutlet
    },
    {
        path: exports.AppPaths.modalPaths.waitingScreen,
        component: waiting_screen_component_1.WaitingScreenComponent,
        outlet: modalOutlet
    },
    {
        path: exports.AppPaths.modalPaths.pauseGame,
        component: pause_screen_component_1.PauseScreenComponent,
        outlet: secondLevelModalOutlet
    },
    {
        path: exports.AppPaths.home,
        component: landingpage_component_1.LandingpageComponent,
        children: [
            {
                path: exports.AppPaths.gameList,
                component: game_list_component_1.GameListComponent
            },
            {
                path: exports.AppPaths.newGame,
                component: new_game_component_1.NewGameComponent
            },
            {
                path: '',
                component: user_name_component_1.UserNameComponent
            },
            {
                path: "**",
                redirectTo: ''
            }
        ]
    },
    {
        path: "**",
        redirectTo: exports.AppPaths.home,
    }
];
var RoutingModule = /** @class */ (function () {
    function RoutingModule() {
    }
    RoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forRoot(appRoutes)
            ],
            exports: [
                router_1.RouterModule
            ],
            declarations: []
        })
    ], RoutingModule);
    return RoutingModule;
}());
exports.RoutingModule = RoutingModule;
//# sourceMappingURL=routing.module.js.map