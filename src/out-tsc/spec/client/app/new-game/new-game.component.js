"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Game_1 = require("../../../api/Game");
var routing_service_1 = require("../services/routing.service");
var game_service_1 = require("../services/game.service");
var socket_service_1 = require("../services/socket.service");
var Sockets_1 = require("../../../api/Sockets");
var routing_module_1 = require("../routing/routing.module");
var NewGameComponent = /** @class */ (function () {
    function NewGameComponent(socketService, routingService, gameService) {
        this.socketService = socketService;
        this.routingService = routingService;
        this.gameService = gameService;
        this.game = this.gameService.defaultGame();
        this.gameTypes = [];
        this.possibleNbPlayers = [3, 4, 5, 6];
        this.gameDurationsInMin = [15, 20, 25, 30];
        this.isTitleInvalid = false;
        this.minutes = 15;
    }
    NewGameComponent.prototype.ngOnInit = function () {
        for (var type in Game_1.GameTypes) {
            this.gameTypes.push(type);
        }
    };
    NewGameComponent.prototype.select = function (gameType) {
        this.game.type = gameType;
    };
    NewGameComponent.prototype.createNewGame = function () {
        var _this = this;
        if (this.game.name) {
            this.game.duration.minutes = this.minutes;
            if (typeof (this.game.duration.minutes) == "string") {
                this.game.duration.minutes = parseInt(this.game.duration.minutes);
            }
            this.gameService.createNewGame(this.game);
            this.socketService.onReceive(Sockets_1.SocketEvent.joinGameAsModerator, function (gameId) {
                _this.gameService.currentGameId = gameId;
                _this.gameService.myPlayerProfile = _this.game.moderator;
                _this.gameService.availableGames.set(gameId, _this.game);
                _this.routingService.navigateTo(routing_module_1.AppPaths.moderatorPath);
            });
        }
        else {
            this.isTitleInvalid = true;
        }
    };
    NewGameComponent.prototype.checkTitle = function () {
        this.isTitleInvalid = this.game.name == '';
    };
    NewGameComponent.prototype.cancel = function () {
        this.routingService.navigateTo("");
    };
    NewGameComponent = __decorate([
        core_1.Component({
            selector: 'app-new-game',
            templateUrl: './new-game.component.html',
            styleUrls: ['./new-game.component.scss']
        }),
        __metadata("design:paramtypes", [socket_service_1.SocketService,
            routing_service_1.RoutingService,
            game_service_1.GameService])
    ], NewGameComponent);
    return NewGameComponent;
}());
exports.NewGameComponent = NewGameComponent;
//# sourceMappingURL=new-game.component.js.map