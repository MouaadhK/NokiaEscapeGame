"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var neuralNeworkBuilder_1 = require("../../assets/libraries/BYONND/src/neuralNeworkBuilder");
var Game_1 = require("../../../api/Game");
var FubukiComponent = /** @class */ (function () {
    function FubukiComponent() {
    }
    FubukiComponent.prototype.ngOnInit = function () {
        var lvl = [[5, 2, 1], [7, 3, 8], [6, 9, 4]];
        var lvl2 = [[1, 5, 9], [4, 6, 2], [7, 3, 8]];
        var lvl3 = [[1, 7, 2], [4, 8, 9], [3, 6, 5]];
        var lvl4 = [[1, 6, 3], [7, 8, 4], [5, 9, 2]];
        fubukiLevel: Game_1.FubukiLevel[] = [
            {
                name: 'Level 1',
                difficulty: Game_1.SokobanLevelDifficulty.Easy,
                passphraseSegment: '',
                index: 0,
                game: lvl
            },
            {
                name: 'Level 2',
                difficulty: Game_1.SokobanLevelDifficulty.Medium,
                passphraseSegment: '',
                index: 1,
                game: lvl2
            },
            {
                name: 'Level 3',
                difficulty: Game_1.SokobanLevelDifficulty.Hard,
                passphraseSegment: '',
                index: 2,
                game: lvl3
            },
            {
                name: 'Level 4',
                difficulty: Game_1.SokobanLevelDifficulty.Expert,
                passphraseSegment: '',
                index: 3,
                game: lvl4
            }
        ];
        this.canvas = this.canvas.nativeElement;
        this.canvas.getContext('2d');
        this.neuralNetworkBuilder = new neuralNeworkBuilder_1.NeuralNetworkBuilder(this.canvas);
        this.fubuki = this.neuralNetworkBuilder.build('fubuki');
        this.fubuki.define([4, 4, 4, 4]);
        for (var i = 0; i < this.fubuki.layers.length; i++) {
            for (var j = 0; j < this.fubuki.layers[i].neurons.length; j++) {
                if (i === this.fubuki.layers.length - 1 || j === this.fubuki.layers[i].neurons.length - 1) {
                    this.fubuki.layers[i].neurons[j].activate();
                }
            }
        }
        // --------------------------------- HTML/CSS
        var btn;
        btn = document.createElement('button');
        var t = document.createTextNode('GO'); // Create a text node
        btn.style.fontSize = '20px';
        btn.style.fontStyle = 'oblique';
        btn.appendChild(t); // Append the text to <button>
        var inputs;
        inputs = document.createElement('input');
        inputs.style.marginLeft = this.fubuki.layers[0].neurons[0].x + 'px';
        inputs.style.marginTop = this.fubuki.layers[0].neurons[0].y + 'px';
        btn.style.background = 'transparent';
        btn.style.border = 'transparent';
        btn.style.marginLeft = this.fubuki.layers[3].neurons[3].x + 'px';
        btn.style.marginTop = this.fubuki.layers[3].neurons[3].y + 'px';
        btn.style.width = '25px';
        btn.style.height = '40px';
        btn.style.zIndex = '2';
        btn.style.position = 'absolute';
        document.getElementById('test').appendChild(btn);
        // -------------------------------------
        this.fubuki.draw();
        //--------------------------INPUT style (position )
        var input = [document.createElement('input'), document.createElement('input'), document.createElement('input'), document.createElement('input'), document.createElement('input'), document.createElement('input'), document.createElement('input'), document.createElement('input'), document.createElement('input'), document.createElement('input'), document.createElement('input'), document.createElement('input'), document.createElement('input'), document.createElement('input')];
        var z = 0;
        for (var i = 0; i < 3; i++) {
            for (var j = 0; j < 3; j++) {
                input[z].setAttribute('type', 'text');
                input[z].style.marginLeft = this.fubuki.layers[i].neurons[j].x - 10 + 'px'; //READ ONLY pour qui sont fixer
                input[z].style.marginTop = this.fubuki.layers[i].neurons[j].y - 27 + 'px';
                input[z].style.fontSize = '30px';
                //input[z].style.fontStyle = "oblique";
                input[z].style.width = '25px';
                input[z].style.height = '40px';
                input[z].style.zIndex = '2';
                input[z].style.position = 'absolute';
                input[z].readOnly = false;
                input[z].style.background = 'transparent';
                input[z].style.border = 'transparent';
                document.getElementById('test').appendChild(input[z]);
                z++;
            }
            z++;
        }
        //---------------------------
        function getRandomInt(max) {
            return Math.floor(Math.random() * Math.floor(max));
        }
        this.fubuki.layers[0].neurons[0].setValue(getRandomInt(10).toString());
        this.fubuki.layers[0].neurons[1].setValue(getRandomInt(10).toString());
        this.fubuki.layers[0].neurons[2].setValue(getRandomInt(10).toString());
        this.fubuki.layers[1].neurons[0].setValue(getRandomInt(10).toString());
        this.fubuki.layers[1].neurons[1].setValue(getRandomInt(10).toString());
        this.fubuki.layers[1].neurons[2].setValue(getRandomInt(10).toString());
        this.fubuki.layers[2].neurons[0].setValue(getRandomInt(10).toString());
        this.fubuki.layers[2].neurons[1].setValue(getRandomInt(10).toString());
        this.fubuki.layers[2].neurons[2].setValue(getRandomInt(10).toString());
        //---------------**** Give the value of neurons in the list of value fixed
        /*
         for (let i = 0; i < this.fubuki.layers.length; i++) {
          for (let j = 0; j < this.fubuki.layers[i].neurons.length; j++) {
            this.fubuki.layers[i].neurons[j].setValue(fubukiLevel.game.lvl[i][j].toString());
          }
        }
    //---------------------------------*/
        //---------------- Calculate the sum of the random value and set in the fubuki layers
        var line1 = parseInt(this.fubuki.layers[0].neurons[0].value.valueOf()) + parseInt(this.fubuki.layers[0].neurons[1].value.valueOf()) + parseInt(this.fubuki.layers[0].neurons[2].value.valueOf());
        var line2 = parseInt(this.fubuki.layers[1].neurons[0].value.valueOf()) + parseInt(this.fubuki.layers[1].neurons[1].value.valueOf()) + parseInt(this.fubuki.layers[1].neurons[2].value.valueOf());
        var line3 = parseInt(this.fubuki.layers[2].neurons[0].value.valueOf()) + parseInt(this.fubuki.layers[2].neurons[1].value.valueOf()) + parseInt(this.fubuki.layers[2].neurons[2].value.valueOf());
        var column1 = parseInt(this.fubuki.layers[0].neurons[0].value.valueOf()) + parseInt(this.fubuki.layers[1].neurons[0].value.valueOf()) + parseInt(this.fubuki.layers[2].neurons[0].value.valueOf());
        var column2 = parseInt(this.fubuki.layers[0].neurons[1].value.valueOf()) + parseInt(this.fubuki.layers[1].neurons[1].value.valueOf()) + parseInt(this.fubuki.layers[2].neurons[1].value.valueOf());
        var column3 = parseInt(this.fubuki.layers[0].neurons[2].value.valueOf()) + parseInt(this.fubuki.layers[1].neurons[2].value.valueOf()) + parseInt(this.fubuki.layers[2].neurons[2].value.valueOf());
        this.fubuki.layers[0].neurons[3].setValue(line1);
        this.fubuki.layers[1].neurons[3].setValue(line2);
        this.fubuki.layers[2].neurons[3].setValue(line3);
        this.fubuki.layers[3].neurons[0].setValue(column1);
        this.fubuki.layers[3].neurons[1].setValue(column2);
        this.fubuki.layers[3].neurons[2].setValue(column3);
        function hideneurons(index) {
            if (index === 0) {
                this.fubuki.layers[0].neurons[1].setValue('');
                this.fubuki.layers[1].neurons[2].setValue('');
                this.fubuki.layers[2].neurons[0].setValue('');
            }
            else if (index === 1) {
                this.fubuki.layers[0].neurons[1].setValue('');
                this.fubuki.layers[1].neurons[2].setValue('');
                this.fubuki.layers[1].neurons[0].setValue('');
                this.fubuki.layers[2].neurons[0].setValue('');
                this.fubuki.layers[2].neurons[2].setValue('');
            }
            else if (index === 2) {
                this.fubuki.layers[0].neurons[1].setValue('');
                this.fubuki.layers[1].neurons[2].setValue('');
                this.fubuki.layers[1].neurons[0].setValue('');
                this.fubuki.layers[2].neurons[0].setValue('');
                this.fubuki.layers[2].neurons[1].setValue('');
                this.fubuki.layers[2].neurons[2].setValue('');
            }
            else if (index === 3) {
                this.fubuki.layers[0].neurons[1].setValue('');
                this.fubuki.layers[1].neurons[2].setValue('');
                this.fubuki.layers[1].neurons[0].setValue('');
                this.fubuki.layers[2].neurons[0].setValue('');
                this.fubuki.layers[2].neurons[1].setValue('');
                this.fubuki.layers[2].neurons[2].setValue('');
            }
        }
        hideneurons(0);
        // hideneurons(fubukiLevel.index);
        //---------------------------------
        this.fubuki.draw();
        //Check the winning time
        btn.onclick = function () {
            if ((line1 === parseInt(this.fubuki.layers[0].neurons[3].value.valueOf())) && (line2 === parseInt(this.fubuki.layers[1].neurons[3].value.valueOf())) && (line3 === parseInt(this.fubuki.layers[2].neurons[3].value.valueOf())) && (column1 === parseInt(this.fubuki.layers[3].neurons[0].value.valueOf())) && (column2 === parseInt(this.fubuki.layers[3].neurons[1].value.valueOf())) && (column3 === parseInt(this.fubuki.layers[3].neurons[2].value.valueOf()))) {
                console.log('Game on 1 DONE ');
                //---------- Clean the input
                this.fubuki.layers[0].neurons[0].setValue('Y');
                this.fubuki.layers[0].neurons[1].setValue('O');
                this.fubuki.layers[0].neurons[2].setValue('U');
                this.fubuki.layers[1].neurons[0].setValue('W');
                this.fubuki.layers[1].neurons[1].setValue('I');
                this.fubuki.layers[1].neurons[2].setValue('N');
                this.fubuki.layers[0].neurons[3].setValue('');
                this.fubuki.layers[1].neurons[3].setValue('');
                this.fubuki.layers[2].neurons[3].setValue('');
                this.fubuki.layers[3].neurons[0].setValue('');
                this.fubuki.layers[3].neurons[1].setValue('');
                this.fubuki.layers[3].neurons[2].setValue('');
                this.fubuki.layers[2].neurons[0].setValue('');
                this.fubuki.layers[2].neurons[1].setValue('');
                this.fubuki.layers[2].neurons[2].setValue('');
                // -------------Send a random part of the answer at each time win in the console and recall this fubuki
            }
            this.fubuki.draw();
        };
    };
    __decorate([
        core_1.ViewChild('canvas'),
        __metadata("design:type", Object)
    ], FubukiComponent.prototype, "canvas", void 0);
    FubukiComponent = __decorate([
        core_1.Component({
            selector: 'app-fubuki',
            templateUrl: './fubuki.component.html',
            styleUrls: ['./fubuki.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], FubukiComponent);
    return FubukiComponent;
}());
exports.FubukiComponent = FubukiComponent;
//# sourceMappingURL=fubuki.component.js.map