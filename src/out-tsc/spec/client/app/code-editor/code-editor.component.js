"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Game_1 = require("../../../api/Game");
var socket_service_1 = require("../services/socket.service");
var game_service_1 = require("../services/game.service");
var routing_service_1 = require("../services/routing.service");
var CodeMirror = require("codemirror");
var code_service_1 = require("../services/code.service");
require("codemirror/mode/clike/clike");
// import * as ot from '../../../server/lib/ot/index';
var CodeEditorComponent = /** @class */ (function () {
    function CodeEditorComponent(socketService, gameService, routingService, codeService) {
        this.socketService = socketService;
        this.gameService = gameService;
        this.routingService = routingService;
        this.codeService = codeService;
        this.readOnly = false;
        this.phraseToEncrypt = "";
        this.isPlayer = false;
        this.readonly = "";
        this.canUndo = false;
        this.canRedo = false;
        this.isPlayer = this.gameService.myPlayerProfile.type == Game_1.UserType.player;
    }
    CodeEditorComponent.prototype.onresize = function (event) {
        this.resizeCodeEditor();
    };
    CodeEditorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isPlayer ? this.readonly = "" : this.readonly = "nocursor";
        this.socketService.onReceive('doc', function (obj) {
            _this.init(obj.str, obj.revision, obj.clients, new ot.SocketIOAdapter(_this.socketService.getGlobalSocket()));
            if (_this.isPlayer) {
                _this.codeService.code.gameId = _this.gameService.getCurrentGame().id;
                _this.codeService.code.value = obj.str;
            }
            ot.gameService = _this.gameService;
            _this.resizeCodeEditor();
        });
    };
    CodeEditorComponent.prototype.ngAfterViewChecked = function () {
        if (this.codeMirrorEditor)
            this.resizeCodeEditor();
    };
    CodeEditorComponent.prototype.init = function (str, revision, clients, serverAdapter) {
        // this.codeMirrorEditor.setValue(str);
        // this.editorConfig = ;
        this.codeMirrorEditor = CodeMirror(this.codeEditor.nativeElement, {
            lineNumbers: true,
            lineWrapping: true,
            readOnly: this.readonly,
            mode: "text/x-c++src",
            theme: 'monokai'
        });
        this.codeMirrorEditor.setValue(str);
        this.cmClient = new ot.EditorClient(revision, clients, serverAdapter, new ot.CodeMirrorAdapter(this.codeMirrorEditor));
        // var userListWrapper = document.getElementById('userlist-wrapper');
        // userListWrapper.appendChild(cmClient.clientListEl);
        var _this = this;
        this.codeMirrorEditor.on('change', function () {
            _this.codeService.code.value = _this.codeMirrorEditor.getValue();
            _this.cmClient.undoManager.canUndo() ? _this.canUndo = true : _this.canUndo = false;
            _this.cmClient.undoManager.canRedo() ? _this.canRedo = true : _this.canRedo = false;
            _this.codeService.emitModifySubject();
        });
    };
    CodeEditorComponent.prototype.resizeCodeEditor = function () {
        var boxHeight = document.getElementsByClassName('gameBox')[0].clientHeight;
        var boxWidth = document.getElementsByClassName('gameBox')[0].clientWidth;
        var controlsHeight = document.getElementsByClassName('controls')[0].clientHeight;
        // console.log(boxHeight, boxWidth, controlsHeight);
        if (this.codeMirrorEditor) {
            this.codeMirrorEditor.setSize('98%', (boxHeight - controlsHeight - 8));
        }
    };
    CodeEditorComponent.prototype.undo = function () {
        this.codeMirrorEditor.undo();
    };
    CodeEditorComponent.prototype.redo = function () {
        this.codeMirrorEditor.redo();
    };
    CodeEditorComponent.prototype.setCharAt = function (str, index, chr) {
        if (index > str.length - 1)
            return str;
        return str.substr(0, index) + chr + str.substr(index + 1);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], CodeEditorComponent.prototype, "readOnly", void 0);
    __decorate([
        core_1.ViewChild("codeEditor"),
        __metadata("design:type", core_1.ElementRef)
    ], CodeEditorComponent.prototype, "codeEditor", void 0);
    __decorate([
        core_1.HostListener('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], CodeEditorComponent.prototype, "onresize", null);
    CodeEditorComponent = __decorate([
        core_1.Component({
            selector: 'code-editor',
            templateUrl: './code-editor.component.html',
            styleUrls: ['./code-editor.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [socket_service_1.SocketService,
            game_service_1.GameService,
            routing_service_1.RoutingService,
            code_service_1.CodeService])
    ], CodeEditorComponent);
    return CodeEditorComponent;
}());
exports.CodeEditorComponent = CodeEditorComponent;
//# sourceMappingURL=code-editor.component.js.map