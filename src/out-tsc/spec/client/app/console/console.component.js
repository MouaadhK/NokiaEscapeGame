"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Game_1 = require("../../../api/Game");
var Sockets_1 = require("../../../api/Sockets");
var socket_service_1 = require("../services/socket.service");
var game_service_1 = require("../services/game.service");
var routing_service_1 = require("../services/routing.service");
var code_service_1 = require("../services/code.service");
var ConsoleComponent = /** @class */ (function () {
    function ConsoleComponent(socketService, gameService, routingService, codeService) {
        this.socketService = socketService;
        this.gameService = gameService;
        this.routingService = routingService;
        this.codeService = codeService;
        this.readOnly = false;
        this.shellText = "";
        this.phraseToEncrypt = "";
        this.binaryWasCreated = false;
        this.inCompileTime = false;
    }
    ConsoleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.modifySubscription = this.codeService.modifySubject.subscribe(function () {
            _this.binaryWasCreated = false;
        });
        if (!this.gameService.isModerator()) {
            this.printInConsole(this.gameService.myPlayerProfile.name, "", false);
        }
        if (!this.gameService.isModerator()) {
            this.socketService.onReceive(Sockets_1.SocketEvent.sokobanLevelCompletePlayer, function (segment) {
                var phrase = "You completed this level! You have unlocked the following segment of the passphrase : <span style='color: #39B54A;'>" + segment + "</span> ! Complete the other levels to discover the full passphrase !";
                _this.printInConsole(_this.gameService.myPlayerProfile.name, phrase);
            });
        }
        else {
            this.socketService.onReceive(Sockets_1.SocketEvent.sokobanLevelCompleteModerator, function (data) {
                var phrase = "Completed the " + data.level.difficulty + " level \"" + data.level.name + "\" and unlocked the passphrase segment \"" + data.segment + "\".";
                _this.printInConsole(data.playerName, phrase);
            });
        }
        this.socketService.onReceive(Sockets_1.SocketEvent.newPlayerForModerator, function (player) {
            var phrase = 'Joined the game';
            _this.printInConsole(player.name, phrase);
        });
        this.socketService.onReceive(Sockets_1.SocketEvent.leavingPlayerForModerator, function (player) {
            var phrase = 'Left the game';
            _this.printInConsole(player.name, phrase);
        });
        this.socketService.onReceive(Sockets_1.SocketEvent.wrongPassphrase, function () {
            _this.printInConsole(_this.gameService.myPlayerProfile.name, "Incorrect passphrase");
        });
        this.socketService.onReceive(Sockets_1.SocketEvent.gameEnded, function (data) {
            if (data.success) {
                var winTime = Game_1.Time.prototype.substract(_this.gameService.getCurrentGame().duration, _this.gameService.getCurrentGame().remainingTime);
                var phrase = "Victory! Passphrase found in " + winTime.minutes + " minutes and " + winTime.seconds + " seconds!";
                _this.printInConsole(data.player.name, phrase);
            }
            else {
                _this.printInConsole("All", "Time elapsed... Players disconnecting");
            }
            _this.socketService.removeListener(Sockets_1.SocketEvent.gameEnded);
        });
        this.socketService.onReceive(Sockets_1.SocketEvent.compilationResult, function (compilationResult) {
            _this.inCompileTime = false;
            if (compilationResult.player.name) {
                _this.printInConsole(compilationResult.player.name, compilationResult.stderr + compilationResult.stdout);
                _this.binaryWasCreated = compilationResult.binaryWasCreated;
            }
            else {
                _this.binaryWasCreated = compilationResult.binaryWasCreated;
                _this.printInConsole(_this.gameService.myPlayerProfile.name, compilationResult.stderr + compilationResult.stdout);
            }
        });
        this.socketService.onReceive(Sockets_1.SocketEvent.executionLog, function (executionLog) {
            // this.printInConsole(execLog.stderr + execLog.stdout);
            if (executionLog.player.name) {
                _this.printInConsole(executionLog.player.name, executionLog.stderr + executionLog.stdout);
            }
            else {
                _this.printInConsole(_this.gameService.myPlayerProfile.name, executionLog.stderr + executionLog.stdout);
            }
        });
    };
    ConsoleComponent.prototype.scrollConsoleToBottom = function () {
        this.console.nativeElement.scrollTop = this.console.nativeElement.scrollHeight;
    };
    ConsoleComponent.prototype.check = function () {
        var checkType = {
            passphrase: this.phraseToEncrypt,
            gameId: this.gameService.getCurrentGame().id,
            player: this.gameService.myPlayerProfile,
            gameType: this.gameService.getCurrentGame().type
        };
        this.socketService.emit(Sockets_1.SocketEvent.checkPassphrase, checkType);
    };
    ConsoleComponent.prototype.printInConsole = function (username, text, newLine) {
        if (newLine === void 0) { newLine = true; }
        var toPrint = "";
        if (this.gameService.isModerator()) {
            toPrint = "<span style='color: #007BEA;'> " + username + "@Nokia~$ </span>" + text + "<br>";
        }
        else {
            toPrint = text + ((newLine ? '<br><br>' : '') + "<span style='color: #007BEA;'> " + username + "@Nokia~$ </span>");
        }
        this.console.nativeElement.insertAdjacentHTML('beforeend', toPrint);
        this.scrollConsoleToBottom();
    };
    ConsoleComponent.prototype.compile = function () {
        this.inCompileTime = true;
        this.socketService.emit(Sockets_1.SocketEvent.compileCode, this.codeService.code);
    };
    ConsoleComponent.prototype.run = function () {
        var passphraseNormalize = this.phraseToEncrypt.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase();
        // console.log(passphraseNormalize);
        this.socketService.emit(Sockets_1.SocketEvent.runCode, passphraseNormalize);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ConsoleComponent.prototype, "readOnly", void 0);
    __decorate([
        core_1.ViewChild("console"),
        __metadata("design:type", core_1.ElementRef)
    ], ConsoleComponent.prototype, "console", void 0);
    ConsoleComponent = __decorate([
        core_1.Component({
            selector: 'console',
            templateUrl: './console.component.html',
            styleUrls: ['./console.component.scss']
        }),
        __metadata("design:paramtypes", [socket_service_1.SocketService,
            game_service_1.GameService,
            routing_service_1.RoutingService,
            code_service_1.CodeService])
    ], ConsoleComponent);
    return ConsoleComponent;
}());
exports.ConsoleComponent = ConsoleComponent;
//# sourceMappingURL=console.component.js.map