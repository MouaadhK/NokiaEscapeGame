"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var game_service_1 = require("../services/game.service");
var routing_service_1 = require("../services/routing.service");
var routing_module_1 = require("../routing/routing.module");
var UserNameComponent = /** @class */ (function () {
    function UserNameComponent(gameService, routingService) {
        this.gameService = gameService;
        this.routingService = routingService;
        this.isNameInvalid = false;
    }
    UserNameComponent.prototype.ngOnInit = function () {
        if (this.gameService.myPlayerProfile.name) {
            this.routingService.navigateTo(routing_module_1.AppPaths.gameList);
        }
    };
    UserNameComponent.prototype.onKeyPress = function (event) {
        if (event.which === 13) {
            this.goToGameList();
        }
    };
    UserNameComponent.prototype.goToGameList = function () {
        if (this.gameService.myPlayerProfile.name) {
            this.isNameInvalid = false;
            this.routingService.navigateTo(routing_module_1.AppPaths.gameList);
        }
        else {
            this.isNameInvalid = true;
        }
    };
    UserNameComponent.prototype.checkName = function () {
        this.isNameInvalid = this.gameService.myPlayerProfile.name == '';
    };
    UserNameComponent = __decorate([
        core_1.Component({
            selector: 'app-user-name',
            templateUrl: './user-name.component.html',
            styleUrls: ['./user-name.component.scss']
        }),
        __metadata("design:paramtypes", [game_service_1.GameService,
            routing_service_1.RoutingService])
    ], UserNameComponent);
    return UserNameComponent;
}());
exports.UserNameComponent = UserNameComponent;
//# sourceMappingURL=user-name.component.js.map