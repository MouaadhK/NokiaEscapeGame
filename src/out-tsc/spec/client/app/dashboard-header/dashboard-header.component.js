"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var game_service_1 = require("../services/game.service");
var routing_service_1 = require("../services/routing.service");
var routing_module_1 = require("../routing/routing.module");
var Game_1 = require("../../../api/Game");
var DashboardHeaderComponent = /** @class */ (function () {
    function DashboardHeaderComponent(gameService, routingService) {
        this.gameService = gameService;
        this.routingService = routingService;
        this.volumeOn = false;
        this.music = new Audio("../../assets/escape_game_music.mp3");
        this.isModerator = false;
    }
    DashboardHeaderComponent.prototype.ngOnInit = function () {
        this.isModerator = this.gameService.myPlayerProfile.type == Game_1.UserType.moderator;
        this.music.load();
        this.music.loop = true;
    };
    DashboardHeaderComponent.prototype.startMusic = function () {
        this.volumeOn = true;
        this.music.play();
    };
    DashboardHeaderComponent.prototype.stopMusic = function () {
        this.volumeOn = false;
        this.music.pause();
    };
    DashboardHeaderComponent.prototype.goToLandingPage = function () {
        this.routingService.navigateTo(routing_module_1.AppPaths.home);
    };
    DashboardHeaderComponent = __decorate([
        core_1.Component({
            selector: 'app-dashboard-header',
            templateUrl: './dashboard-header.component.html',
            styleUrls: ['./dashboard-header.component.scss']
        }),
        __metadata("design:paramtypes", [game_service_1.GameService,
            routing_service_1.RoutingService])
    ], DashboardHeaderComponent);
    return DashboardHeaderComponent;
}());
exports.DashboardHeaderComponent = DashboardHeaderComponent;
//# sourceMappingURL=dashboard-header.component.js.map