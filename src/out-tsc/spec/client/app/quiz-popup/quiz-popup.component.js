"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var routing_service_1 = require("../services/routing.service");
var modal_service_1 = require("../services/modal.service");
var routing_module_1 = require("../routing/routing.module");
var QuizPopupComponent = /** @class */ (function () {
    function QuizPopupComponent(routingService, modalService) {
        this.routingService = routingService;
        this.modalService = modalService;
        this.currentQuestionIndex = 0;
        this.score = 0;
        this.questions = [];
        this.success = false;
        this.afterQuizTimeOut = 3000;
    }
    QuizPopupComponent.prototype.ngOnInit = function () {
        if (this.modalService.data) {
            this.questions = this.modalService.data.quiz;
        }
    };
    QuizPopupComponent.prototype.choseAnswer = function (answer) {
        if (answer.isCorrect) {
            this.score++;
        }
        this.currentQuestionIndex++;
        if (this.currentQuestionIndex == this.questions.length) {
            if (this.score >= Math.ceil(2 * this.questions.length / 3)) {
                this.success = true;
                setTimeout(this.continueGame.bind(this), this.afterQuizTimeOut);
            }
            else {
                this.success = false;
                setTimeout(this.goToFailureScreen.bind(this), this.afterQuizTimeOut);
            }
        }
    };
    QuizPopupComponent.prototype.continueGame = function () {
        this.routingService.closeModal();
    };
    QuizPopupComponent.prototype.goToFailureScreen = function () {
        var _this = this;
        this.routingService.closeModal();
        setTimeout((function () {
            _this.routingService.openModal(routing_module_1.AppPaths.modalPaths.gameOver);
        }).bind(this), 10);
    };
    QuizPopupComponent = __decorate([
        core_1.Component({
            selector: 'app-quiz-popup',
            templateUrl: './quiz-popup.component.html',
            styleUrls: ['./quiz-popup.component.scss']
        }),
        __metadata("design:paramtypes", [routing_service_1.RoutingService,
            modal_service_1.ModalService])
    ], QuizPopupComponent);
    return QuizPopupComponent;
}());
exports.QuizPopupComponent = QuizPopupComponent;
//# sourceMappingURL=quiz-popup.component.js.map