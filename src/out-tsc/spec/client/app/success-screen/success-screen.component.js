"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Game_1 = require("../../../api/Game");
var game_service_1 = require("../services/game.service");
var SuccessScreenComponent = /** @class */ (function () {
    function SuccessScreenComponent(gameService, renderer) {
        this.gameService = gameService;
        this.renderer = renderer;
        this.displayMessage = [true, false, false, false];
        this.currentIndex = 0;
        this.hideButton = false;
        this.isJunior = false;
        this.winTime = Game_1.Time.prototype.substract(this.gameService.getCurrentGame().duration, this.gameService.getCurrentGame().remainingTime);
        this.isJunior = this.gameService.getCurrentGame().type == Game_1.GameTypes.Junior;
    }
    SuccessScreenComponent.prototype.ngOnInit = function () {
        this.incrementAnimationText();
    };
    SuccessScreenComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.renderer.listen(this.animationEnd.nativeElement, 'animationend', function () {
            _this.renderer.addClass(_this.animationEnd.nativeElement, 'animation-done');
        });
    };
    SuccessScreenComponent.prototype.incrementAnimationText = function () {
        var _this = this;
        if (this.currentIndex < this.displayMessage.length - 1) {
            setTimeout((function () {
                _this.displayMessage[++_this.currentIndex] = true;
                _this.incrementAnimationText();
            }).bind(this), 3500);
            if (this.currentIndex > 0) {
                this.hideCursor(this.currentIndex);
            }
        }
    };
    SuccessScreenComponent.prototype.hideCursor = function (index) {
        var _this = this;
        switch (index) {
            case 1:
                document.getElementById('animationEnd2').addEventListener('animationend', function () {
                    document.getElementById('animationEnd2').classList.add('animation-done');
                });
                break;
            case 2:
                document.getElementById('animationEnd3').addEventListener('animationend', function () {
                    document.getElementById('animationEnd3').classList.add('animation-done');
                });
                break;
        }
        var element = this.isJunior ? document.getElementById('animationEnd4') : document.getElementById('animationEnd3');
        if (element) {
            element.addEventListener('animationend', function () {
                _this.hideButton = true;
            });
        }
    };
    SuccessScreenComponent.prototype.goToLandingPage = function () {
        location.reload();
    };
    __decorate([
        core_1.ViewChild("animationEnd"),
        __metadata("design:type", core_1.ElementRef)
    ], SuccessScreenComponent.prototype, "animationEnd", void 0);
    SuccessScreenComponent = __decorate([
        core_1.Component({
            selector: 'app-success-screen',
            templateUrl: './success-screen.component.html',
            styleUrls: ['./success-screen.component.scss']
        }),
        __metadata("design:paramtypes", [game_service_1.GameService,
            core_1.Renderer2])
    ], SuccessScreenComponent);
    return SuccessScreenComponent;
}());
exports.SuccessScreenComponent = SuccessScreenComponent;
//# sourceMappingURL=success-screen.component.js.map