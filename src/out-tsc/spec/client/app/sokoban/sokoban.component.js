"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var jQuery = require("jquery");
var sokoban_service_1 = require("../services/sokoban.service");
var Game_1 = require("../../../api/Game");
var game_service_1 = require("../services/game.service");
var socket_service_1 = require("../services/socket.service");
var Sockets_1 = require("../../../api/Sockets");
var SokobanComponent = /** @class */ (function () {
    function SokobanComponent(sokobanService, gameService, socketService, renderer) {
        this.sokobanService = sokobanService;
        this.gameService = gameService;
        this.socketService = socketService;
        this.renderer = renderer;
        this.sokobanLevels = [
            {
                name: "Level 1",
                difficulty: Game_1.SokobanLevelDifficulty.Easy,
                passphraseSegment: "",
                index: 0,
                game: " #######\n #     #\n # .$. #\n## $@$ #\n#  .$. #\n#      #\n########"
            },
            {
                name: "Level 2",
                difficulty: Game_1.SokobanLevelDifficulty.Medium,
                passphraseSegment: "",
                index: 1,
                game: "###### #####\n#    ###   #\n# $$     #@#\n# $ #...   #\n#   ########\n#####"
            },
            {
                name: "Level 3",
                difficulty: Game_1.SokobanLevelDifficulty.Hard,
                passphraseSegment: "",
                index: 2,
                game: "  ######\n  # ..@#\n  # $$ #\n  ## ###\n   # #\n   # #\n#### #\n#    ##\n# #   #\n#   # #\n###   #\n  #####"
            },
            {
                name: "Level 4",
                difficulty: Game_1.SokobanLevelDifficulty.Expert,
                passphraseSegment: "",
                index: 3,
                game: "      #####\n      #   ##\n      # $  #\n######## #@##\n# .  # $ $  #\n#        $# #\n#...#####   #\n#####   #####"
            }
        ];
        this.currentIndex = this.generateRandomInt(0, this.sokobanLevels.length - 1); //Exclude last level (expert) from first round
        this.gameIndex = this.currentIndex;
        this.solved = false;
        this.isGraphicLoaded = false;
    }
    SokobanComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sokobanService.sokobanSubject.subscribe(function () {
            _this.nextLevel();
        });
        this.initSokoban();
    };
    SokobanComponent.prototype.initSokoban = function () {
        this.sokobanService.init(jQuery);
        var _this = this;
        jQuery(function ($) {
            $('pre.sokoban').sokoban();
            _this.isGraphicLoaded = true;
        });
    };
    SokobanComponent.prototype.nextLevel = function () {
        var _this = this;
        this.solved = true;
        this.announceLevelComplete(this.currentIndex);
        setTimeout((function () {
            _this.sokobanLevels.splice(_this.currentIndex, 1);
            _this.solved = false;
            if (_this.sokobanLevels.length > 0) {
                _this.currentIndex = _this.generateRandomInt(0, _this.sokobanLevels.length);
                _this.gameIndex = _this.sokobanLevels[_this.currentIndex].index;
                _this.sokobanService.focusSokoban();
            }
            else {
                _this.currentIndex = -1;
            }
        }).bind(this), 2000);
    };
    SokobanComponent.prototype.announceLevelComplete = function (index) {
        var sokobanLevelData = {
            gameId: this.gameService.getCurrentGame().id,
            index: this.sokobanLevels[index].index,
            level: this.sokobanLevels[index]
        };
        this.socketService.emit(Sockets_1.SocketEvent.sokobanLevelComplete, sokobanLevelData);
    };
    SokobanComponent.prototype.generateRandomInt = function (min, maxExcl) {
        return min + Math.floor(Math.random() * (maxExcl - min));
    };
    SokobanComponent = __decorate([
        core_1.Component({
            selector: 'app-sokoban',
            templateUrl: './sokoban.component.html',
            styleUrls: ['./sokoban.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None //make the CSS working
        }),
        __metadata("design:paramtypes", [sokoban_service_1.SokobanService,
            game_service_1.GameService,
            socket_service_1.SocketService,
            core_1.Renderer])
    ], SokobanComponent);
    return SokobanComponent;
}());
exports.SokobanComponent = SokobanComponent;
//# sourceMappingURL=sokoban.component.js.map