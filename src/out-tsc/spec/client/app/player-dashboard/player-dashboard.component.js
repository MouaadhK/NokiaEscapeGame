"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var socket_service_1 = require("../services/socket.service");
var game_service_1 = require("../services/game.service");
var routing_service_1 = require("../services/routing.service");
//Modal
var modal_1 = require("ngx-bootstrap/modal");
var PlayerDashboardComponent = /** @class */ (function () {
    function PlayerDashboardComponent(socketService, gameService, routingService, modalService) {
        this.socketService = socketService;
        this.gameService = gameService;
        this.routingService = routingService;
        this.modalService = modalService;
        this.currentGameId = "";
        this.gameJoined = false;
    }
    PlayerDashboardComponent.prototype.cancel = function () {
        this.gameService.currentGameId = "";
        this.routingService.navigateTo("");
    };
    PlayerDashboardComponent = __decorate([
        core_1.Component({
            selector: 'player-dashboard',
            templateUrl: './player-dashboard.component.html',
            styleUrls: ['./player-dashboard.component.scss']
        }),
        __metadata("design:paramtypes", [socket_service_1.SocketService,
            game_service_1.GameService,
            routing_service_1.RoutingService,
            modal_1.BsModalService])
    ], PlayerDashboardComponent);
    return PlayerDashboardComponent;
}());
exports.PlayerDashboardComponent = PlayerDashboardComponent;
//# sourceMappingURL=player-dashboard.component.js.map