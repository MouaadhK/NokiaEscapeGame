"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var socket_service_1 = require("../services/socket.service");
var Sockets_1 = require("../../../api/Sockets");
var game_service_1 = require("../services/game.service");
var routing_service_1 = require("../services/routing.service");
var modal_1 = require("ngx-bootstrap/modal");
var GameControlsComponent = /** @class */ (function () {
    function GameControlsComponent(socketService, gameService, routingService, modalService) {
        this.socketService = socketService;
        this.gameService = gameService;
        this.routingService = routingService;
        this.modalService = modalService;
        this.currentGameId = '';
        this.startPauseResume = "START ";
        this.gamePaused = false;
        this.gameStarted = false;
        this.endOfGame = false;
    }
    GameControlsComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.gameService.currentGameId) {
            this.routingService.navigateTo('');
        }
        else {
            this.currentGameId = this.gameService.currentGameId;
        }
        this.socketService.onReceive(Sockets_1.SocketEvent.gameEnded, function () {
            _this.endOfGame = true;
        });
    };
    GameControlsComponent.prototype.action = function () {
        if (!this.gameStarted || this.gamePaused) {
            this.startResumeGame();
        }
        else {
            this.pauseGame();
        }
    };
    GameControlsComponent.prototype.startResumeGame = function () {
        this.startPauseResume = "PAUSE ";
        this.gameStarted = true;
        this.gamePaused = false;
        this.socketService.emit(Sockets_1.SocketEvent.startOrResumeGame, this.currentGameId);
    };
    GameControlsComponent.prototype.pauseGame = function () {
        this.startPauseResume = "RESUME";
        this.gamePaused = true;
        this.socketService.emit(Sockets_1.SocketEvent.pauseGame, this.gameService.currentGameId);
    };
    GameControlsComponent.prototype.playIntro = function () {
        this.socketService.emit(Sockets_1.SocketEvent.startIntroForEveryone, this.gameService.currentGameId);
    };
    GameControlsComponent.prototype.abandon = function () {
        this.socketService.emit(Sockets_1.SocketEvent.abandonGame, this.gameService.currentGameId);
        location.reload();
    };
    GameControlsComponent = __decorate([
        core_1.Component({
            selector: 'app-game-controls',
            templateUrl: './game-controls.component.html',
            styleUrls: ['./game-controls.component.scss']
        }),
        __metadata("design:paramtypes", [socket_service_1.SocketService,
            game_service_1.GameService,
            routing_service_1.RoutingService,
            modal_1.BsModalService])
    ], GameControlsComponent);
    return GameControlsComponent;
}());
exports.GameControlsComponent = GameControlsComponent;
//# sourceMappingURL=game-controls.component.js.map