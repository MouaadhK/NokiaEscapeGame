"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Sockets_1 = require("../../../api/Sockets");
var socket_service_1 = require("../services/socket.service");
var game_service_1 = require("../services/game.service");
var p5 = require("p5");
var _this;
var GameStatusComponent = /** @class */ (function () {
    function GameStatusComponent(socketService, gameService) {
        var _this = this;
        this.socketService = socketService;
        this.gameService = gameService;
        this._this = this;
        this.playerName = "";
        this.players = [];
        this.createCanvas = function () {
            GameStatusComponent_1.firstPlayer = true;
            _this.p5 = new p5(_this.playerBox);
        };
        this.playerBox = function (p) {
            var windowW = 799;
            var windowH = 507;
            var font;
            var textString = _this.playerName;
            var x = p.random(10, 500);
            var y = p.random(10, 700);
            var c = p.color(255, 0, 0);
            function preload() {
                font = p.loadFont('');
            }
            p.setup = function () {
                var canvas = p.createCanvas(windowW, windowH);
                canvas.parent("test");
                p.frameRate(30);
                p.noStroke();
                p.textSize(16);
            };
            p.draw = function () {
                for (var i = 0; i < _this.players.length; i++) {
                    p.fill(_this.players[i].color);
                    p.rect(_this.players[i].x, _this.players[i].y, 125, 35);
                    p.fill(0);
                    p.text(_this.players[i].name, _this.players[i].x, _this.players[i].y, 125, 35);
                    if (_this.players.length > 1) {
                        for (var j = 0; j < _this.players.length; j++) {
                            if (_this.players[j] != _this.players[i]) {
                                p.stroke(255);
                                p.line(_this.players[i].x, _this.players[i].y, _this.players[j].x, _this.players[j].y);
                            }
                        }
                    }
                }
            };
        };
    }
    GameStatusComponent_1 = GameStatusComponent;
    GameStatusComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.socketService.onReceive(Sockets_1.SocketEvent.newPlayerForModerator, function (player) {
            _this.playerName = player.name;
            if (!GameStatusComponent_1.firstPlayer) {
                var playerStatus = { name: _this.playerName, x: _this.generateRandomInt(0, 700), y: _this.generateRandomInt(0, 500), color: 'white' };
                _this.players.push(playerStatus);
                _this.createCanvas();
            }
            else {
                var playerStatus = { name: _this.playerName, x: _this.generateRandomInt(0, 700), y: _this.generateRandomInt(0, 500), color: 'white' };
                _this.players.push(playerStatus);
            }
        });
        this.socketService.onReceive(Sockets_1.SocketEvent.leavingPlayerForModerator, function (player) {
            for (var i = 0; i < _this.players.length; i++) {
                if (_this.players[i].name == player.name) {
                    _this.players[i].color = 'red';
                }
            }
        });
    };
    GameStatusComponent.prototype.generateRandomInt = function (min, maxExcl) {
        return min + Math.floor(Math.random() * (maxExcl - min));
    };
    GameStatusComponent.firstPlayer = false;
    __decorate([
        core_1.ViewChild("test"),
        __metadata("design:type", core_1.ElementRef)
    ], GameStatusComponent.prototype, "test", void 0);
    GameStatusComponent = GameStatusComponent_1 = __decorate([
        core_1.Component({
            selector: 'game-status',
            templateUrl: './game-status.component.html',
            styleUrls: ['./game-status.component.scss']
        }),
        __metadata("design:paramtypes", [socket_service_1.SocketService,
            game_service_1.GameService])
    ], GameStatusComponent);
    return GameStatusComponent;
    var GameStatusComponent_1;
}());
exports.GameStatusComponent = GameStatusComponent;
//# sourceMappingURL=game-status.component.js.map