"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Game_1 = require("../../../api/Game");
var Sockets_1 = require("../../../api/Sockets");
var socket_service_1 = require("../services/socket.service");
var game_service_1 = require("../services/game.service");
var routing_service_1 = require("../services/routing.service");
var routing_module_1 = require("../routing/routing.module");
var LandingpageComponent = /** @class */ (function () {
    function LandingpageComponent(socketService, gameService, routingService) {
        this.socketService = socketService;
        this.gameService = gameService;
        this.routingService = routingService;
        this.gameTitle = "Save the Network";
        this.subtitle = "Nokia Escape Game";
        this.newGameInfoShown = false;
        this.newGame = new Game_1.Game;
    }
    LandingpageComponent.prototype.ngOnInit = function () {
        this.newGame = this.gameService.defaultGame();
    };
    LandingpageComponent.prototype.hostNewGame = function () {
        this.routingService.navigateTo(routing_module_1.AppPaths.newGame);
    };
    LandingpageComponent.prototype.createNewGame = function () {
        var _this = this;
        this.gameService.createNewGame(this.newGame);
        this.socketService.onReceive(Sockets_1.SocketEvent.joinGameAsModerator, function (gameId) {
            _this.routingService.navigateTo(routing_module_1.AppPaths.moderatorPath);
            _this.gameService.currentGameId = gameId;
            _this.gameService.myPlayerProfile = _this.newGame.moderator;
        });
    };
    LandingpageComponent.prototype.goToLandingPage = function () {
        this.routingService.navigateTo(routing_module_1.AppPaths.home);
    };
    LandingpageComponent = __decorate([
        core_1.Component({
            selector: 'landingpage',
            templateUrl: './landingpage.component.html',
            styleUrls: ['./landingpage.component.scss']
        }),
        __metadata("design:paramtypes", [socket_service_1.SocketService,
            game_service_1.GameService,
            routing_service_1.RoutingService])
    ], LandingpageComponent);
    return LandingpageComponent;
}());
exports.LandingpageComponent = LandingpageComponent;
//# sourceMappingURL=landingpage.component.js.map