"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Game_1 = require("../../../api/Game");
var Sockets_1 = require("../../../api/Sockets");
var socket_service_1 = require("../services/socket.service");
var game_service_1 = require("../services/game.service");
var routing_service_1 = require("../services/routing.service");
var routing_module_1 = require("../routing/routing.module");
var GamecardComponent = /** @class */ (function () {
    function GamecardComponent(socketService, gameService, routingService) {
        this.socketService = socketService;
        this.gameService = gameService;
        this.routingService = routingService;
        this.gameJoined = false;
    }
    GamecardComponent.prototype.joinGame = function (gameId) {
        var _this = this;
        var joinReqData = {
            gameId: "",
            player: new Game_1.User()
        };
        joinReqData.gameId = gameId;
        this.gameService.myPlayerProfile.id = this.socketService.getSocketId();
        joinReqData.player = this.gameService.myPlayerProfile;
        this.socketService.emit(Sockets_1.SocketEvent.joinGameAsPlayerReq, joinReqData);
        var joinGameAsPlayerResp = this.socketService.onReceive(Sockets_1.SocketEvent.joinGameAsPlayerResp, function (result) {
            if (result.gameJoined) {
                _this.gameService.passphraseMask = result.passphraseMask;
                _this.gameService.currentGameId = gameId;
                _this.routingService.navigateTo(routing_module_1.AppPaths.playerPath);
                setTimeout((function () {
                    _this.routingService.openModal(routing_module_1.AppPaths.modalPaths.waitingScreen);
                }).bind(_this), 10);
            }
            else {
                alert("Game could not be joined!");
            }
            joinGameAsPlayerResp.removeListener(Sockets_1.SocketEvent.joinGameAsPlayerResp);
            _this.gameJoined = result.gameJoined;
        });
    };
    GamecardComponent.prototype.timeToString = function (time) {
        var t = new Game_1.Time(time.minutes, time.seconds);
        return t.toString();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Game_1.Game)
    ], GamecardComponent.prototype, "game", void 0);
    GamecardComponent = __decorate([
        core_1.Component({
            selector: 'gamecard',
            templateUrl: './gamecard.component.html',
            styleUrls: ['./gamecard.component.scss']
        }),
        __metadata("design:paramtypes", [socket_service_1.SocketService,
            game_service_1.GameService,
            routing_service_1.RoutingService])
    ], GamecardComponent);
    return GamecardComponent;
}());
exports.GamecardComponent = GamecardComponent;
//# sourceMappingURL=gamecard.component.js.map