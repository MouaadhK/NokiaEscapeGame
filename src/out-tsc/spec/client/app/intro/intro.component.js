"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var socket_service_1 = require("../services/socket.service");
var game_service_1 = require("../services/game.service");
var routing_service_1 = require("../services/routing.service");
var routing_module_1 = require("../routing/routing.module");
var Sockets_1 = require("../../../api/Sockets");
var IntroComponent = /** @class */ (function () {
    function IntroComponent(socketService, gameService, routingService) {
        this.socketService = socketService;
        this.gameService = gameService;
        this.routingService = routingService;
        this.sound = new Audio("../../assets/speech_sokoban.mp3");
        this.barHeights = [];
        this.refreshIntervalId = 0;
        this.speechText = "Nous sommes en l’an 2050. Une intelligence artificielle nommée Singularity a été mise en place, et contrôle tout sur notre planète. Nous venons d’apprendre que cette intelligence artificielle vient de lancer une rebellion contre les êtres humains. Son objectif: le réseau mondial des communications. Nokia a aujourd’hui réuni des équipes de développeurs qui se sont portés bénévoles pour protéger le réseau de l’attaque. Nous vous confions la responsabilité de protéger tout le réseau européen. Attention, Singularity sait se défendre. Elle risque de vous mettre des bâtons dans les roues pour vous stopper à votre tour. Vous devez déjouer ses pièges pour poursuivre votre mission.";
        this.speechSegments = [];
        this.currentSpeechSegment = "";
        this.currentSegmentIndex = 0;
    }
    IntroComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.socketService.onReceive(Sockets_1.SocketEvent.gameStarted, function (time) {
            _this.sound.pause();
            _this.sound.removeEventListener("ended", null);
        });
        this.socketService.onReceive(Sockets_1.SocketEvent.gameEnded, function () {
            _this.sound.pause();
            _this.sound.removeEventListener("ended", null);
        });
        this.sliceSpeech(8);
        this.playIntroSound();
        this.sound.addEventListener("ended", function () {
            _this.hide();
        });
    };
    IntroComponent.prototype.hide = function () {
        var _this = this;
        this.sound.pause();
        clearInterval(this.refreshIntervalId);
        this.sound.currentTime = 0;
        if (this.gameService.getCurrentGame().status == "Staging" && !this.gameService.isModerator()) {
            this.routingService.closeModal();
            setTimeout((function () {
                _this.routingService.openModal(routing_module_1.AppPaths.modalPaths.waitingScreen);
            }).bind(this), 10);
        }
        this.routingService.closeModal();
    };
    IntroComponent.prototype.playIntroSound = function () {
        var _this = this;
        this.speech();
        this.sound.load();
        this.sound.playbackRate = 1.1;
        var context = new window.AudioContext();
        var source = context.createMediaElementSource(this.sound);
        var analyser = context.createAnalyser();
        source.connect(analyser);
        source.connect(context.destination);
        analyser.fftSize = 64;
        var frequencyData = new Uint8Array(analyser.frequencyBinCount);
        this.refreshIntervalId = setInterval((function () {
            analyser.getByteFrequencyData(frequencyData);
            frequencyData.slice(0, analyser.frequencyBinCount / 2 - 4).forEach(function (value, index) {
                _this.barHeights[index] = value * 10.0 / 255 + 0.3;
            });
        }).bind(this), 20);
        this.sound.play();
    };
    IntroComponent.prototype.speech = function () {
        this.currentSpeechSegment = this.speechSegments[this.currentSegmentIndex++];
        if (this.currentSegmentIndex < this.speechSegments.length) {
            setTimeout(this.speech.bind(this), 5000);
        }
    };
    IntroComponent.prototype.sliceSpeech = function (nbFragment) {
        var words = this.speechText.split(" ");
        var segmentLength = (words.length * 1.0) / nbFragment;
        for (var i = 0; i < nbFragment - 1; i++) {
            this.speechSegments.push(words.splice(0, Math.round(segmentLength - 0.01)).join(' '));
        }
        this.speechSegments.push(words.join(' '));
    };
    IntroComponent = __decorate([
        core_1.Component({
            selector: 'app-intro',
            templateUrl: './intro.component.html',
            styleUrls: ['./intro.component.scss']
        }),
        __metadata("design:paramtypes", [socket_service_1.SocketService,
            game_service_1.GameService,
            routing_service_1.RoutingService])
    ], IntroComponent);
    return IntroComponent;
}());
exports.IntroComponent = IntroComponent;
//# sourceMappingURL=intro.component.js.map