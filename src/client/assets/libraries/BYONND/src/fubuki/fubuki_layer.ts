import { Connector } from "../connector/connector";
import { Point } from "../point";
import { Neuron } from "../neuron";
import { ConnectorSimple } from "../connector/connectorSimple";

export class Fubuki_layer {
	public size: number;
	public contracted: boolean;
	public parentLayer: Fubuki_layer;
	public neurons: Neuron[];
	public y: number;
	public connectors: Connector[];
	constructor(size: number) {
		this.size = size;
		this.contracted = this.size > 60 ? true : false;
		this.parentLayer = null;
		this.neurons = new Array<Neuron>();
		this.y = 35;
		this.connectors = new Array<Connector>();
	}

	public getRadius(): number {
		return this.neurons[0].radius;
	}

	public setRadius(newRadius: number) {
		for (let i = 0; i < this.neurons.length; i++) {
			this.neurons[i].radius = newRadius;
		}
	}

	public getX(): number {
		return this.neurons[0].x;
	}

	public getY(): number {
		return this.neurons[0].y;
	}

	public getNbNeurons(): number {
		return this.size;
	}

	public display(canvas: HTMLCanvasElement): void {
		const context = canvas.getContext("2d");
		for (let i = 0; i < this.size; i++) {
			this.neurons[i].display(canvas);
		}
	}

	public createConnectors(): void {
		this.connectors = Array();
		for (let i = 1; i < this.neurons.length; i++) {
			let connector: ConnectorSimple = new ConnectorSimple(
				new Point(this.neurons[i - 1].x + this.getRadius(),
					this.getY()),
				new Point(this.neurons[i].x - this.getRadius(),
					this.getY())
			)
			connector.isPrimary = true;
			this.connectors.push(
				connector
			);
		}
		if (this.parentLayer != null) {
			for (let i = 0; i < this.neurons.length; i++) {
				for (let j = 0; j < this.size; j++) {
					let connector: ConnectorSimple = new ConnectorSimple(
						new Point(this.neurons[j].x, this.getY()), 
						new Point(this.parentLayer.neurons[i].x, this.parentLayer.getY()))
					if (i === j) {
						connector.isPrimary = true;	
					}
					this.connectors.push(connector);
				}
			}
		}
	}

	public activateNextNeuron(): boolean {
		if (!this.contracted) {
			let neuronIndex = -1;
			let i = 0;
			let found = false;
			while (i < this.neurons.length && !found) {
				if (this.neurons[i].active) {
					neuronIndex = i;
					found = true;
				}
				i++;
			}
			if (neuronIndex === -1) {
				this.neurons[0].activate();
				return true;
			}
			else {
				this.neurons[neuronIndex].disable();
				if (neuronIndex === this.neurons.length - 1) {
					this.neurons[0].activate();
					return false;
				}
				else {
					this.neurons[neuronIndex + 1].activate();
					return true;
				}
			}
		}
		return true;
	}

	public activateNeuron(index: number) {
		index = index % this.neurons.length;
		for (let i = 0; i < this.neurons.length; i++) {
			this.neurons[i].disable();
		}
		this.neurons[index].activate();
	}

	public disable(): void {
		for (let i = 0; i < this.neurons.length; i++) {
			this.neurons[i].disable();
		}
	}
}