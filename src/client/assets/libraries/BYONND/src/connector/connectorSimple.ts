import { Point } from "../point";
import { Connector } from "./connector";

export class ConnectorSimple extends Connector {
	public p1: Point;
	public p2: Point;
	public isPrimary: boolean;
	constructor(p1: Point, p2: Point) {
		super(p1, p2);
		this.isPrimary = false;
	}

    // Override
	public display(canvas: HTMLCanvasElement): void {
		const context = canvas.getContext("2d");
		context.save();
		context.beginPath();
		context.lineWidth = this.isPrimary ? 2 : 1;
		context.moveTo(this.p1.x, this.p1.y);
		context.lineTo(this.p2.x, this.p2.y);
		context.strokeStyle = this.isPrimary ? "#344250" : "#283540";
		context.stroke();
		context.restore();
	}
}