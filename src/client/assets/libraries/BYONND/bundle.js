class NeuralNetwork {
}

class NeuralNetworkBuilder {
    constructor(canvas) {
        this.canvas = canvas;
    }
    build(type) {
        switch (type) {
            case "mlp":
                return new MLP(this.canvas);
                break;
            case "cnn":
                return new CNN(this.canvas);
                break;
            case "rnn":
                return new RNN(this.canvas);
                break;
            case "fubuki":
                return new Fubuki(this.canvas);
                break;
            default:
                return null;
                break;
        }
    }
}

class Neuron {
    constructor(x, y, radius, active, value, hovered) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.active = active;
        this.value = value;
        this.hovered = hovered;
    }
    display(canvas) {
        const context = canvas.getContext("2d");
        context.beginPath();
        context.fillStyle = this.active ? "#FFBF02" : "#239DF9";
        context.lineWidth = 2;
        context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
        context.strokeStyle = this.active ? "#FF7900" : "#124191";
        context.stroke();
        context.fill();
        context.font = (this.radius) + "px Arial";
        context.fillStyle = "#124191";
        const value = this.value === "" ? "" : this.value;
        context.fillText(value, this.x - this.radius / 3, this.y + this.radius / 4);
    }
    activate() {
        this.active = true;
    }
    disable() {
        this.active = false;
    }
    collision(x, y) {
        const dx = x - this.x;
        const dy = y - this.y;
        return ((dx * dx) / (this.radius * this.radius) + (dy * dy) /
            (this.radius * this.radius) <= 1);
    }
    setValue(value) {
        this.value = value;
    }
}

var Orientation;
(function (Orientation) {
    Orientation["HORIZONTAL"] = "Horizontal";
    Orientation["VERTICAL"] = "Vertical";
})(Orientation || (Orientation = {}));

class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

class Quadrilateral {
    constructor(p1, p2, p4, p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }
    display(canvas) {
        const context = canvas.getContext("2d");
        context.beginPath();
        context.lineWidth = 1;
        context.moveTo(this.p1.x, this.p1.y);
        context.lineTo(this.p2.x, this.p2.y);
        context.lineTo(this.p3.x, this.p3.y);
        context.lineTo(this.p4.x, this.p4.y);
        context.lineTo(this.p1.x, this.p1.y);
        context.strokeStyle = "#124191";
        context.stroke();
        context.fillStyle = "#239DF9";
        context.fill();
    }
}

class Connector {
    constructor(p1, p2) {
        this.p1 = p1;
        this.p2 = p2;
    }
    display(canvas) { }
}

class ConnectorArrow extends Connector {
    constructor(p1, p2) {
        super(p1, p2);
    }
    // Override
    display(canvas) {
        const context = canvas.getContext("2d");
        context.beginPath();
        const headlen = 10;
        const angle = Math.atan2(this.p2.y - this.p1.y, this.p2.x - this.p1.x);
        context.save();
        context.moveTo(this.p1.x, this.p1.y);
        context.lineTo(this.p2.x, this.p2.y);
        context.lineTo(this.p2.x - headlen * Math.cos(angle - Math.PI / 6), this.p2.y - headlen * Math.sin(angle - Math.PI / 6));
        context.moveTo(this.p2.x, this.p2.y);
        context.lineTo(this.p2.x - headlen * Math.cos(angle + Math.PI / 6), this.p2.y - headlen * Math.sin(angle + Math.PI / 6));
        context.stroke();
        context.restore();
    }
}

class ConnectorArrowDash extends Connector {
    constructor(p1, p2) {
        super(p1, p2);
    }
    // Override
    display(canvas) {
        const context = canvas.getContext("2d");
        context.save();
        context.beginPath();
        const headlen = 10;
        const angle = Math.atan2(this.p2.y - this.p1.y, this.p2.x - this.p1.x);
        context.setLineDash([5, 15]);
        context.moveTo(this.p1.x, this.p1.y);
        context.lineTo(this.p2.x, this.p2.y);
        context.stroke();
        context.restore();
        context.save();
        context.beginPath();
        context.moveTo(this.p2.x, this.p2.y);
        context.lineTo(this.p2.x - headlen * Math.cos(angle - Math.PI / 6), this.p2.y - headlen * Math.sin(angle - Math.PI / 6));
        context.moveTo(this.p2.x, this.p2.y);
        context.lineTo(this.p2.x - headlen * Math.cos(angle + Math.PI / 6), this.p2.y - headlen * Math.sin(angle + Math.PI / 6));
        context.stroke();
        context.restore();
    }
}

class ConnectorSimple extends Connector {
    constructor(p1, p2) {
        super(p1, p2);
    }
    // Override
    display(canvas) {
        const context = canvas.getContext("2d");
        context.save();
        context.beginPath();
        context.lineWidth = 1;
        context.moveTo(this.p1.x, this.p1.y);
        context.lineTo(this.p2.x, this.p2.y);
        context.strokeStyle = "#000000";
        context.stroke();
        context.restore();
    }
}

class CNN extends NeuralNetwork {
    constructor(canvas) {
        super();
        this.canvas = canvas;
        this.layers = new Array();
        this.error = "";
    }
    // Override
    init() {
        this.initLayers(this.canvas);
        this.createConnectors();
    }
    // Override
    draw() {
        const context = this.canvas.getContext("2d");
        context.save();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.display();
    }
    // Override
    display() {
        for (let i = this.layers.length - 1; i > 0; i--) {
            this.layers[i].display(this.canvas);
            for (let j = 0; j < this.layers[i].connectors.length; j++) {
                this.layers[i].connectors[j].display(this.canvas);
            }
        }
        this.layers[0].display(this.canvas);
    }
    // Override
    addLayer(nbX, nbY, nbChannels) {
        try {
            const layer = new CNN_Layer(nbX, nbY, nbChannels);
            this.layers.push(layer);
            this.init();
        }
        catch (_a) {
            this.error = "Error: can't add an empty layer";
        }
    }
    // Override
    define(nbNeuronsChannel) {
        for (let i = 0; i < nbNeuronsChannel.length; i++) {
            this.addLayer(nbNeuronsChannel[i][0], nbNeuronsChannel[i][1], nbNeuronsChannel[i][2]);
        }
    }
    createConnectors() {
        for (let i = 1; i < this.layers.length; i++) {
            this.layers[i].createConnectors();
        }
    }
    initLayers(canvas) {
        for (let iLayer = 0; iLayer < this.layers.length; iLayer++) {
            if (iLayer > 0) {
                this.layers[iLayer].parentLayer = this.layers[iLayer - 1];
                if (this.layers[iLayer].parentLayer.size_y === 1) {
                    this.layers[iLayer].y = this.layers[iLayer].parentLayer.y + this.layers[iLayer].parentLayer.height + 150;
                }
                else {
                    this.layers[iLayer].y = this.layers[iLayer].parentLayer.y + this.layers[iLayer].parentLayer.height + 60;
                }
            }
            if (!this.layers[iLayer].contracted) {
                for (let j = 0; j < this.layers[iLayer].size_y; j++) {
                    this.layers[iLayer].neurons.push(new Array());
                    for (let i = 0; i < this.layers[iLayer].size_x; i++) {
                        const max_radius = 20;
                        const min_radius = 1;
                        const displaySize = this.layers[iLayer].contracted ? max_radius : this.layers[iLayer].size_x;
                        const canvas_width = canvas.width;
                        let gap = 4;
                        let neuron_radius = this.layers[iLayer].isLeaf ?
                            canvas_width / (this.layers[iLayer].size_x * 2.5) :
                            canvas_width / (this.layers[iLayer].size_x * 5);
                        neuron_radius = neuron_radius > max_radius ? max_radius : neuron_radius;
                        neuron_radius = neuron_radius < min_radius ? min_radius : neuron_radius;
                        const free_space = canvas_width - displaySize * (neuron_radius * 2 + gap);
                        const offset = this.layers[iLayer].isLeaf ? 2.2 : 1.5;
                        this.layers[iLayer].neurons[j].push(new Neuron(free_space / 2 + neuron_radius + (i * neuron_radius * offset) + gap + j * (neuron_radius / 4), this.layers[iLayer].y + (neuron_radius / 2) * j, neuron_radius, false, ""));
                        gap += 4;
                    }
                }
                this.layers[iLayer].pos_x = this.layers[iLayer].neurons[0][0].x;
                this.layers[iLayer].pos_y = this.layers[iLayer].neurons[0][0].y;
                this.layers[iLayer].width = this.layers[iLayer].neurons[this.layers[iLayer].size_y - 1][this.layers[iLayer].size_x - 1].x - this.layers[iLayer].pos_x;
                this.layers[iLayer].height = this.layers[iLayer].neurons[this.layers[iLayer].size_y - 1][this.layers[iLayer].size_x - 1].y - this.layers[iLayer].pos_y;
            }
            else {
                this.layers[iLayer].pos_x = 300;
                this.layers[iLayer].pos_y = this.layers[iLayer].y;
                this.layers[iLayer].width = 350;
                this.layers[iLayer].height = 150;
            }
        }
        this.layers[0].isRoot = true;
    }
    // Override
    getNbNeurons() {
        let nbNeurons = 0;
        for (let i = 0; i < this.layers.length; i++) {
            nbNeurons += this.layers[i].getNbNeurons();
        }
        return nbNeurons;
    }
    // Override
    getNbLayers() {
        return this.layers.length;
    }
    // Override
    isContracted() {
        for (let i = 0; i < this.layers.length; i++) {
            if (this.layers[i].contracted) {
                return true;
            }
        }
        return false;
    }
    // Override
    getError() {
        return this.error;
    }
}

class CNN_Layer {
    constructor(size_x, size_y, nbChannels) {
        this.size_x = size_x;
        this.size_y = size_y == null ? 1 : size_y;
        this.nbChannels = nbChannels == null ? 0 : nbChannels;
        this.isLeaf = this.size_y === 1 && this.nbChannels === 0 ? true : false;
        this.contracted = (this.size_x > 60 ||
            this.size_y > 60) ? true : false;
        this.parentLayer = null;
        this.neurons = new Array();
        this.y = 100;
        this.connectors = new Array();
    }
    createConnectors() {
        if (this.connectors.length === 0) {
            this.connectors = Array();
            if (this.size_y > 1 || this.parentLayer.size_y > 1) {
                for (let i = 0; i < 3; i++) {
                    for (let j = 0; j < 3; j++) {
                        this.connectors.push(new ConnectorSimple(new Point((this.pos_x + (this.width / 2)), (this.pos_y + this.height / 2)), new Point((this.parentLayer.pos_x + (this.parentLayer.width / 2) - this.parentLayer.width / 4 + this.parentLayer.width / 4 * i), (this.parentLayer.y + this.parentLayer.height / 2) - this.parentLayer.height / 4 + this.parentLayer.height / 4 * j)));
                    }
                }
            }
            else {
                if (this.parentLayer.getNbNeurons() > 25 || this.getNbNeurons() > 25) {
                    for (let i = 0; i < 11; i++) {
                        for (let j = 0; j < 11; j++) {
                            this.connectors.push(new ConnectorSimple(new Point(50 + i * 95, this.getY() - this.getRadius() - 5), new Point(50 + j * 95, this.parentLayer.getY() - this.parentLayer.getRadius() + 35)));
                        }
                    }
                }
                else {
                    for (let i = 0; i < this.parentLayer.getNbNeurons(); i++) {
                        for (let j = 0; j < this.getNbNeurons(); j++) {
                            this.connectors.push(new ConnectorSimple(new Point(this.neurons[0][j].x, this.getY() - this.getRadius()), new Point(this.parentLayer.neurons[0][i].x, this.parentLayer.getY() + this.parentLayer.getRadius())));
                        }
                    }
                }
            }
        }
    }
    display(canvas) {
        const context = canvas.getContext("2d");
        if (!this.contracted) {
            for (let j = 0; j < this.size_y; j++) {
                for (let i = 0; i < this.size_x; i++) {
                    this.neurons[j][i].display(canvas);
                }
            }
        }
        else {
            const condensed_layer = new Quadrilateral(new Point(this.pos_x, this.pos_y), new Point(this.pos_x + this.width, this.pos_y), new Point(this.pos_x + 60, this.pos_y + this.height), new Point(this.pos_x + this.width + 60, this.pos_y + this.height));
            condensed_layer.display(canvas);
        }
        if (!this.isLeaf) {
            context.font = "20px Arial";
            context.fillStyle = "#124191";
            let text = this.size_x + " x " + this.size_y;
            context.fillText(text, this.pos_x - 100, this.pos_y + 30);
            context.font = "30px Arial";
            context.fillStyle = "#124191";
            text = "X " + this.nbChannels;
            context.fillText(text, this.pos_x + this.width + 50, this.pos_y + (this.height / 2));
        }
    }
    getNbNeurons() {
        return this.size_x * this.size_y;
    }
    getX() {
        return this.neurons[0][0].x;
    }
    getY() {
        return this.neurons[0][0].y;
    }
    getRadius() {
        return this.neurons[0][0].radius;
    }
    setRadius(newRadius) {
        for (let i = 0; i < this.size_y; i++) {
            for (let j = 0; j < this.size_x; j++) {
                this.neurons[i][j].radius = newRadius;
            }
        }
    }
}

class MLP extends NeuralNetwork {
    constructor(canvas) {
        super();
        this.canvas = canvas;
        this.layers = new Array();
        this.error = "";
    }
    // Override
    init() {
        this.initLayers(this.canvas);
        this.balanceRadius();
        this.createConnectors();
    }
    // Override
    addLayer(nbNeurons) {
        try {
            const layer = new MLP_Layer(nbNeurons);
            this.layers.push(layer);
            this.init();
        }
        catch (_a) {
            this.error = "Error: can't add an empty layer";
        }
    }
    // Override
    define(nbNeurons) {
        for (let i = 0; i < nbNeurons.length; i++) {
            this.addLayer(nbNeurons[i]);
        }
    }
    // Override
    draw() {
        const context = this.canvas.getContext("2d");
        context.save();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.display();
    }
    initLayers(canvas) {
        for (let j = 0; j < this.layers.length; j++) {
            if (this.layers[j].neurons.length === 0) {
                if (j > 0) {
                    this.layers[j].parentLayer = this.layers[j - 1];
                    this.layers[j].y = this.layers[j].parentLayer.y + 150;
                }
                const max_radius = 30;
                const min_radius = 1;
                const displaySize = this.layers[j].contracted ? max_radius : this.layers[j].size;
                const canvas_width = canvas.width;
                let gap = 4;
                let neuron_radius = canvas_width / ((displaySize * 2)) - (gap / 2) - 1;
                neuron_radius = neuron_radius > max_radius ? max_radius : neuron_radius;
                neuron_radius = neuron_radius < min_radius ? min_radius : neuron_radius;
                const free_space = canvas_width - displaySize * (neuron_radius * 2 + gap);
                if (!this.layers[j].contracted) {
                    for (let i = 0; i < displaySize; i++) {
                        this.layers[j].neurons.push(new Neuron(free_space / 2 + neuron_radius + (i * neuron_radius * 2) + gap, this.layers[j].y, neuron_radius, false, ""));
                        gap += 4;
                    }
                }
                else {
                    for (let i = 0; i < 4; i++) {
                        this.layers[j].neurons.push(new Neuron(free_space / 2 + neuron_radius + (i * neuron_radius * 2) + gap, this.layers[j].y, neuron_radius, false, ""));
                        gap += 8;
                    }
                    for (let i = 0; i < 4; i++) {
                        this.layers[j].neurons.push(new Neuron(canvas_width - free_space / 2 - neuron_radius - (i * neuron_radius * 2) - gap + 40, this.layers[j].y, neuron_radius, false, ""));
                        gap += 8;
                    }
                }
            }
        }
    }
    balanceRadius() {
        let min_radius = this.layers[0].getRadius();
        for (let i = 1; i < this.layers.length; i++) {
            if (min_radius > this.layers[i].getRadius()) {
                min_radius = this.layers[i].getRadius();
            }
        }
        for (let i = 0; i < this.layers.length; i++) {
            this.layers[i].setRadius(min_radius);
        }
    }
    createConnectors() {
        for (let i = 1; i < this.layers.length; i++) {
            this.layers[i].createConnectors();
        }
    }
    // Override
    display() {
        for (let i = 1; i < this.layers.length; i++) {
            for (let j = 0; j < this.layers[i].connectors.length; j++) {
                this.layers[i].connectors[j].display(this.canvas);
            }
        }
        for (let i = 0; i < this.layers.length; i++) {
            this.layers[i].display(this.canvas);
        }
    }
    // Override
    getNbNeurons() {
        let nbNeurons = 0;
        for (let i = 0; i < this.layers.length; i++) {
            nbNeurons += this.layers[i].getNbNeurons();
        }
        return nbNeurons;
    }
    activateLayerNextNeuron(layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        return this.layers[layerIndex].activateNextNeuron();
    }
    activateLayerNeuron(layerIndex, neuronIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].activateNeuron(neuronIndex);
    }
    activateOutputNeuron(neuronIndex) {
        this.layers[this.layers.length - 1].activateNeuron(neuronIndex);
    }
    disableLayer(layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].disable();
    }
    // Override
    getNbLayers() {
        return this.layers.length;
    }
    // Override
    isContracted() {
        for (let i = 0; i < this.layers.length; i++) {
            if (this.layers[i].contracted) {
                return true;
            }
        }
        return false;
    }
    // Override
    getError() {
        return this.error;
    }
}

class MLP_Layer {
    constructor(size) {
        this.size = size;
        this.contracted = this.size > 60 ? true : false;
        this.parentLayer = null;
        this.neurons = new Array();
        this.y = 100;
        this.connectors = new Array();
    }
    getRadius() {
        return this.neurons[0].radius;
    }
    setRadius(newRadius) {
        for (let i = 0; i < this.neurons.length; i++) {
            this.neurons[i].radius = newRadius;
        }
    }
    getX() {
        return this.neurons[0].x;
    }
    getY() {
        return this.neurons[0].y;
    }
    getNbNeurons() {
        return this.size;
    }
    display(canvas) {
        const context = canvas.getContext("2d");
        if (!this.contracted) {
            for (let i = 0; i < this.size; i++) {
                this.neurons[i].display(canvas);
            }
        }
        else {
            const canvas_width = canvas.width;
            for (let i = 0; i < this.neurons.length; i++) {
                this.neurons[i].display(canvas);
            }
            context.font = "30px Arial";
            const text = ". . . . . " + this.size + " neurons . . . . .";
            context.fillText(text, canvas_width / 2 - text.length * 6, this.getY() + this.getRadius() / 2);
        }
    }
    createConnectors() {
        if (this.connectors.length === 0) {
            if (!this.contracted && !this.parentLayer.contracted) {
                this.connectors = Array();
                for (let i = 0; i < this.parentLayer.size; i++) {
                    for (let j = 0; j < this.size; j++) {
                        this.connectors.push(new ConnectorSimple(new Point(this.neurons[j].x, this.getY() - this.getRadius()), new Point(this.parentLayer.neurons[i].x, this.parentLayer.getY() + this.parentLayer.getRadius())));
                    }
                }
            }
            else {
                for (let i = 0; i < 11; i++) {
                    for (let j = 0; j < 11; j++) {
                        this.connectors.push(new ConnectorSimple(new Point(50 + i * 101, this.getY() - this.getRadius() - 5), new Point(50 + j * 101, this.parentLayer.getY() - this.parentLayer.getRadius() + 35)));
                    }
                }
            }
        }
    }
    activateNextNeuron() {
        if (!this.contracted) {
            let neuronIndex = -1;
            let i = 0;
            let found = false;
            while (i < this.neurons.length && !found) {
                if (this.neurons[i].active) {
                    neuronIndex = i;
                    found = true;
                }
                i++;
            }
            if (neuronIndex === -1) {
                this.neurons[0].activate();
                return true;
            }
            else {
                this.neurons[neuronIndex].disable();
                if (neuronIndex === this.neurons.length - 1) {
                    this.neurons[0].activate();
                    return false;
                }
                else {
                    this.neurons[neuronIndex + 1].activate();
                    return true;
                }
            }
        }
        return true;
    }
    activateNeuron(index) {
        index = index % this.neurons.length;
        for (let i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
        this.neurons[index].activate();
    }
    disable() {
        for (let i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
    }
}

class Fubuki extends NeuralNetwork {
    constructor(canvas) {
        super();
        this.canvas = canvas;
        this.layers = new Array();
        this.error = "";
    }
    // Override
    init() {
        this.initLayers(this.canvas);
        this.balanceRadius();
        this.createConnectors();
    }
    // Override
    addLayer(nbNeurons) {
        try {
            const layer = new Fubuki_layer(nbNeurons);
            this.layers.push(layer);
            this.init();
        }
        catch (_a) {
            this.error = "Error: can't add an empty layer";
        }
    }
    // Override
    define(nbNeurons) {
        for (let i = 0; i < nbNeurons.length; i++) {
            this.addLayer(nbNeurons[i]);
        }
    }
    // Override
    draw() {
        const context = this.canvas.getContext("2d");
        context.save();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.display();
    }
    initLayers(canvas) {
        for (let j = 0; j < this.layers.length; j++) {
            if (this.layers[j].neurons.length === 0) {
                if (j > 0) {
                    this.layers[j].parentLayer = this.layers[j - 1];
                    this.layers[j].y = this.layers[j].parentLayer.y + 150;
                }
                const max_radius = 30;
                const min_radius = 1;
                const displaySize = this.layers[j].contracted ? max_radius : this.layers[j].size;
                const canvas_width = canvas.width;
                let gap = 0;
                let neuron_radius = canvas_width / ((displaySize * 2)) - (gap / 2) - 1;
                neuron_radius = neuron_radius > max_radius ? max_radius : neuron_radius;
                neuron_radius = neuron_radius < min_radius ? min_radius : neuron_radius;
                for (let i = 0; i < displaySize; i++) {
                    this.layers[j].neurons.push(new Neuron(100 + neuron_radius + (i * neuron_radius * 2) + gap, this.layers[j].y, neuron_radius, false, ""));
                    gap += 150;
                }
            }
        }
    }
    balanceRadius() {
        let min_radius = this.layers[0].getRadius();
        for (let i = 1; i < this.layers.length; i++) {
            if (min_radius > this.layers[i].getRadius()) {
                min_radius = this.layers[i].getRadius();
            }
        }
        for (let i = 0; i < this.layers.length; i++) {
            this.layers[i].setRadius(min_radius);
        }
    }
    createConnectors() {
        for (let i = 0; i < this.layers.length; i++) {
            this.layers[i].createConnectors();
        }
    }
    // Override
    display() {
        for (let i = 0; i < this.layers.length; i++) {
            for (let j = 0; j < this.layers[i].connectors.length; j++) {
                this.layers[i].connectors[j].display(this.canvas);
            }
        }
        for (let i = 0; i < this.layers.length; i++) {
            this.layers[i].display(this.canvas);
        }
    }
    // Override
    getNbNeurons() {
        let nbNeurons = 0;
        for (let i = 0; i < this.layers.length; i++) {
            nbNeurons += this.layers[i].getNbNeurons();
        }
        return nbNeurons;
    }
    activateLayerNextNeuron(layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        return this.layers[layerIndex].activateNextNeuron();
    }
    activateLayerNeuron(layerIndex, neuronIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].activateNeuron(neuronIndex);
    }
    activateOutputNeuron(neuronIndex) {
        this.layers[this.layers.length - 1].activateNeuron(neuronIndex);
    }
    disableLayer(layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].disable();
    }
    // Override
    getNbLayers() {
        return this.layers.length;
    }
    // Override
    isContracted() {
        for (let i = 0; i < this.layers.length; i++) {
            if (this.layers[i].contracted) {
                return true;
            }
        }
        return false;
    }
    // Override
    getError() {
        return this.error;
    }
}

class Fubuki_layer {
    constructor(size) {
        this.size = size;
        this.contracted = this.size > 60 ? true : false;
        this.parentLayer = null;
        this.neurons = new Array();
        this.y = 100;
        this.connectors = new Array();
    }
    getRadius() {
        return this.neurons[0].radius;
    }
    setRadius(newRadius) {
        for (let i = 0; i < this.neurons.length; i++) {
            this.neurons[i].radius = newRadius;
        }
    }
    getX() {
        return this.neurons[0].x;
    }
    getY() {
        return this.neurons[0].y;
    }
    getNbNeurons() {
        return this.size;
    }
    display(canvas) {
        const context = canvas.getContext("2d");
        for (let i = 0; i < this.size; i++) {
            this.neurons[i].display(canvas);
        }
    }
    createConnectors() {
        this.connectors = Array();
        for (let i = 1; i < this.neurons.length; i++) {
            this.connectors.push(new ConnectorSimple(new Point(this.neurons[i - 1].x + this.getRadius(), this.getY()), new Point(this.neurons[i].x - this.getRadius(), this.getY())));
        }
        if (this.parentLayer != null) {
            for (let i = 0; i < 4; i++) {
                for (let j = 0; j < this.size; j++) {
                    if (i === j) {
                        this.connectors.push(new ConnectorSimple(new Point(this.neurons[j].x, this.getY() - this.getRadius()), new Point(this.parentLayer.neurons[i].x, this.parentLayer.getY() + this.parentLayer.getRadius())));
                    }
                }
            }
        }
    }
    activateNextNeuron() {
        if (!this.contracted) {
            let neuronIndex = -1;
            let i = 0;
            let found = false;
            while (i < this.neurons.length && !found) {
                if (this.neurons[i].active) {
                    neuronIndex = i;
                    found = true;
                }
                i++;
            }
            if (neuronIndex === -1) {
                this.neurons[0].activate();
                return true;
            }
            else {
                this.neurons[neuronIndex].disable();
                if (neuronIndex === this.neurons.length - 1) {
                    this.neurons[0].activate();
                    return false;
                }
                else {
                    this.neurons[neuronIndex + 1].activate();
                    return true;
                }
            }
        }
        return true;
    }
    activateNeuron(index) {
        index = index % this.neurons.length;
        for (let i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
        this.neurons[index].activate();
    }
    disable() {
        for (let i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
    }
}

class RNN extends NeuralNetwork {
    constructor(canvas) {
        super();
        this.canvas = canvas;
        this.steps = new Array();
        this.layers = new Array();
        this.orientation = Orientation.HORIZONTAL;
        this.error = "";
    }
    // Override
    init() {
        this.initLayers(this.canvas);
        this.createConnectors();
    }
    // Override
    addLayer(nbNeurons) {
        try {
            const layer = new RNN_Layer(nbNeurons);
            this.layers.push(layer);
        }
        catch (_a) {
            this.error = "Error: can't add an empty layer";
        }
    }
    // Override
    define(nbSteps, nbLayers, serie, orientation) {
        this.nbLayers = nbLayers + 2;
        this.contracted = nbSteps > 15;
        this.nbSteps = this.isContracted() ? 15 : nbSteps;
        for (let i = 0; i < this.nbLayers; i++) {
            this.addLayer(3);
        }
        switch (orientation.toLowerCase()) {
            case "horizontal":
                this.orientation = Orientation.HORIZONTAL;
                break;
            case "vertical":
                this.orientation = Orientation.VERTICAL;
                break;
        }
        this.init();
        this.serie = serie;
    }
    // Override
    draw() {
        const context = this.canvas.getContext("2d");
        context.save();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.display();
    }
    initLayers(canvas) {
        const honrizontalConnectorSize = 50;
        let gap_step = 4 + honrizontalConnectorSize;
        const max_radius = 30;
        const min_radius = 1;
        const displaySize = this.nbSteps;
        const canvas_width = canvas.width;
        let neuron_radius = canvas_width / ((displaySize * 2)) - ((gap_step) / 2) - 1;
        neuron_radius = neuron_radius > max_radius ? max_radius : neuron_radius;
        neuron_radius = neuron_radius < min_radius ? min_radius : neuron_radius;
        const free_space = canvas_width - displaySize * (neuron_radius * 2 + gap_step);
        for (let i = 0; i < this.nbSteps; i++) {
            for (let j = 0; j < this.layers.length; j++) {
                if (this.layers[j].neurons.length === 0) {
                    if (j > 0) {
                        this.layers[j].parentLayer = this.layers[j - 1];
                        this.layers[j].y = this.layers[j].parentLayer.y + 120;
                    }
                    let gap_layer = 0;
                    for (let k = 0; k < this.layers[j].size; k++) {
                        switch (this.orientation) {
                            case Orientation.HORIZONTAL:
                                this.layers[j].neurons.push(new Neuron(free_space / 2 + neuron_radius + (i * neuron_radius * 2) + gap_step - 30, this.layers[j].y + gap_layer, neuron_radius, false, ""));
                                break;
                            case Orientation.VERTICAL:
                                this.layers[j].neurons.push(new Neuron(this.layers[j].y, free_space / 2 + neuron_radius + (i * neuron_radius * 2) + gap_step + gap_layer - 30, neuron_radius, false, ""));
                                break;
                        }
                        gap_layer += neuron_radius / 3;
                    }
                }
            }
            gap_step += 4 + honrizontalConnectorSize;
            this.steps.push(this.layers);
            this.layers = new Array();
            for (let i = 0; i < this.nbLayers; i++) {
                this.addLayer(3);
            }
        }
    }
    createConnectors() {
        for (let i = 0; i < this.nbSteps; i++) {
            for (let j = 0; j < this.layers.length; j++) {
                this.steps[i][j].createConnectors(i, j, this);
            }
        }
    }
    displaySteps() {
        for (let i = 0; i < this.nbSteps; i++) {
            for (let j = 0; j < this.layers.length; j++) {
                this.steps[i][j].display(this.canvas, this);
            }
        }
    }
    // Override
    display() {
        const context = this.canvas.getContext("2d");
        for (let i = 0; i < this.nbSteps; i++) {
            for (let j = 0; j < this.steps[i].length; j++) {
                for (let k = 0; k < this.steps[i][j].connectors.length; k++) {
                    this.steps[i][j].connectors[k].display(this.canvas);
                }
            }
        }
        this.displaySteps();
        if (this.serie != null) {
            if (this.nbSteps + 1 === this.serie.length) {
                switch (this.orientation) {
                    case Orientation.HORIZONTAL:
                        for (let i = 0; i < this.nbSteps; i++) {
                            context.font = "30px Arial";
                            context.fillText(this.serie[i], this.steps[i][0].neurons[0].x - 10, this.steps[i][0].neurons[0].y - 40);
                            context.fillText("t=" + i, this.steps[i][0].neurons[0].x - 20, this.steps[i][0].neurons[0].y - 90);
                        }
                        for (let i = 1; i <= this.nbSteps; i++) {
                            context.font = "30px Arial";
                            context.fillText(this.serie[i], this.steps[i - 1][0].neurons[0].x - 10, this.steps[i - 1][this.nbLayers - 1].neurons[0].y + 80);
                        }
                        break;
                    case Orientation.VERTICAL:
                        for (let i = 0; i < this.nbSteps; i++) {
                            context.font = "30px Arial";
                            const lastNeuron = this.steps[i][0].neurons[this.steps[i][0].neurons.length - 1];
                            context.fillText(this.serie[i], lastNeuron.x - 60, lastNeuron.y);
                            context.fillText("t=" + i, lastNeuron.x - 120, lastNeuron.y);
                        }
                        for (let i = 1; i <= this.nbSteps; i++) {
                            context.font = "30px Arial";
                            const lastNeuron = this.steps[i - 1][this.nbLayers - 1].neurons[this.steps[i - 1][this.nbLayers - 1].neurons.length - 1];
                            context.fillText(this.serie[i], lastNeuron.x + 60, lastNeuron.y);
                        }
                        break;
                }
            }
            else {
                if (this.isContracted()) {
                    console.error("Can't display serie data in a contracted view (serie length > 15)");
                }
                else {
                    console.error("The (nb steps+1) and the serie length must be equal");
                }
            }
        }
    }
    // Override
    getNbNeurons() {
        let nbNeurons = 0;
        for (let i = 0; i < this.layers.length; i++) {
            nbNeurons += this.layers[i].getNbNeurons();
        }
        return nbNeurons;
    }
    activateLayerNeuron(layerIndex, neuronIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].activateNeuron(neuronIndex);
    }
    activateOutputNeuron(neuronIndex) {
        this.layers[this.layers.length - 1].activateNeuron(neuronIndex);
    }
    disableLayer(layerIndex) {
        layerIndex = layerIndex % this.layers.length;
        this.layers[layerIndex].disable();
    }
    // Override
    getNbLayers() {
        return this.layers.length;
    }
    // Override
    isContracted() {
        return this.contracted;
    }
    // Override
    getError() {
        return this.error;
    }
}

class RNN_Layer {
    constructor(size) {
        this.size = size;
        this.parentLayer = null;
        this.neurons = new Array();
        this.y = 150;
        this.connectors = new Array();
    }
    getRadius() {
        return this.neurons[0].radius;
    }
    setRadius(newRadius) {
        for (let i = 0; i < this.neurons.length; i++) {
            this.neurons[i].radius = newRadius;
        }
    }
    getX() {
        return this.neurons[0].x;
    }
    getY() {
        return this.neurons[0].y;
    }
    getNbNeurons() {
        return this.size;
    }
    display(canvas, neuralNetwork) {
        for (let i = 0; i < this.size; i++) {
            this.neurons[i].display(canvas);
        }
    }
    createConnectors(indexStep, indexLayer, neuralNetwork) {
        if (this.connectors.length === 0) {
            if (indexLayer > 0) {
                switch (neuralNetwork.orientation) {
                    case Orientation.HORIZONTAL:
                        this.connectors.push(new ConnectorArrow(new Point(this.parentLayer.neurons[this.parentLayer.neurons.length - 1].x, this.parentLayer.neurons[this.parentLayer.neurons.length - 1].y + this.parentLayer.getRadius()), new Point(this.neurons[0].x, this.getY() - this.getRadius())));
                        if (indexLayer < neuralNetwork.nbLayers - 1 && indexStep < neuralNetwork.nbSteps - 1) {
                            const next_nn = neuralNetwork.steps[indexStep + 1][indexLayer];
                            this.connectors.push(new ConnectorArrow(new Point(this.neurons[this.neurons.length - 1].x + this.getRadius(), this.neurons[this.neurons.length - 1].y), new Point(next_nn.neurons[next_nn.neurons.length - 1].x - this.getRadius(), next_nn.neurons[next_nn.neurons.length - 1].y)));
                        }
                        break;
                    case Orientation.VERTICAL:
                        this.connectors.push(new ConnectorArrow(new Point(this.parentLayer.neurons[this.parentLayer.neurons.length - 1].x + this.parentLayer.getRadius(), this.parentLayer.neurons[this.parentLayer.neurons.length - 1].y), new Point(this.neurons[this.neurons.length - 1].x - this.getRadius(), this.neurons[this.neurons.length - 1].y)));
                        if (indexStep < neuralNetwork.nbSteps - 1 && indexLayer < neuralNetwork.nbLayers - 1) {
                            const next_nn = neuralNetwork.steps[indexStep + 1][indexLayer];
                            this.connectors.push(new ConnectorArrow(new Point(this.neurons[0].x, this.neurons[0].y + this.getRadius()), new Point(next_nn.neurons[0].x, next_nn.neurons[0].y - next_nn.getRadius())));
                        }
                        break;
                }
            }
            if (indexLayer === neuralNetwork.nbLayers - 1 && indexStep < neuralNetwork.nbSteps - 1) {
                const cur_nn = neuralNetwork.steps[indexStep][indexLayer];
                const next_nn = neuralNetwork.steps[indexStep + 1][0];
                switch (neuralNetwork.orientation) {
                    case Orientation.HORIZONTAL:
                        this.connectors.push(new ConnectorArrowDash(new Point(cur_nn.neurons[0].x + cur_nn.getRadius() / 3, cur_nn.getY() - cur_nn.getRadius()), new Point(next_nn.neurons[next_nn.neurons.length - 1].x - next_nn.getRadius() / 3, next_nn.neurons[next_nn.neurons.length - 1].y + next_nn.getRadius())));
                        break;
                    case Orientation.VERTICAL:
                        this.connectors.push(new ConnectorArrowDash(new Point(cur_nn.neurons[0].x - cur_nn.getRadius(), cur_nn.getY() + cur_nn.getRadius()), new Point(next_nn.neurons[next_nn.neurons.length - 1].x + next_nn.getRadius(), next_nn.neurons[next_nn.neurons.length - 1].y - next_nn.getRadius())));
                        break;
                }
            }
        }
    }
    activateNeuron(index) {
        index = index % this.neurons.length;
        for (let i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
        this.neurons[index].activate();
    }
    disable() {
        for (let i = 0; i < this.neurons.length; i++) {
            this.neurons[i].disable();
        }
    }
}
