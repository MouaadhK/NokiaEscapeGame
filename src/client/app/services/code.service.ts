import { Injectable } from "@angular/core";
import { Code } from "../../../api/Game";
import { Subject } from "rxjs";

@Injectable()
export class CodeService{

    code: Code = {gameId: "", value: ""};
    modifySubject = new Subject();

    constructor(){
    }

    emitModifySubject(){
        this.modifySubject.next();
    }
}