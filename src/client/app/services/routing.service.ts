import { Injectable } from '@angular/core';
import { RouterModule, ActivatedRoute, Router, Params } from '@angular/router';
import { ModalService } from './modal.service';
import { AppPaths } from '../routing/routing.module';


@Injectable()
export class RoutingService {

  baseUrl = "/";
  isOnNewGameScreen = false;
  isModalOpen = false;
  isSecondLevelModalOpen = false;

  constructor(public router: Router,
              private activatedRoute: ActivatedRoute,
              private modalService: ModalService) { 
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
  }

  
  navigateTo(url: string)
  {
    this.isOnNewGameScreen = (url == AppPaths.newGame);
    this.router.navigateByUrl(this.baseUrl+url, { skipLocationChange: true });
  }

  openModal(target: string, modalData: any = null) {
    this.modalService.data = modalData;
    this.router.navigate([{ outlets: { modal: [target] } }], { skipLocationChange: true });
    this.isModalOpen = true;
  }

  closeModal() {
    this.router.navigate([{ outlets: { modal: null } }], { skipLocationChange: true });
    this.modalService.data = null;
    this.isModalOpen = false
  }

  openSecondLevelModal(target: string, modalData: any = null){
    this.modalService.secondLevelData = modalData;
    this.router.navigate([{ outlets: { secondLevelModal: target } }], { skipLocationChange: true });
    this.isSecondLevelModalOpen = true;
  }

  closeSecondLevelModal(){
    this.modalService.secondLevelData = null;
    this.router.navigate([{ outlets: { secondLevelModal: null } }], { skipLocationChange: true });
    this.isSecondLevelModalOpen = false;
  }

}
