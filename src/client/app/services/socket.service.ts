import { Injectable } from '@angular/core';
import * as SocketIo from 'socket.io-client';
import { SocketEvent } from '../../../api/Sockets';

@Injectable()
export class SocketService {

	private globalSocket: SocketIOClient.Socket;
	private gameSocket: SocketIOClient.Socket;

	constructor() {
		this.globalSocket = SocketIo.connect(); //http://192.168.1.26:3000
		window.addEventListener("beforeunload", (ev) => {
			this.globalSocket.close();
			//console.log("Socket disconnected");
		});
	}


	onReceive(socketEvent: string, callback) {
		return this.globalSocket.on(socketEvent, (data) => {
			// console.log("Received: " + socketEvent);
			callback(data);
		});
	}

	emit(socketEvent: string, ...data) {
		this.globalSocket.emit(socketEvent, ...data);
		// console.log("Event emitted: " + socketEvent);

	}

	getSocketId() {
		return this.globalSocket.id;
	}

	removeListener(socketEvent: string)
	{
		this.globalSocket.removeEventListener(socketEvent);
	}

	getGlobalSocket(){
		return this.globalSocket;
	}

}
