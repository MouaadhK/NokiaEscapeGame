import { Injectable } from '@angular/core';
import { Game, User, UserType, Time, QuizPopup, GameTypes } from '../../../api/Game';
import { SocketService } from '../services/socket.service';
import { SocketEvent, SocketData } from '../../../api/Sockets';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { ReplaySubject } from 'rxjs';

//Modal
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { PauseScreenComponent } from '../pause-screen/pause-screen.component';
import { QuizPopupComponent } from '../quiz-popup/quiz-popup.component';
import { RoutingService } from './routing.service';
import { AppPaths } from '../routing/routing.module';
import { SokobanService } from './sokoban.service';


@Injectable()
export class GameService {

	availableGames = new Map<string, Game>();

	availableGameList: Game[] = [];

	myPlayerProfile = new User();

	currentGameId: string = "";
	private playersInCurrentGame: User[] = [];

	passphraseMask: string = "";

	constructor(private socketService: SocketService,
		private routingService: RoutingService,
		private sokobanService: SokobanService) {
		this.myPlayerProfile.type = UserType.player;

		this.socketService.emit(SocketEvent.getAvailableGames);

		this.socketService.onReceive(SocketEvent.availableGames, (availableGames) => {
			this.updateAvailableGames(availableGames);
		});

		this.socketService.onReceive(SocketEvent.playerList, (players: User[]) => {
			this.updatePlayersInCurrentGame(players);
		});

		this.socketService.onReceive(SocketEvent.timeSync, (remainingTime: Time) => {
			this.getCurrentGame().remainingTime = remainingTime;
		});

		//Game pause
		this.socketService.onReceive(SocketEvent.gamePaused, () => {
			this.showPauseScreen();
		});

		//Game start or resume
		this.socketService.onReceive(SocketEvent.gameStarted, (time: Time) => {
			if(this.getCurrentGame().duration.minutes == this.getCurrentGame().remainingTime.minutes){
				this.routingService.closeModal();
			}
			setTimeout(
				(() => {
					this.routingService.closeSecondLevelModal();
				}).bind(this),100
			);
			setTimeout((this.sokobanService.focusSokoban).bind(this.sokobanService),1000);
		});

		//Game end
		this.socketService.onReceive(SocketEvent.gameEnded, (data: SocketData.GameEndData) => {
			if (!this.isModerator()) {
				if (data.success) {
					this.openSuccessScreen();
				}
				else {
					this.openFailureScreen();
				}
			}
		});

		//QuizPopup Triggered
		this.socketService.onReceive(SocketEvent.triggerQuizPopup, (popup: QuizPopup) => {
			this.quizPopup = popup;
			this.showQuizPopupScreen(this.quizPopup);
		});

		this.socketService.onReceive(SocketEvent.startIntroForEveryone,() => {
			this.routingService.openModal(AppPaths.modalPaths.intro);
		});
	}

	openSuccessScreen() {
		this.routingService.openModal(AppPaths.modalPaths.success);
	}

	openFailureScreen() {
		this.routingService.openModal(AppPaths.modalPaths.gameOver);
	}

	updateAvailableGames(gameList: Game[]) {
		this.availableGameList = this.sortGames(gameList);
		gameList.forEach((game, index) => {
			this.availableGames.set(game.id, game);
		});
	}

	listAvailableGames() {
		return this.availableGameList;
	}


	sortGames(gameList: Game[]): Game[] {
		gameList.sort((game1: Game, game2: Game): number => {
			if (game2.isJoinable && !game1.isJoinable)
				return 1;
			else
				return -1;
		});
		return gameList;
	}

	createNewGame(newGame: Game) {
		newGame.id = this.socketService.getSocketId();
		// newGame.duration.seconds = 0;
		// newGame.remainingTime = new Time(newGame.duration.minutes,newGame.duration.seconds);
		newGame.moderator.id = newGame.id;
		newGame.moderator.name = "Moderator";
		newGame.moderator.type = UserType.moderator;
		newGame.isModeratorConnected = true;
		this.socketService.emit(SocketEvent.createNewGame, newGame);
	}

	defaultGame(): Game {
		let duration = new Time(15, 0);

		return {
			name: "",
			id: "",
			description: "",
			type: GameTypes.Regular,
			status: "Staging",
			maxPlayerCount: 5,
			currentPlayerCount: 0,
			isJoinable: true,
			duration: duration,
			remainingTime: duration,
			isModeratorConnected: false,
			moderator: new User(),
			players: new Map<string,User>()
		};
	}



	updateGameInfo(updatedGame: Game) {
		this.socketService.emit(SocketEvent.updateGameInfo, updatedGame);
	}

	updatePlayersInCurrentGame(players: User[]) {
		this.playersInCurrentGame = players;
	}

	getPlayersInCurrentGame() {
		return this.playersInCurrentGame;
	}

	getCurrentGame() {
		return this.availableGames.get(this.currentGameId);
	}


	printRemainingTime() {
		if (this.getCurrentGame()) {
			return this.formatTime(this.getCurrentGame().remainingTime.minutes, this.getCurrentGame().remainingTime.seconds)
		}
	}

	formatTime(minutes: number, seconds: number){
		let min: string = "",sec: string = "";

		minutes < 10 ? min += '0' + minutes : min += minutes;
		seconds < 10 ? sec += '0' + seconds : sec += seconds;

		return min + ':' + sec;
	}

	showPauseScreen() {
		this.routingService.openSecondLevelModal(AppPaths.modalPaths.pauseGame);
	}


	//quizPopupScreen
	quizPopup: QuizPopup;

	showQuizPopupScreen(popup: QuizPopup) {
		this.routingService.openModal(AppPaths.modalPaths.quizPopup, { quiz: popup });

	}

	hideQuizPopupScreen() {
		this.routingService.closeModal();
	}

	isJuniorGame() {
		return this.getCurrentGame().type === GameTypes.Junior;
	}
  isTeenGame() {
    return this.getCurrentGame().type === GameTypes.Teen;
  }

	isModerator() {
		return this.myPlayerProfile.type === UserType.moderator;
	}
}
