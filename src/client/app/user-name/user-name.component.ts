import { Component, OnInit } from '@angular/core';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';
import { AppPaths } from '../routing/routing.module';

@Component({
  selector: 'app-user-name',
  templateUrl: './user-name.component.html',
  styleUrls: ['./user-name.component.scss']
})
export class UserNameComponent implements OnInit {

  constructor(private gameService: GameService,
    private routingService: RoutingService) { }

  isNameInvalid = false;

  ngOnInit() {
    if (this.gameService.myPlayerProfile.name) {
      this.routingService.navigateTo(AppPaths.gameList);
    }
  }

  onKeyPress(event) {
    if(event.which === 13)
    {
      this.goToGameList();
    }
  }

  goToGameList() {
    if (this.gameService.myPlayerProfile.name) {
      this.isNameInvalid = false;
      this.routingService.navigateTo(AppPaths.gameList);
    }
    else {
      this.isNameInvalid = true;
    }
  }

  checkName()
  {
      this.isNameInvalid = this.gameService.myPlayerProfile.name == '';
  }

}
