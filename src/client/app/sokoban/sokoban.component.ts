import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, Renderer, AfterContentInit } from '@angular/core';
import * as jQuery from "jquery";
import { SokobanService } from '../services/sokoban.service';
import { SokobanLevel, SokobanLevelDifficulty } from '../../../api/Game';
import { GameService } from '../services/game.service';
import { SocketService } from '../services/socket.service';
import { SocketEvent, SocketData } from '../../../api/Sockets';

@Component({
	selector: 'app-sokoban',
	templateUrl: './sokoban.component.html',
	styleUrls: ['./sokoban.component.scss'],
	encapsulation: ViewEncapsulation.None //make the CSS working
})
export class SokobanComponent implements OnInit {
	sokobanLevels: SokobanLevel[] = [
		{
			name: "Level 1",
			difficulty: SokobanLevelDifficulty.Easy,
			passphraseSegment: "",
			index: 0,
			game:
				` #######
 #     #
 # .$. #
## $@$ #
#  .$. #
#      #
########`
		},
		{
			name: "Level 2",
			difficulty: SokobanLevelDifficulty.Medium,
			passphraseSegment: "",
			index: 1,
			game:
				`###### #####
#    ###   #
# $$     #@#
# $ #...   #
#   ########
#####`
		},
		{
			name: "Level 3",
			difficulty: SokobanLevelDifficulty.Hard,
			passphraseSegment: "",
			index: 2,
			game:
				`  ######
  # ..@#
  # $$ #
  ## ###
   # #
   # #
#### #
#    ##
# #   #
#   # #
###   #
  #####`
		},
		{
			name: "Level 4",
			difficulty: SokobanLevelDifficulty.Expert,
			passphraseSegment: "",
			index: 3,
			game:
				`      #####
      #   ##
      # $  #
######## #@##
# .  # $ $  #
#        $# #
#...#####   #
#####   #####`
		}
	];

	currentIndex: number = this.generateRandomInt(0, this.sokobanLevels.length-1); //Exclude last level (expert) from first round
	gameIndex: number = this.currentIndex;
	solved = false;
	isGraphicLoaded: boolean = false;

	constructor(private sokobanService: SokobanService,
		private gameService: GameService,
		private socketService: SocketService,
		private renderer: Renderer) { }

	ngOnInit() {
		this.sokobanService.sokobanSubject.subscribe(
			() => {
				this.nextLevel();
			}
		)
		this.initSokoban();
	}
	initSokoban() {
		this.sokobanService.init(jQuery);
		let _this = this;
		jQuery(function ($) {
			$('pre.sokoban').sokoban();
			_this.isGraphicLoaded = true;
		});
		
	}

	nextLevel() {
		this.solved = true;
		this.announceLevelComplete(this.currentIndex);
		setTimeout(
			(() => {
				this.sokobanLevels.splice(this.currentIndex, 1);
				this.solved = false;
				if (this.sokobanLevels.length > 0) {
					this.currentIndex = this.generateRandomInt(0, this.sokobanLevels.length);
					this.gameIndex = this.sokobanLevels[this.currentIndex].index;
					this.sokobanService.focusSokoban();
				}
				else {
					this.currentIndex = -1;
				}
			}).bind(this), 2000
		)
	}

	announceLevelComplete(index: number) {
		let sokobanLevelData: SocketData.SokobanLevelData = {
			gameId: this.gameService.getCurrentGame().id,
			index: this.sokobanLevels[index].index,
			level: this.sokobanLevels[index]
		}
		this.socketService.emit(SocketEvent.sokobanLevelComplete, sokobanLevelData);
	}

	generateRandomInt(min: number, maxExcl: number) {
		return min + Math.floor(Math.random() * (maxExcl - min));
	}
}
