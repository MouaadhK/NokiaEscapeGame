import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PauseScreenComponent } from './pause-screen.component';

describe('PauseScreenComponent', () => {
  let component: PauseScreenComponent;
  let fixture: ComponentFixture<PauseScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PauseScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PauseScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
