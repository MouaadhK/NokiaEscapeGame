import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pause-screen',
  templateUrl: './pause-screen.component.html',
  styleUrls: ['./pause-screen.component.scss']
})
export class PauseScreenComponent implements OnInit {
  
  constructor() { }

  ngOnInit() {
  }

}
