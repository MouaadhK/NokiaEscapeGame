import { Component, OnInit } from '@angular/core';
import { Game, UserType } from '../../../api/Game';
import { SocketEvent } from '../../../api/Sockets';
import { SocketService } from '../services/socket.service';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';
import * as SocketIo from 'socket.io-client';
import { AppPaths } from '../routing/routing.module';

@Component({
  selector: 'landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.scss']
})
export class LandingpageComponent implements OnInit {

  gameTitle = "Save the Network";
  subtitle = "Nokia Escape Game";

  newGameInfoShown = false; 
  newGame = new Game;

  constructor(private socketService: SocketService,
              private gameService: GameService,
              private routingService: RoutingService) { 
  }

  ngOnInit() {
    this.newGame = this.gameService.defaultGame();  
  }


  hostNewGame() {
    this.routingService.navigateTo(AppPaths.newGame);
  }



  createNewGame()
  {
    this.gameService.createNewGame(this.newGame);
    this.socketService.onReceive(SocketEvent.joinGameAsModerator, 
      (gameId)=>
    {
      this.routingService.navigateTo(AppPaths.moderatorPath);
      this.gameService.currentGameId = gameId;
      this.gameService.myPlayerProfile = this.newGame.moderator;
    });
  }

  goToLandingPage()
  {
    this.routingService.navigateTo(AppPaths.home);
  }

}
