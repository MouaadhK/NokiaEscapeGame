import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewChecked, AfterViewInit } from '@angular/core';
import { Time, GameTypes } from '../../../api/Game';
import { GameService } from '../services/game.service';

@Component({
	selector: 'app-success-screen',
	templateUrl: './success-screen.component.html',
	styleUrls: ['./success-screen.component.scss']
})
export class SuccessScreenComponent implements OnInit, AfterViewInit {

	winnerName: string;
	winTime: Time;
	displayMessage: boolean[] = [true,false,false,false];
	currentIndex = 0;
	hideButton = false;
	isJunior = false;

	@ViewChild("animationEnd") animationEnd: ElementRef;

	constructor(private gameService: GameService,
				private renderer: Renderer2) { 
		this.winTime = Time.prototype.substract(this.gameService.getCurrentGame().duration,this.gameService.getCurrentGame().remainingTime);
		this.isJunior = this.gameService.getCurrentGame().type == GameTypes.Junior;
	}

	ngOnInit() {
		this.incrementAnimationText();
	}

	ngAfterViewInit(){
		this.renderer.listen(this.animationEnd.nativeElement,'animationend',() => {
			this.renderer.addClass(this.animationEnd.nativeElement,'animation-done');
		});
	}

	incrementAnimationText(){
		if(this.currentIndex < this.displayMessage.length-1){
			setTimeout(
				(() => {
					this.displayMessage[++this.currentIndex] = true;
					this.incrementAnimationText();
				}).bind(this),3500
			);
			if(this.currentIndex > 0){
				this.hideCursor(this.currentIndex);
			}
		}
	}

	hideCursor(index: number){
		switch(index){
			case 1:
				document.getElementById('animationEnd2').addEventListener('animationend', () => {
					document.getElementById('animationEnd2').classList.add('animation-done');
				});
				break;
			case 2:
				document.getElementById('animationEnd3').addEventListener('animationend', () => {
					document.getElementById('animationEnd3').classList.add('animation-done');
				});
				break;
		}
		var element = this.isJunior ? document.getElementById('animationEnd4') : document.getElementById('animationEnd3');
		if(element){
			element.addEventListener('animationend', () => {
				this.hideButton = true;
			});
		}
	}

	goToLandingPage() {
		location.reload();
	}
}
