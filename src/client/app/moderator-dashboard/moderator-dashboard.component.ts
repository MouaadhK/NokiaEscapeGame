import { Component, OnInit, TemplateRef } from '@angular/core';
import { SocketService } from '../services/socket.service';
import { SocketEvent } from '../../../api/Sockets';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';
import { GameStatusComponent } from '../game-status/game-status.component';
import { CodeEditorComponent } from '../code-editor/code-editor.component';

//Modal
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { IntroComponent } from '../intro/intro.component';
import { AppPaths } from '../routing/routing.module';

@Component({
	selector: 'moderator-dashboard',
	templateUrl: './moderator-dashboard.component.html',
	styleUrls: ['./moderator-dashboard.component.scss']
})
export class ModeratorDashboardComponent implements OnInit {

	constructor(private gameService: GameService) {
	}

	ngOnInit() {
	}
}

