import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RouterModule, ActivatedRoute, Router } from '@angular/router';
import { RoutingService } from '../services/routing.service';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  exportAs: 'modal',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  constructor( private routingService : RoutingService ) {}

  ngOnInit() {
  }

  closeModal( $event ) {
    this.routingService.closeModal();
  }
}
