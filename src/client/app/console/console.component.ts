import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked, Input } from '@angular/core';
import { ChatMessage, User, Code, CompilationResult, ExecutionLog, SokobanLevel, Time, CheckType } from '../../../api/Game';
import { SocketEvent, SocketData } from '../../../api/Sockets';
import { SocketService } from '../services/socket.service';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';
import { CodeService } from '../services/code.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'console',
	templateUrl: './console.component.html',
	styleUrls: ['./console.component.scss']
})
export class ConsoleComponent implements OnInit{

	@Input() readOnly = false;
	@ViewChild("console") private console: ElementRef;

	shellText: string = "";
	phraseToEncrypt: string = "";
	binaryWasCreated: boolean = false;
	modifySubscription: Subscription;
	inCompileTime: boolean = false;
	onrun: boolean = false;

	constructor(private socketService: SocketService,
		private gameService: GameService,
		private routingService: RoutingService,
		private codeService: CodeService) {
	}

	ngOnInit() {

		this.modifySubscription = this.codeService.modifySubject.subscribe(
			() => {
				this.binaryWasCreated = false;
			}
		)

		if(!this.gameService.isModerator()){
			this.printInConsole(this.gameService.myPlayerProfile.name, "",false);
		}
		
		if(!this.gameService.isModerator()){
			this.socketService.onReceive(SocketEvent.sokobanLevelCompletePlayer, (segment: string) => {
				let phrase: string = "Tu as complété ce niveau! Tu as débloqué le morceau de phrase : <span style='color: #39B54A;'>" + segment +  "</span> ! Réussi les autres niveaux pour découvrir la phrase complète !"
				this.printInConsole(this.gameService.myPlayerProfile.name, phrase);
			});
			this.socketService.onReceive(SocketEvent.fubukiLevelCompletePlayer, (segment: string) => {
				let phrase: string = "Tu as complété ce niveau! Tu as débloqué le morceau de phrase : <span style='color: #39B54A;'>" + segment +  "</span> ! Réussi les autres niveaux pour découvrir la phrase complète !"
				this.printInConsole(this.gameService.myPlayerProfile.name, phrase);
			});
			this.socketService.onReceive(SocketEvent.fubukiLevelFailed, () => {
				let phrase: string = "Mauvaise combinaison ! Essaye encore"
				this.printInConsole(this.gameService.myPlayerProfile.name, phrase);
			})
		}
		else{
			this.socketService.onReceive(SocketEvent.sokobanLevelCompleteModerator, (data: any) => {
				let phrase: string = "Completed the " + data.level.difficulty + " level \"" + data.level.name + "\" and unlocked the passphrase segment \"" + data.segment + "\"." 
				this.printInConsole(data.playerName, phrase);
			})
			this.socketService.onReceive(SocketEvent.fubukiLevelCompleteModerator, (data: any) => {
				let phrase: string = "Completed the " + data.level.difficulty + " level \"" + data.level.name + "\" and unlocked the passphrase segment \"" + data.segment + "\"." 
				this.printInConsole(data.playerName, phrase);
			})
		}

		this.socketService.onReceive(SocketEvent.newPlayerForModerator,(player: User) => {
			let phrase: string = 'Joined the game';
			this.printInConsole(player.name, phrase);
		});

		this.socketService.onReceive(SocketEvent.leavingPlayerForModerator,(player: User) => {
			let phrase: string = 'Left the game';
			this.printInConsole(player.name, phrase);
		});

		this.socketService.onReceive(SocketEvent.wrongPassphrase, () => {
			this.printInConsole(this.gameService.myPlayerProfile.name, "Incorrect passphrase");
		})

		this.socketService.onReceive(SocketEvent.gameEnded, (data: SocketData.GameEndData) => {
			if(data.success){
				let winTime: Time = Time.prototype.substract(this.gameService.getCurrentGame().duration,this.gameService.getCurrentGame().remainingTime);
				let phrase: string = "Victory! Passphrase found in " + winTime.minutes + " minutes and " + winTime.seconds + " seconds!"
				this.printInConsole(data.player.name, phrase);
			}
			else{
				this.printInConsole("All", "Time elapsed... Players disconnecting");
			}
			this.socketService.removeListener(SocketEvent.gameEnded);
		});

		this.socketService.onReceive(SocketEvent.compilationResult, (compilationResult: CompilationResult) => {
			this.inCompileTime = false;
			if(compilationResult.player.name){
				this.printInConsole(compilationResult.player.name, compilationResult.stderr + compilationResult.stdout);
				this.binaryWasCreated = compilationResult.binaryWasCreated;
			}
			else{
				this.binaryWasCreated = compilationResult.binaryWasCreated;
				this.printInConsole(this.gameService.myPlayerProfile.name, compilationResult.stderr + compilationResult.stdout);
			}
		});

		this.socketService.onReceive(SocketEvent.executionLog, (executionLog: ExecutionLog) => {
			// this.printInConsole(execLog.stderr + execLog.stdout);
			if(executionLog.player.name){
				this.printInConsole(executionLog.player.name, executionLog.stderr + executionLog.stdout);
			}
			else{
				this.printInConsole(this.gameService.myPlayerProfile.name, executionLog.stderr + executionLog.stdout);
			}
		});
		this.socketService.onReceive(SocketEvent.consoleReturn,(data: any) => {
      this.printInConsole(this.gameService.myPlayerProfile.name, data );
    });


	}

	scrollConsoleToBottom() {
		this.console.nativeElement.scrollTop = this.console.nativeElement.scrollHeight;
	}

	check(){
		let checkType: CheckType = {
			passphrase: this.phraseToEncrypt,
			gameId: this.gameService.getCurrentGame().id,
			player: this.gameService.myPlayerProfile,
			gameType: this.gameService.getCurrentGame().type
		}
		this.socketService.emit(SocketEvent.checkPassphrase,checkType);
	}

	printInConsole(username: string, text: string, newLine: boolean = true){
		let toPrint: string = "";
		if(this.gameService.isModerator()){
			toPrint = `<span style='color: #007BEA;'> ${username}@Nokia~$ </span>` + text + `<br>`;
		}
		else{
			toPrint = text +  `${newLine ? '<br><br>' : ''}<span style='color: #007BEA;'> ${username}@Nokia~$ </span>`;
		}
		this.console.nativeElement.insertAdjacentHTML('beforeend',toPrint);
		this.scrollConsoleToBottom()
	}

	compile(){
		this.inCompileTime = true;
		this.socketService.emit(SocketEvent.compileCode,this.codeService.code);
	}

	run(){
	  var passphraseNormalize: string = this.phraseToEncrypt.normalize('NFD').replace(/[\u0300-\u036f]/g,"").toLowerCase();
		// console.log(passphraseNormalize);
    this.onrun = true;
		this.socketService.emit(SocketEvent.runCode, passphraseNormalize);
	}
  guess(){
    var passphraseNormalize: string = this.phraseToEncrypt.normalize('NFD').replace(/[\u0300-\u036f]/g,"").toLowerCase();

    this.socketService.emit(SocketEvent.onGuess, passphraseNormalize);

  }
  bloque(champ){
    champ.disabled = true;
    setTimeout(function(){champ.disabled = false; } , 3000);
  }
}

