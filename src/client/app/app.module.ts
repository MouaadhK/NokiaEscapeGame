import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ModalModule } from 'ngx-bootstrap/modal';
//code formatting
import { CodemirrorModule } from 'ng2-codemirror';

//Routing module
import { RoutingModule } from './routing/routing.module';


//Services
import { SocketService } from './services/socket.service';
import { GameService } from './services/game.service';
import { RoutingService } from './services/routing.service';
import { ModalService } from "./services/modal.service";


//Components
import { AppComponent } from './app.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { GamecardComponent } from './gamecard/gamecard.component';
import { DecimalPipe } from '@angular/common';
import { ModeratorDashboardComponent } from './moderator-dashboard/moderator-dashboard.component';
import { PlayerDashboardComponent } from './player-dashboard/player-dashboard.component';
import { GameStatusComponent } from './game-status/game-status.component';
import { ChatBoxComponent } from './chat-box/chat-box.component';
import { CodeEditorComponent } from './code-editor/code-editor.component';
import { ConsoleComponent } from './console/console.component';
import { IntroComponent } from './intro/intro.component';
import { QuizEditorComponent } from './quiz-editor/quiz-editor.component';
import { SuccessScreenComponent } from './success-screen/success-screen.component';
import { FailureScreenComponent } from './failure-screen/failure-screen.component';
import { PauseScreenComponent } from './pause-screen/pause-screen.component';
import { QuizPopupComponent } from './quiz-popup/quiz-popup.component';
import { ModalComponent } from './modal/modal.component';
import { UserNameComponent } from './user-name/user-name.component';
import { GameListComponent } from './game-list/game-list.component';
import { NewGameComponent } from './new-game/new-game.component';
import { DashboardHeaderComponent } from './dashboard-header/dashboard-header.component';
import { GameControlsComponent } from './game-controls/game-controls.component';
import { SokobanComponent } from './sokoban/sokoban.component';
import { SokobanService } from './services/sokoban.service';
import { WaitingScreenComponent } from './waiting-screen/waiting-screen.component';
import { CodeService } from './services/code.service';
import { FubukiComponent } from './fubuki/fubuki.component';



@NgModule({
  declarations: [
    AppComponent,
    LandingpageComponent,
    GamecardComponent,
    ModeratorDashboardComponent,
    PlayerDashboardComponent,
    GameStatusComponent,
    ChatBoxComponent,
    CodeEditorComponent,
    ConsoleComponent,
    IntroComponent,
    QuizEditorComponent,
    SuccessScreenComponent,
    FailureScreenComponent,
    PauseScreenComponent,
    QuizPopupComponent,
    ModalComponent,
    UserNameComponent,
    GameListComponent,
    NewGameComponent,
    DashboardHeaderComponent,
    GameControlsComponent,
    SokobanComponent,
    WaitingScreenComponent,
    FubukiComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RoutingModule,
    CodemirrorModule,
    ModalModule.forRoot()
  ],
  providers: [
    SocketService,
    GameService,
    RoutingService,
    ModalService,
    SokobanService,
    CodeService
  ],
  bootstrap: [AppComponent],
  entryComponents: [IntroComponent, SuccessScreenComponent, FailureScreenComponent, PauseScreenComponent, QuizPopupComponent]
})
export class AppModule { }
