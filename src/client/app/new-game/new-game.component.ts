import { Component, OnInit } from '@angular/core';
import { GameTypes, Game, Time, User } from '../../../api/Game';
import { RoutingService } from "../services/routing.service";
import { GameService } from '../services/game.service';
import { SocketService } from '../services/socket.service';
import { SocketEvent } from '../../../api/Sockets';
import { AppPaths } from '../routing/routing.module';

@Component({
	selector: 'app-new-game',
	templateUrl: './new-game.component.html',
	styleUrls: ['./new-game.component.scss']
})
export class NewGameComponent implements OnInit {

	game = this.gameService.defaultGame();
	

	gameTypes: GameTypes[] = [];
	possibleNbPlayers = [3, 4, 5, 6];
	gameDurationsInMin = [15, 20, 25, 30];
	isTitleInvalid = false;
	minutes: number = 15;

	constructor(private socketService: SocketService,
		private routingService: RoutingService,
		private gameService: GameService) { }

	ngOnInit() {
		for (let type in GameTypes) {
			this.gameTypes.push(<GameTypes>type);
		}
	}

	select(gameType: GameTypes) {
		this.game.type = gameType;
	}

	createNewGame() {
		if(this.game.name){
			this.game.duration.minutes = this.minutes;
			if(typeof(this.game.duration.minutes) == "string"){
				this.game.duration.minutes = parseInt(this.game.duration.minutes);
			}

			this.gameService.createNewGame(this.game);
			this.socketService.onReceive(SocketEvent.joinGameAsModerator,
				(gameId) => {
					this.gameService.currentGameId = gameId;
					this.gameService.myPlayerProfile = this.game.moderator;
					this.gameService.availableGames.set(gameId,this.game);
					this.routingService.navigateTo(AppPaths.moderatorPath);
				}
			);
		}
		else{
			this.isTitleInvalid = true;
		}
	}

	checkTitle() {
		this.isTitleInvalid = this.game.name == '';
	}

	cancel() {
		this.routingService.navigateTo("");
	}
}
