import { Component, OnInit } from '@angular/core';
import { SocketService } from '../services/socket.service';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';

//Modal
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { AppPaths } from '../routing/routing.module';
import { SocketEvent } from '../../../api/Sockets';
import { Time } from '../../../api/Game';

@Component({
	selector: 'app-intro',
	templateUrl: './intro.component.html',
	styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {

	sound = new Audio("../../assets/speech_sokoban.mp3");
	barHeights = [];
	refreshIntervalId: number = 0;
	speechText: string = "Nous sommes en l’an 2050. Une intelligence artificielle nommée Singularity a été mise en place, et contrôle tout sur notre planète. Nous venons d’apprendre que cette intelligence artificielle vient de lancer une rebellion contre les êtres humains. Son objectif: le réseau mondial des communications. Nokia a aujourd’hui réuni des équipes de développeurs qui se sont portés bénévoles pour protéger le réseau de l’attaque. Nous vous confions la responsabilité de protéger tout le réseau européen. Attention, Singularity sait se défendre. Elle risque de vous mettre des bâtons dans les roues pour vous stopper à votre tour. Vous devez déjouer ses pièges pour poursuivre votre mission.";
	speechSegments: string[] = [];
	currentSpeechSegment: string = "";
	currentSegmentIndex: number = 0;


	constructor(private socketService: SocketService,
		private gameService: GameService,
		private routingService: RoutingService) { }

	ngOnInit() {

		this.socketService.onReceive(SocketEvent.gameStarted, (time: Time) => {
			this.sound.pause();
			this.sound.removeEventListener("ended",null);
		});

		this.socketService.onReceive(SocketEvent.gameEnded, () => {
			this.sound.pause();
			this.sound.removeEventListener("ended",null);
		});

		this.sliceSpeech(8);
		this.playIntroSound();
		this.sound.addEventListener("ended", () => {
			this.hide();
		});
	}

	hide() {
		this.sound.pause();
		clearInterval(this.refreshIntervalId);
		this.sound.currentTime = 0;

		if (this.gameService.getCurrentGame().status == "Staging" && !this.gameService.isModerator()) {
			this.routingService.closeModal();
			setTimeout(
				(() => {
					this.routingService.openModal(AppPaths.modalPaths.waitingScreen);
				}).bind(this), 10
			)
		}

		this.routingService.closeModal();
	}

	playIntroSound() {
		this.speech();
		this.sound.load();
		this.sound.playbackRate = 1.1;

		var context = new window.AudioContext();

		var source = context.createMediaElementSource(this.sound);
		var analyser = context.createAnalyser();

		source.connect(analyser);

		source.connect(context.destination);

		analyser.fftSize = 64;

		var frequencyData = new Uint8Array(analyser.frequencyBinCount);
		
		this.refreshIntervalId = setInterval((() => {
			analyser.getByteFrequencyData(frequencyData);
			frequencyData.slice(0, analyser.frequencyBinCount/2 - 4).forEach((value, index) => {
				this.barHeights[index] = value*10.0/255+0.3;
			});
		}).bind(this), 20);

		this.sound.play();
	}

	speech(){
		this.currentSpeechSegment = this.speechSegments[this.currentSegmentIndex++];
		if(this.currentSegmentIndex < this.speechSegments.length){
			setTimeout(this.speech.bind(this),5000);
		}
	}

	sliceSpeech(nbFragment: number){
		let words = this.speechText.split(" ");
		var segmentLength = (words.length*1.0) / nbFragment;
		for(var i = 0;i<nbFragment-1; i++){
			this.speechSegments.push(words.splice(0,Math.round(segmentLength-0.01)).join(' '));
		}
		this.speechSegments.push(words.join(' '));
	}
}
