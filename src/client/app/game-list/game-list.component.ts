import { Component, OnInit } from '@angular/core';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';

@Component({
	selector: 'app-game-list',
	templateUrl: './game-list.component.html',
	styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {

	constructor(private gameService: GameService,
				private routingService: RoutingService) { }

	ngOnInit() {
		if (this.gameService.myPlayerProfile.name == '') {
			this.routingService.navigateTo('/');
		}
	}
}
