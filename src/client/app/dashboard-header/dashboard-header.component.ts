import { Component, OnInit } from '@angular/core';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';
import { AppPaths } from '../routing/routing.module';
import { UserType } from '../../../api/Game';

@Component({
	selector: 'app-dashboard-header',
	templateUrl: './dashboard-header.component.html',
	styleUrls: ['./dashboard-header.component.scss']
})
export class DashboardHeaderComponent implements OnInit {

	volumeOn: boolean = false;
	music = new Audio("../../assets/escape_game_music.mp3");
	isModerator: boolean = false;

	constructor(private gameService: GameService,
		private routingService: RoutingService) { }

	ngOnInit() {
		this.isModerator = this.gameService.myPlayerProfile.type == UserType.moderator;
		this.music.load();
		this.music.loop = true;
	}

	startMusic(){
		this.volumeOn = true;
		this.music.play();
	}

	stopMusic(){
		this.volumeOn = false;
		this.music.pause();
	}

	goToLandingPage() {
		this.routingService.navigateTo(AppPaths.home);
	}

}
