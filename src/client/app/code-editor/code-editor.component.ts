import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked, Input, ViewEncapsulation, HostListener } from '@angular/core';
import { ChatMessage, User, Code, CompilationResult, ExecutionLog, UserType } from '../../../api/Game';
import * as $ from "jquery";
import { SocketEvent } from '../../../api/Sockets';
import { SocketService } from '../services/socket.service';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';
import * as CodeMirror from 'codemirror';
import { CodeService } from '../services/code.service';
import 'codemirror/mode/clike/clike';
// import * as ot from '../../../server/lib/ot/index';

@Component({
	selector: 'code-editor',
	templateUrl: './code-editor.component.html',
	styleUrls: ['./code-editor.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class CodeEditorComponent implements AfterViewChecked {

	@Input() readOnly = false;
	@ViewChild("codeEditor") codeEditor: ElementRef;
	@HostListener('window:resize', ['$event'])
	onresize(event) {
		this.resizeCodeEditor();
	}

	phraseToEncrypt: string = "";
	codeMirrorEditor: CodeMirror.Editor;
	editorConfig: CodeMirror.EditorConfiguration;
	isPlayer: boolean = false;
	readonly: string = "";
	cmClient: any;
	canUndo: boolean = false;
	canRedo: boolean = false;

	constructor(private socketService: SocketService,
		private gameService: GameService,
		private routingService: RoutingService,
		private codeService: CodeService) {
		this.isPlayer = this.gameService.myPlayerProfile.type == UserType.player;
	}

	ngOnInit() {
		this.isPlayer ? this.readonly = "" : this.readonly = "nocursor";

		this.socketService.onReceive('doc', (obj: any) => {
			this.init(obj.str, obj.revision, obj.clients, new ot.SocketIOAdapter(this.socketService.getGlobalSocket()));
			if (this.isPlayer) {
				this.codeService.code.gameId = this.gameService.getCurrentGame().id;
				this.codeService.code.value = obj.str;
			}
			ot.gameService = this.gameService;
			this.resizeCodeEditor();
		})
	}

	ngAfterViewChecked() {
		if (this.codeMirrorEditor)
			this.resizeCodeEditor();
	}

	init(str, revision, clients, serverAdapter) {
		// this.codeMirrorEditor.setValue(str);
		// this.editorConfig = ;
		this.codeMirrorEditor = CodeMirror(this.codeEditor.nativeElement, {
			lineNumbers: true,
			lineWrapping: true,
			readOnly: this.readonly,
			mode: "text/x-c++src",
			theme: 'monokai'
		});
		this.codeMirrorEditor.setValue(str);
		this.cmClient = new ot.EditorClient(
			revision, clients,
			serverAdapter, new ot.CodeMirrorAdapter(this.codeMirrorEditor)
		);

		// var userListWrapper = document.getElementById('userlist-wrapper');
		// userListWrapper.appendChild(cmClient.clientListEl);
		let _this = this;
		this.codeMirrorEditor.on('change', function () {
			_this.codeService.code.value = _this.codeMirrorEditor.getValue();
			_this.cmClient.undoManager.canUndo() ? _this.canUndo = true : _this.canUndo = false;
			_this.cmClient.undoManager.canRedo() ? _this.canRedo = true : _this.canRedo = false;
			_this.codeService.emitModifySubject();
		});
	}

	resizeCodeEditor() {
		let boxHeight = document.getElementsByClassName('gameBox')[0].clientHeight;
		let boxWidth = document.getElementsByClassName('gameBox')[0].clientWidth;
		let controlsHeight = document.getElementsByClassName('controls')[0].clientHeight;
		// console.log(boxHeight, boxWidth, controlsHeight);
		if (this.codeMirrorEditor) { 
			this.codeMirrorEditor.setSize('98%', (boxHeight - controlsHeight - 8)); 
		}
	}

	undo() {
		this.codeMirrorEditor.undo();
	}

	redo() {
		this.codeMirrorEditor.redo();
	}

	setCharAt(str, index, chr) {
		if (index > str.length - 1) return str;
		return str.substr(0, index) + chr + str.substr(index + 1);
	}

}
