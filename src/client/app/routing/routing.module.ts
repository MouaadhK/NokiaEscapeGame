import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ModeratorDashboardComponent } from '../moderator-dashboard/moderator-dashboard.component';
import { PlayerDashboardComponent } from '../player-dashboard/player-dashboard.component';
import { LandingpageComponent } from '../landingpage/landingpage.component';
import { QuizEditorComponent } from '../quiz-editor/quiz-editor.component';
import { IntroComponent } from '../intro/intro.component';
import { SuccessScreenComponent } from '../success-screen/success-screen.component';
import { FailureScreenComponent } from '../failure-screen/failure-screen.component';
import { PauseScreenComponent } from '../pause-screen/pause-screen.component';
import { QuizPopupComponent } from '../quiz-popup/quiz-popup.component';
import { UserNameComponent } from '../user-name/user-name.component';
import { GameListComponent } from '../game-list/game-list.component';
import { NewGameComponent } from '../new-game/new-game.component';
import { WaitingScreenComponent } from '../waiting-screen/waiting-screen.component';

const modalsPath = "modal:";

export const AppPaths = {
	moderatorPath: "dashboard",
	playerPath: "play/",
	quizEditor: "editQuiz/:type",
	home: "",
	gameList: "games",
	newGame: "newGame",
	modalPaths: {
		success: modalsPath + "success",
		gameOver: modalsPath + "gameOver",
		intro: modalsPath + "intro",
		quizPopup: modalsPath + "quizPopup",
		pauseGame: modalsPath + "pauseGame",
		waitingScreen: modalsPath + "waitingScreen"
	}
};

const modalOutlet = 'modal';
const secondLevelModalOutlet = 'secondLevelModal';


const appRoutes: Routes = [
	{
		path: AppPaths.moderatorPath,
		component: ModeratorDashboardComponent
	},
	{
		path: AppPaths.playerPath,
		component: PlayerDashboardComponent
	},
	{
		path: AppPaths.quizEditor,
		component: QuizEditorComponent
	},

	//Modals
	{
		path: AppPaths.modalPaths.intro,
		component: IntroComponent,
		outlet: modalOutlet
	},
	{
		path: AppPaths.modalPaths.success,
		component: SuccessScreenComponent,
		outlet: modalOutlet
	},
	{
		path: AppPaths.modalPaths.gameOver,
		component: FailureScreenComponent,
		outlet: modalOutlet
	},
	{
		path: AppPaths.modalPaths.quizPopup,
		component: QuizPopupComponent,
		outlet: modalOutlet
	},
	{
		path: AppPaths.modalPaths.waitingScreen,
		component: WaitingScreenComponent,
		outlet: modalOutlet
	},
	{
		path: AppPaths.modalPaths.pauseGame,
		component: PauseScreenComponent,
		outlet: secondLevelModalOutlet
	},

	{
		path: AppPaths.home,
		component: LandingpageComponent,
		children: [
			{
				path: AppPaths.gameList,
				component: GameListComponent
			},
			{
				path: AppPaths.newGame,
				component: NewGameComponent
			},
			{
				path: '',
				component: UserNameComponent
			},
			{
				path: "**",
				redirectTo: ''
			}
		]
	},
	{
		path: "**",
		redirectTo: AppPaths.home,
	}
];



@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes)
	],
	exports: [
		RouterModule
	],
	declarations: []
})
export class RoutingModule { }
