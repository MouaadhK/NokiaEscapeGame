import { Component, OnInit } from '@angular/core';
import { SocketService } from '../services/socket.service';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';
import { ChatBoxComponent } from '../chat-box/chat-box.component';
import { GameStatusComponent } from '../game-status/game-status.component';
import { CodeEditorComponent } from '../code-editor/code-editor.component';
import { Game, User, UserType, Time, GameTypes } from '../../../api/Game';
import { SocketEvent, SocketData } from '../../../api/Sockets';
import { AppPaths } from '../routing/routing.module';

//Modal
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { IntroComponent } from '../intro/intro.component';
import { PauseScreenComponent } from '../pause-screen/pause-screen.component';


@Component({
	selector: 'player-dashboard',
	templateUrl: './player-dashboard.component.html',
	styleUrls: ['./player-dashboard.component.scss']
})
export class PlayerDashboardComponent {

	currentGameId: string = "";
	gameJoined = false;

	constructor(private socketService: SocketService,
		private gameService: GameService,
		private routingService: RoutingService,
		private modalService: BsModalService) {
	}

	cancel() {
		this.gameService.currentGameId = "";
		this.routingService.navigateTo("");
	}
}
