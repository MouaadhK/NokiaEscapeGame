import { Component, OnInit } from '@angular/core';
import { SocketService } from '../services/socket.service';
import { SocketEvent } from '../../../api/Sockets';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { AppPaths } from '../routing/routing.module';
import { User } from '../../../api/Game';

@Component({
	selector: 'app-game-controls',
	templateUrl: './game-controls.component.html',
	styleUrls: ['./game-controls.component.scss']
})
export class GameControlsComponent implements OnInit {

	currentGameId: string = '';
	startPauseResume = "START ";
	gamePaused = false;
	gameStarted = false;
	endOfGame = false;

	constructor(private socketService: SocketService,
		private gameService: GameService,
		private routingService: RoutingService,
		private modalService: BsModalService) { }

	ngOnInit() {
		if(!this.gameService.currentGameId)
		{
		  this.routingService.navigateTo('');
		}
		else{
		  this.currentGameId = this.gameService.currentGameId;
		}

		this.socketService.onReceive(SocketEvent.gameEnded, () => {
			this.endOfGame = true;
		});
	}

	action() {
		if (!this.gameStarted || this.gamePaused) {
			this.startResumeGame();
		}
		else {
			this.pauseGame();
		}
	}

	startResumeGame() {
		this.startPauseResume = "PAUSE ";
		this.gameStarted = true;
		this.gamePaused = false;
		this.socketService.emit(SocketEvent.startOrResumeGame, this.currentGameId);
	}

	pauseGame() {
		this.startPauseResume = "RESUME";
		this.gamePaused = true;
		this.socketService.emit(SocketEvent.pauseGame, this.gameService.currentGameId);
	}

	playIntro() {
		this.socketService.emit(SocketEvent.startIntroForEveryone,this.gameService.currentGameId);
	}

	abandon(){
		this.socketService.emit(SocketEvent.abandonGame, this.gameService.currentGameId);
		location.reload();
	}
}
