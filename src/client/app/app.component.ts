import { Component } from '@angular/core';
import { AppPaths } from './routing/routing.module';
import { RoutingService } from './services/routing.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  constructor(private routingService: RoutingService){
  }
}
