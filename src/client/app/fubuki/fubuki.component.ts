import {Component, ElementRef, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import { NeuralNetworkBuilder } from '../../assets/libraries/BYONND/src/neuralNeworkBuilder';
import { Fubuki } from '../../assets/libraries/BYONND/src/fubuki/fubuki';
import { FubukiLevel, FubukiLevelDifficulty} from '../../../api/Game';
import { SocketService } from '../services/socket.service';
import { SocketEvent, SocketData } from '../../../api/Sockets';
import { GameService } from '../services/game.service';

@Component({
	selector: 'app-fubuki',
	templateUrl: './fubuki.component.html',
	styleUrls: ['./fubuki.component.scss']
})
export class FubukiComponent implements OnInit, AfterViewInit {
	@ViewChild('canvas') canvas: any;

	neuralNetworkBuilder: NeuralNetworkBuilder;
	fubuki: any;
	fubukiLevel: FubukiLevel[] = [
		{
			name: 'Level 1',
			difficulty: FubukiLevelDifficulty.Easy,
			passphraseSegment: '',
			index: 0,
			game: [[5, 2, 1], [7, 3, 8] , [6, 9, 4]],
			neuronsToRemove: [
				[0, 0],
				[0, 2],
				[1, 1],
				[2, 0],
				[2, 2]
			]
		},
		{
			name: 'Level 2',
			difficulty: FubukiLevelDifficulty .Medium,
			passphraseSegment: '',
			index: 1,
			game: [[1, 5, 9], [4, 6, 2], [7, 3, 8]],
			neuronsToRemove: [
				[0, 1],
				[1, 0],
				[1, 1],
				[1, 2],
				[2, 1]
			]
		},
		{
			name: 'Level 3',
			difficulty: FubukiLevelDifficulty.Hard,
			passphraseSegment: '',
			index: 2,
			game: [[1, 7, 2], [4, 8, 9], [3, 6, 5]],
			neuronsToRemove: [
				[0, 1],
				[0, 2],
				[1, 0],
				[1, 2],
				[2, 0],
				[2, 1]
			]
		},
		{
			name: 'Level 4',
			difficulty: FubukiLevelDifficulty.Expert,
			passphraseSegment: '',
			index: 3,
			game: [[1, 6, 3], [7, 8, 4], [5, 9, 2]],
			neuronsToRemove: [
				[0, 0],
				[0, 2],
				[1, 0],
				[1, 1],
				[1, 2],
				[2, 0],
				[2, 1],
				[2, 2]
			]
		}
	];

	currentIndex: number = this.generateRandomInt(0, this.fubukiLevel.length - 1);
	solved = false;

	constructor(
		private socketService: SocketService,
		private gameService: GameService) {
		}

	ngOnInit() {
		this.canvas = (<HTMLCanvasElement>this.canvas.nativeElement);
		this.canvas.getContext('2d');
		this.neuralNetworkBuilder = new NeuralNetworkBuilder(this.canvas);
		this.initialiseFubuki();
	}

	ngAfterViewInit(){
	}

	initialiseFubuki(){
		this.fubuki = this.neuralNetworkBuilder.build('fubuki');
		this.fubuki.define([4, 4, 4, 4]);

		for (let i = 0; i < this.fubuki.layers.length; i++) {
			for (let j = 0; j < this.fubuki.layers[i].neurons.length; j++) {
				if (i === this.fubuki.layers.length - 1 || j === this.fubuki.layers[i].neurons.length - 1) {
					this.fubuki.layers[i].neurons[j].activate();
				}
			}
		}
		this.fillFubukiFromLevel();
		this.removeNeuronsValuePerLevel();
		this.fubuki.draw();
	}

	fillFubukiFromLevel() {
		const sumColumn: number[] = [];
		for (let i = 0; i < this.fubuki.layers.length - 1; i++) {
			let sumLine = 0;
			for (let j = 0; j < this.fubuki.layers[i].neurons.length - 1; j++) {
				this.fubuki.layers[i].neurons[j].setValue(this.fubukiLevel[this.currentIndex].game[i][j]);
				sumLine += this.fubukiLevel[this.currentIndex].game[i][j];
				if (i == 0)
					sumColumn[j] = this.fubukiLevel[this.currentIndex].game[i][j];
				else
					sumColumn[j] += this.fubukiLevel[this.currentIndex].game[i][j];
			}
			this.fubuki.layers[i].neurons[this.fubuki.layers[i].neurons.length - 1].setValue(sumLine);
		}

		for (let i = 0; i < sumColumn.length; i++){
			this.fubuki.layers[this.fubuki.layers.length - 1].neurons[i].setValue(sumColumn[i]);
		}
	}

	removeNeuronsValuePerLevel(){
		for (let i = 0; i < this.fubukiLevel[this.currentIndex].neuronsToRemove.length; i++){
			const lineIndex = this.fubukiLevel[this.currentIndex].neuronsToRemove[i][0];
			const columnIndex = this.fubukiLevel[this.currentIndex].neuronsToRemove[i][1];
			const currentNeuron = this.fubuki.layers[lineIndex].neurons[columnIndex];
			currentNeuron.setValue('');
			currentNeuron.setReadOnly(false);
		}
	}

	checkResult(){
		const sumColumn: number[] = [];
		let resultOk = true;
		const useNumber = new Set();
		for (let i = 0; i < this.fubuki.layers.length - 1; i++) {
			let sumLine = 0;
			for (let j = 0; j < this.fubuki.layers[i].neurons.length - 1; j++) {
				sumLine += this.fubuki.layers[i].neurons[j].value;
        useNumber.add(this.fubuki.layers[i].neurons[j].value);
				if (i == 0)
					sumColumn[j] =  this.fubuki.layers[i].neurons[j].value;
				else
					sumColumn[j] +=  this.fubuki.layers[i].neurons[j].value;
			}

			if (this.fubuki.layers[i].neurons[this.fubuki.layers[i].neurons.length - 1].value !== sumLine){
				resultOk = false;
				break;
			}
		}

		for (let i = 0; i < sumColumn.length; i++){
			if (this.fubuki.layers[this.fubuki.layers.length - 1].neurons[i].value !== sumColumn[i]){
				resultOk = false;
				break;
			}
		}
    if (useNumber.size !== 9){
      resultOk = false;
    }
		if (! resultOk){
			this.removeNeuronsValuePerLevel();
			this.socketService.emit(SocketEvent.fubukiLevelFailed);
		}
		else{
			this.announceLevelComplete(this.currentIndex);
		}
	}

	announceLevelComplete(index: number){
		const fubukiLevelData: SocketData.FubukiLevelData = {
			gameId: this.gameService.getCurrentGame().id,
			index: this.fubukiLevel[index].index,
			level: this.fubukiLevel[index]
		};
		this.socketService.emit(SocketEvent.fubukiLevelComplete, fubukiLevelData);
		this.solved = true;
		setTimeout(
			(() => {
				this.fubukiLevel.splice(this.currentIndex, 1);
				this.solved = false;
				if (this.fubukiLevel.length > 0) {
					this.currentIndex = this.generateRandomInt(0, this.fubukiLevel.length);
					this.initialiseFubuki();
				}
				else {
					this.currentIndex = -1;
				}
			}).bind(this), 2000;
	}

	generateRandomInt(min: number, maxExcl: number) {
		return min + Math.floor(Math.random() * (maxExcl - min));
	}

}
