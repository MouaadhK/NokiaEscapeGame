import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FubukiComponent } from './fubuki.component';

describe('FubukiComponent', () => {
  let component: FubukiComponent;
  let fixture: ComponentFixture<FubukiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FubukiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FubukiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
