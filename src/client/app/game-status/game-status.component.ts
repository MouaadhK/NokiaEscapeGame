import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Game, User, UserType, Time, PlayerStatus } from '../../../api/Game';
import { SocketEvent, SocketData } from '../../../api/Sockets';
import { SocketService } from '../services/socket.service';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';
import * as p5 from 'p5';

let _this: GameStatusComponent;

@Component({
	selector: 'game-status',
	templateUrl: './game-status.component.html',
	styleUrls: ['./game-status.component.scss']
})

export class GameStatusComponent implements OnInit {

	@ViewChild("test") test: ElementRef;
	_this = this;
	static firstPlayer: boolean = false;
	playerName: string = "";

	players: PlayerStatus[] = [];

	constructor(private socketService: SocketService,
		private gameService: GameService) {
	}

	ngOnInit() {
		this.socketService.onReceive(SocketEvent.newPlayerForModerator, (player: User) => {
			this.playerName = player.name;
			if (!GameStatusComponent.firstPlayer) {
				var playerStatus: PlayerStatus = {name: this.playerName,x: this.generateRandomInt(0,700),y: this.generateRandomInt(0,500), color: 'white'};
				this.players.push(playerStatus);
				this.createCanvas();
			}
			else{
				var playerStatus: PlayerStatus = {name: this.playerName,x: this.generateRandomInt(0,700),y: this.generateRandomInt(0,500), color: 'white'};
				this.players.push(playerStatus);
			}
		});

		this.socketService.onReceive(SocketEvent.leavingPlayerForModerator,(player: User) => {
			for(var i = 0;i<this.players.length;i++){
				if(this.players[i].name == player.name){
					this.players[i].color = 'red';
				}
			}
		});
	}

	generateRandomInt(min: number, maxExcl: number) {
        return min + Math.floor(Math.random() * (maxExcl - min));
    }

	private createCanvas = () => {
		GameStatusComponent.firstPlayer = true;
		this.p5 = new p5(this.playerBox);
	}

	private playerBox = function (p: any) {
		var windowW = 799
		var windowH = 507;
		var font;
		var textString = _this.playerName;
		var x = p.random(10, 500);
		var y = p.random(10, 700);
		var c = p.color(255,0,0);

		function preload() {
			font = p.loadFont('');
		}

		p.setup = () => {
			var canvas = p.createCanvas(windowW, windowH);
			canvas.parent("test");
			p.frameRate(30);
			p.noStroke();
			p.textSize(16);
		}

		p.draw = () => {
			for(var i = 0;i<_this.players.length;i++){
				p.fill(_this.players[i].color);
				p.rect(_this.players[i].x,_this.players[i].y,125,35);
				p.fill(0);
				p.text(_this.players[i].name, _this.players[i].x, _this.players[i].y, 125, 35);

				if(_this.players.length > 1){
					for(var j = 0;j<_this.players.length;j++){
						if(_this.players[j] != _this.players[i]){
							p.stroke(255);
							p.line(_this.players[i].x,_this.players[i].y,_this.players[j].x,_this.players[j].y);
						}
					}
				}
			}
		}
	}
}
