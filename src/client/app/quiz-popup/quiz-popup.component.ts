import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SocketService } from '../services/socket.service';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';
import { Game, User, UserType, Time, QuizPopup, QuizQuestion, QuizAnswer } from '../../../api/Game';

import { ModalService } from '../services/modal.service';
import { AppPaths } from '../routing/routing.module';


@Component({
	selector: 'app-quiz-popup',
	templateUrl: './quiz-popup.component.html',
	styleUrls: ['./quiz-popup.component.scss']
})
export class QuizPopupComponent implements OnInit {

	currentQuestionIndex: number = 0;
	score: number = 0;
	questions: QuizQuestion[] = [];
	success = false;

	afterQuizTimeOut = 3000;

	constructor(private routingService: RoutingService,
		private modalService: ModalService) {
	}


	ngOnInit() {
		if(this.modalService.data){
			this.questions = this.modalService.data.quiz;
		}
		
	}

	choseAnswer(answer: QuizAnswer) {
		console.log("CLICKED")
		if (answer.isCorrect) {
			this.score++;
		}
		this.currentQuestionIndex++;

		if (this.currentQuestionIndex == this.questions.length) {
			if (this.score >= Math.ceil(2 * this.questions.length / 3)) {
				this.success = true;
				setTimeout(this.continueGame.bind(this), this.afterQuizTimeOut);
			}
			else {
				this.success = false;
				setTimeout(this.goToFailureScreen.bind(this), this.afterQuizTimeOut);
			}
		}
	}

	continueGame() {
		this.routingService.closeModal();
	}

	goToFailureScreen() {
		this.routingService.closeModal();
		setTimeout(
			(() => {
				this.routingService.openModal(AppPaths.modalPaths.gameOver);
			}).bind(this),10
		);
	}

}
