import { Component, OnInit, Input } from '@angular/core';
import { QuizPopup, QuizQuestion, QuizAnswer, QuizPopupType } from '../../../api/Game';
import { SocketEvent } from '../../../api/Sockets';
import { SocketService } from '../services/socket.service';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'quiz-editor',
	templateUrl: './quiz-editor.component.html',
	styleUrls: ['./quiz-editor.component.scss']
})
export class QuizEditorComponent implements OnInit {

	popups: QuizPopup[] = [];
	popupsText: string;
	type: QuizPopupType;

	constructor(private socketService: SocketService,
				private route: ActivatedRoute) 
				{	}

	ngOnInit() {
		let activatedRoutes = new ActivatedRoute();
		this.route.params.subscribe(params => {
			this.type = params["type"];
			console.log(params);
			console.log(this.type);
			this.socketService.emit(SocketEvent.getQuizPopups, this.type);
			this.socketService.onReceive(SocketEvent.quizPopups, (popups: QuizPopup[]) => {
				this.popups = popups;
				console.log(this.popups);
			});
		})
	}

	addPopup() {
		let newPopup: QuizQuestion[] = [];
		this.popups.push(newPopup);
	}

	addQuestion(popup: QuizPopup) {
		popup.push(new QuizQuestion());
	}

	addAnswer(question: QuizQuestion) {
		question.answers.push(new QuizAnswer());
	}

	deletePopup(index: number) {
		this.popups.splice(index, 1);
	}

	deleteQuestion(popup: QuizPopup, index: number) {
		popup.splice(index, 1);
	}

	deleteAnswer(question: QuizQuestion, index: number) {
		question.answers.splice(index, 1);
	}

	updatePopups(){
		try{
			this.popups = JSON.parse(this.popupsText);
			this.popupsText = JSON.stringify(this.popups,null,4);
		}
		catch(e){}
	}

	save() {
		this.socketService.emit(SocketEvent.updateQuizPopups, this.type, this.popups);
	}

}
