import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { Http } from '@angular/http';
import { ChatMessage, User, UserType } from '../../../api/Game';
import { SocketEvent } from '../../../api/Sockets';
import { SocketService } from '../services/socket.service';
import { GameService } from '../services/game.service';

@Component({
	selector: 'chat-box',
	templateUrl: './chat-box.component.html',
	styleUrls: ['./chat-box.component.scss']
})
export class ChatBoxComponent implements OnInit, AfterViewChecked {

	messages: ChatMessage[] = [];
	newMessage = new ChatMessage();

	@ViewChild("discussion") private discussion: ElementRef;

	constructor(private socketService: SocketService,
		private gameService: GameService,
		private http: Http) { }

	ngOnInit() {
		this.newMessage.content = "";
		this.newMessage.sender = this.gameService.myPlayerProfile;
		this.newMessage.gameId = this.gameService.currentGameId;
		this.socketService.onReceive(SocketEvent.newChatMessage, (message: ChatMessage) => {
			this.displayMessage(message);
		});
	}

	ngAfterViewChecked() {
		//this.scrollMessagesToBottom(); 
	}

	sendMessage() {
		this.newMessage.time = new Date();

		if (this.newMessage.content.trim().length > 0) {
			this.displayMessage(JSON.parse(JSON.stringify(this.newMessage)));
			this.socketService.emit(SocketEvent.newChatMessage, this.newMessage);
			if(!this.gameService.isModerator()){
				// this.sendToSingularity(this.newMessage.content);
			}
			this.newMessage.content = "";
		}
	}

	isFromMe(message: ChatMessage) {
		return message.sender.id == this.socketService.getSocketId();
		//this.scrollMessagesToBottom();
	}

	isFromModerator(message: ChatMessage) {
		return message.sender.type == UserType.moderator;
		//this.scrollMessagesToBottom();
	}


	scrollMessagesToBottom() {
		this.discussion.nativeElement.scrollTop = this.discussion.nativeElement.scrollHeight;
	}

	sendToSingularity(message: string) {
		message = this.replaceAll(message, ' ', '+');
		message = this.replaceAll(message, '%20', '+');

		this.socketService.emit(SocketEvent.newChatMessage, message);

		this.http.get('http://139.54.131.105:8000/internal?service=SMARTCHAT&text=' + message).
		 	map((res) => res.text())
		 	.subscribe(response => {
		 		let chatMsg: ChatMessage = {
		 			gameId: this.gameService.currentGameId,
		 			content: response.toString(),
		 			sender: { name: "Singularity", id: "", email: "", type: 1 },
		 			time: new Date()
		 		};
		 		this.displayMessage(chatMsg);
		 	});
	}
	
	replaceAll(str: string, search, replacement) {
		var target = str;
		return target.replace(new RegExp(search, 'g'), replacement);
	}

	displayMessage(chatMsg)
	{
		this.messages.push(chatMsg);
		setTimeout((this.scrollMessagesToBottom).bind(this),200);
	}
}
