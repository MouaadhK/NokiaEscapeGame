import { Component, OnInit, Input } from '@angular/core';
import { Game, User, UserType, Time } from '../../../api/Game';
import { SocketEvent, SocketData } from '../../../api/Sockets';
import { SocketService } from '../services/socket.service';
import { GameService } from '../services/game.service';
import { RoutingService } from '../services/routing.service';
import { AppPaths } from '../routing/routing.module';

@Component({
	selector: 'gamecard',
	templateUrl: './gamecard.component.html',
	styleUrls: ['./gamecard.component.scss']
})
export class GamecardComponent {

	@Input() game: Game;
	gameJoined = false;

	constructor(private socketService: SocketService,
		private gameService: GameService,
		private routingService: RoutingService) {
	}

	joinGame(gameId) {
		let joinReqData: SocketData.JoinGameAsPlayerReqData = {
			gameId: "",
			player: new User()
		};
		joinReqData.gameId = gameId;
		this.gameService.myPlayerProfile.id = this.socketService.getSocketId();
		joinReqData.player = this.gameService.myPlayerProfile;
		this.socketService.emit(SocketEvent.joinGameAsPlayerReq, joinReqData);
		let joinGameAsPlayerResp = this.socketService.onReceive(SocketEvent.joinGameAsPlayerResp,
			(result: SocketData.JoinGameAsPlayerRespData) => {
				if (result.gameJoined) {
					this.gameService.passphraseMask = result.passphraseMask;
					this.gameService.currentGameId = gameId;
					this.routingService.navigateTo(AppPaths.playerPath);

					setTimeout(
						(() => {
							this.routingService.openModal(AppPaths.modalPaths.waitingScreen);
						}).bind(this), 10
					);
				}
				else {
					alert("Game could not be joined!");
				}
				joinGameAsPlayerResp.removeListener(SocketEvent.joinGameAsPlayerResp);
				this.gameJoined = result.gameJoined;
			});
	}


	timeToString(time: Time) {
		let t = new Time(time.minutes, time.seconds);
		return t.toString();
	}

}
