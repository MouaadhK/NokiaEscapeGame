import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FailureScreenComponent } from './failure-screen.component';

describe('FailureScreenComponent', () => {
  let component: FailureScreenComponent;
  let fixture: ComponentFixture<FailureScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FailureScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FailureScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
