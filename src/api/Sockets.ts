import { User, SokobanLevel, FubukiLevel } from './Game';

export const SocketEvent = 
{
    getAvailableGames: "getAvailableGames",
    createNewGame: "createNewGame",
    joinGameAsPlayerReq: "joinGameAsPlayerReq",
    joinGameAsPlayerResp: "joinGameAsPlayerResp",
    availableGames: "availableGames",
    joinGameAsModerator: "joinGameAsModerator",
    updateGameInfo: "updateGameInfo",
    getPlayerListInGame: "getPlayerListInGame",
    playerList: "updatePlayerList",
    newChatMessage: "newChatMessage",
    getCodeFileContent: "getCodeFileContent",
    codeFileContent: "codeFileContent",
    compileCode: "compileCode",
    compilationResult: "compilationResult",
    runCode: "runCode",
    consoleReturn: "consoleReturn",
    onGuess: "onGuess",
    executionLog: "executionLog",
    startOrResumeGame: "startOrResumeGame",
    gameStarted: "gameStarted",
    pauseGame: "pauseGame",
    gamePaused: "gamePaused",
    abandonGame: "abandonGame",
    timeSync: "timeSync",
    getQuizPopups: "getQuizPopups",
    updateQuizPopups: "updateQuizPopups",
    quizPopups: "quizPopups",
    triggerQuizPopup: "triggerQuizPopup",
    gameEnded: "gameEnded",
    sokobanLevelComplete: "sokobanLevelComplete",
    sokobanLevelCompletePlayer: "sokobanLevelCompletePlayer",
    sokobanLevelCompleteModerator: "sokobanLevelCompleteModerator",
    fubukiLevelComplete: "fubukiLevelComplete",
    fubukiLevelCompletePlayer: "fubukiLevelCompletePlayer",
    fubukiLevelCompleteModerator: "fubukiLevelCompleteModerator",
    fubukiLevelFailed: "fubukiLevelFailed",
    newPlayerForModerator: "newPlayerForModerator",
    leavingPlayerForModerator: "leavingPlayerForModerator",
    checkPassphrase: "checkPassphrase",
    wrongPassphrase: "wrongPassphrase",
    startIntroForEveryone: "startIntroForEveryone"
}

export namespace SocketData
{
    export class JoinGameAsPlayerRespData { 
        gameJoined: boolean = false;
        passphraseMask: string = "";
    }
    export class JoinGameAsPlayerReqData { 
        gameId: string = ""; 
        player: User
    }

    export class GameEndData {
        player: User;
        success: boolean = false;
    }

    export class SokobanLevelData{
        gameId: string = "";
        index: number = 0;
        level: SokobanLevel;
    }


    export class FubukiLevelData{
        gameId: string="";
        index: number = 0;
        level: FubukiLevel;
    }
}


