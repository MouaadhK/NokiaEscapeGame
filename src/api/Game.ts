export class Time {
    minutes: number = 0;
    seconds: number = 0;

    constructor(minutes: number, seconds: number) {
        let secondsCycle = 60;
        this.minutes = minutes + Math.floor(seconds / secondsCycle);
        this.seconds = (seconds % secondsCycle + secondsCycle) % secondsCycle;
    }

    toString(): string {
        let formattedMinutes: string = "";
        let formattedSeconds: string = "";

        formattedMinutes = this.minutes < 10 ? '0' + this.minutes : '' + this.minutes;
        formattedSeconds = this.seconds < 10 ? '0' + this.seconds : '' + this.seconds;

        return formattedMinutes + ":" + formattedSeconds;
    }


    static decrementByOneSecond(time: Time) {
        time.seconds--;
        if (time.seconds == -1) {
            time.seconds = 59;
            time.minutes--;
        }
    }

    valueOf()
    {
        return (this.minutes * 60 + this.seconds);
    }

    substract(time1: Time, time2: Time){
        return new Time(time1.minutes-time2.minutes,time1.seconds-time2.seconds);
        //time1.minutes > time2.minutes ?  
    }
};

export class Game {
    name: string = "";
    id: string = "";
    description?: string = "";
    type: GameTypes;
    status: string = "";
    maxPlayerCount: number = 0;
    currentPlayerCount: number = 0;
    isJoinable: boolean = false;
    remainingTime?: Time;
    duration?: Time;
    isModeratorConnected?: boolean = false;
    moderator?: User;
    players: Map<string,User>;
};

export enum GameTypes {
    Junior = "Junior",
    Regular = "Regular",
    Teen ="Teen"
}


export enum UserType {
    moderator,
    player
}

export class User {
    name: string;
    email: string;
    id: string;
    type: UserType;

    constructor() {
        return {
            name: "",
            email: "",
            id: "",
            type: UserType.player
        }
    }
}

export class ChatMessage {
    gameId: string = "";
    sender: User;
    time: Date;
    content: string = "";
}

export class Code {
    gameId: string = "";
    value: string = "";
}

export class CompilationResult {
    gameId: string = "";
    player: User;
    binaryWasCreated: boolean = false;
    stdout: string = "";
    stderr: string = "";

    constructor() {
        this.gameId = "";
        this.player = new User();
        this.binaryWasCreated = false;
        this.stderr = "";
        this.stdout = "";
    }
}

export class ExecutionLog {
    gameId: string = "";
    player: User;
    stderr: string = "";
    stdout: string = "";
}


export class QuizAnswer {
    answer: string = "";
    isCorrect: boolean = false;

    constructor()
    {
        this.answer = "";
        this.isCorrect = false;
    }
}

export class QuizQuestion {
    question: string = "";
    answers: Array<QuizAnswer>;

    constructor() {
        this.question = "";
        this.answers = [];
    }
}

export type QuizPopup = Array<QuizQuestion>;

export class QuizPopupAssignment {
    player: User; 
    popup: QuizPopup
}

export enum QuizPopupType{
    Regular = "regular",
    Junior = "junior",
    Teen= "junior"
}

export enum SokobanLevelDifficulty {
    Easy = "Easy",
    Medium = "Medium",
    Hard = "Hard" ,
    Expert = "Expert"
}

export class SokobanLevel {
    name: string = "";
    difficulty: SokobanLevelDifficulty;
    game: string = "";
    passphraseSegment: string = "";
    index: number = 0;
}
export enum FubukiLevelDifficulty {
    Easy = "Easy",
    Medium = "Medium",
    Hard = "Hard" ,
    Expert = "Expert"
}
export class FubukiLevel {
    name: string = "";
    difficulty: FubukiLevelDifficulty;
    passphraseSegment: string = "";
    index: number = 0;
    game: number[][];
    neuronsToRemove: number[][];
}

export class PlayerStatus {
    x: number = 0;
    y: number = 0;
    name: string = "";
    color: string = "";
}

export class CheckType{
    passphrase: string = "";
    player: User = new User();
    gameId: string = "";
    gameType: GameTypes;
}
