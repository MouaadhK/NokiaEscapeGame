import * as http from 'http';
import App from './app';
import * as SocketIo from 'socket.io';
import { SocketEvent, SocketData } from '../api/Sockets';
import { GameManager } from './game.manager';
import { Game, User, UserType, ChatMessage, Code, CompilationResult, ExecutionLog, Time, QuizPopup, QuizPopupAssignment, SokobanLevel, CheckType, QuizPopupType, GameTypes } from '../api/Game';
import * as fs from 'fs';
import * as path from 'path';
import { exec } from 'child_process';
import { ChatService } from './chat-service';
import * as shareCodeMirror from 'share-codemirror';
import {Stream} from 'stream';
import {removeAllListeners} from 'cluster';
// import { SocketIO } from 'socket.io';


const port = 8080;
App.set('port', port);

const server = http.createServer(App);
server.listen(port);
const io = SocketIo(server);
let gameManager = new GameManager();
let chatService = new ChatService();

//Timestamp all logs
const consoleLog = console.log;
console.log = function (...args) {
    consoleLog(`${new Date().toUTCString()}: `, ...args);
}

try {
    io.on("connection", newConnection);
    console.log("Server started on port " + port);
} catch (error) {
    console.log(error);
}




function newConnection(socket: SocketIO.Socket) {
    broadcastAvailableGames();
    console.log("New connection: " + socket.id);

    socket.on("error", (error) => {
        console.log('Socket error: ', error);
    });

    //Manage users leaving a game
    socket.on("disconnecting", (data) => {
        for (let gameId in socket.rooms) {
            if (socket.rooms.hasOwnProperty(gameId)) {
                if (socket.id == gameId) {
                    let game = gameManager.getGame(gameId);
                    if (game) {
                        console.log("Moderator leaving game " + gameId);
                        game.isModeratorConnected = false;
                    }
                }
                else {
                    let player = gameManager.getPlayerInGame(gameId, socket.id);
                    if (player) {
                        console.log(`Player ${player.name} (${socket.id}) leaving ${gameId}`);
                        gameManager.removePlayerFromGame(gameId, player.id);
                        broadcastPlayerListInGame(gameId);
                        notifyModeratorOfPlayerLeaving(gameId, player);
                    }
                    else {
                        console.log(`Player ${socket.id} leaving ${gameId}`);
                    }
                }
                if (gameManager.isEmptyGame(gameId)) {
                    console.log("Removing empty game " + gameId);
                    gameManager.removeGame(gameId);
                }
                broadcastAvailableGames();
            }
        }
    });

    //Request list of available games
    socket.on(SocketEvent.getAvailableGames, () => {
        socket.emit(SocketEvent.availableGames, gameManager.getAvailableGameList());
    });

    //Request list of connected players in a game
    socket.on(SocketEvent.getPlayerListInGame, (gameId: string) => {
        broadcastPlayerListInGame(gameId);
    });

    //Create New Game event
    socket.on(SocketEvent.createNewGame, (game: Game) => {
        if (game) {
            console.log("New game created: " + game.name + " : " + game.id);
            gameManager.addNewGame(game);
            socket.join(game.id);
            socket.emit(SocketEvent.joinGameAsModerator, game.id);
            gameManager.registerSharedEditorClient("Moderator", game.id, socket);
            broadcastAvailableGames();
        }
    });

    //Join existing game
    socket.on(SocketEvent.joinGameAsPlayerReq, (joinReqData: SocketData.JoinGameAsPlayerReqData) => {
        if (joinReqData) {
            let gameId = joinReqData.gameId;
            let player = joinReqData.player;
            let result: SocketData.JoinGameAsPlayerRespData = { gameJoined: false, passphraseMask: "" };
            if (!io.sockets.adapter.rooms[gameId]) {
                socket.emit(SocketEvent.joinGameAsPlayerResp, result);
                console.log("Can't Join game " + gameId);
            }
            else {
                socket.join(gameId, (err) => {
                    if (err) {
                        socket.emit(SocketEvent.joinGameAsPlayerResp, result);
                        console.log("Can't Join game " + gameId);
                    }
                    else {
                        let game = gameManager.getGame(gameId);
                        if (game.currentPlayerCount < game.maxPlayerCount && game.isJoinable) {
                            result.gameJoined = true;
                            result.passphraseMask = gameManager.getMask(gameManager.passphrase[game.type]);
                            socket.emit(SocketEvent.joinGameAsPlayerResp, result);
                            console.log(player, " joined Game " + gameId);
                            gameManager.addPlayerInGame(gameId, player);
                            broadcastPlayerListInGame(gameId);
                            broadcastAvailableGames();
                            notifyModeratorOfPlayerJoining(gameId, player);
                            gameManager.registerSharedEditorClient(player.name, game.id, socket);
                        }
                        else {
                            socket.emit(SocketEvent.joinGameAsPlayerResp, result);
                            console.log(player, " couldn't Join game " + gameId);
                        }
                    }
                });
            }
        }
    });

    //Start or resume game 
    socket.on(SocketEvent.startOrResumeGame, (gameId: string) => {
        let game = gameManager.getGame(gameId);
        if (game) {
            game.status = "Ongoing";
            game.isJoinable = false;

            if (!gameManager.areGameQuizPopupsReady(gameId)) {
                let type = game.type == GameTypes.Regular ? QuizPopupType.Regular : QuizPopupType.Junior;
                gameManager.prepareGameQuizPopups(type, gameId);
            }

            broadcastToPlayersInGame(gameId, SocketEvent.gameStarted, null);

            broadcastAvailableGames();
            gameManager.countDown(gameId, (remainingTime: Time) => {
                broadcastToUsersInGame(gameId, SocketEvent.timeSync, remainingTime);

                //Check for game end
                if (remainingTime && remainingTime.minutes == 0 && remainingTime.seconds == 0) {
                    let data: SocketData.GameEndData = {
                        player: new User(),
                        success: false
                    }
                    broadcastToUsersInGame(gameId, SocketEvent.gameEnded, data);
                    console.log(`Game ${game.name} (${gameId}) timed out`);
                    gameManager.pauseGame(gameId);
                }

                //check for pupop trigger and send if any
                let popupAssignment = gameManager.getPopupScheduledForNowInGame(gameId, remainingTime);
                if (popupAssignment && popupAssignment.player) {
                    let playerSocket = io.sockets.connected[popupAssignment.player.id];
                    if (playerSocket) {
                        playerSocket.emit(SocketEvent.triggerQuizPopup,
                            popupAssignment.popup);
                    }

                }

            });
        }
    });

    //Pause game
    socket.on(SocketEvent.pauseGame, (gameId: string) => {
        gameManager.pauseGame(gameId);
        broadcastToPlayersInGame(gameId, SocketEvent.gamePaused, null);
    });

    //New Chat Message sent
    socket.on(SocketEvent.newChatMessage, (receivedMsg: ChatMessage) => {
        if (receivedMsg && receivedMsg.sender){
            let moderatorSocket = io.sockets.connected[receivedMsg.gameId];
            if (socket.id != receivedMsg.gameId){
                //player
                // chatService.sendMessage(receivedMsg.gameId,receivedMsg.content, (responseMsg: ChatMessage) => {
                //     socket.emit(SocketEvent.newChatMessage,responseMsg);
                // });

                if (moderatorSocket) {
                    moderatorSocket.emit(SocketEvent.newChatMessage, receivedMsg);
                }
            }
            else{
                receivedMsg.sender.name = "Singularity";
                receivedMsg.sender.type = UserType.player;
                broadcastToPlayersInGame(receivedMsg.gameId, SocketEvent.newChatMessage, receivedMsg);
            }
        }
    });

    //Update game info
    socket.on(SocketEvent.updateGameInfo, (updatedGame: Game) => {
        if (gameManager.updateGame(updatedGame)) {
            broadcastAvailableGames();
        }
    });

    //get Code from file 
    socket.on(SocketEvent.getCodeFileContent, () => {
        let codeContent = gameManager.getCodeFileContent();
        socket.emit(SocketEvent.codeFileContent, codeContent);
    });

    //Compile code 
    socket.on(SocketEvent.compileCode, (code: Code) => {
        if (code) {
            gameManager.CompileCode(code.gameId, socket.id, code.value, (compilationResult: CompilationResult) => {
                let currentPlayer = gameManager.getPlayerInGame(code.gameId, socket.id);
                compilationResult.player = currentPlayer;
                socket.emit(SocketEvent.compilationResult, compilationResult);
                let moderatorSocket = io.sockets.connected[code.gameId];
                if (moderatorSocket) {
                    moderatorSocket.emit(SocketEvent.compilationResult, compilationResult);
                }
                socket.removeAllListeners(SocketEvent.runCode);
                let onRunCode = socket.on(SocketEvent.runCode, (arg: string) => {

                    let quotedArg = "\"" + arg.trim() + "\"";

                    console.log("Code run with argument: " + arg.trim());

                    let moderatorSocket = io.sockets.connected[code.gameId];
                    let execRes = new ExecutionLog();
                    execRes.gameId = code.gameId;
                    execRes.player = currentPlayer;
                    execRes.stdout = "";
      //              gameManager.runCode(arg.trim(), code.gameId, socket.id, (output: string) => {

                    gameManager.runCode(quotedArg, code.gameId, socket.id, (output: string) => {
                        let game = gameManager.getGame(code.gameId);
                        if(!game) return;

                        if (gameManager.codeExecutionOutputIsCorrect(game.type, output, arg)) {
                            let gameEndData: SocketData.GameEndData = { player: currentPlayer, success: true };
                            gameManager.pauseGame(code.gameId);
                            broadcastToUsersInGame(code.gameId, SocketEvent.gameEnded, gameEndData);
                            console.log(`Game ${code.gameId} ended with success`);
                        }
                        else {
                            execRes.stderr = "Incorrect code or passphrase, try again";
                            socket.emit(SocketEvent.executionLog, execRes);

                            if (moderatorSocket) {
                                execRes.stderr = "Code ran with passphase: " + arg.trim();
                                moderatorSocket.emit(SocketEvent.executionLog, execRes);
                            }
                        }
                    });
                  socket.on(SocketEvent.onGuess, (arg: string) => {
                    gameManager.child.stdin.write(arg+'\n');
                    gameManager.child.stdout.once("data", (data) => {
                      socket.emit( 'consoleReturn', data.toString());
                    });
                    gameManager.child.on('exit', function (codes) {
                      if (codes === 0) {
                        let gameEndData: SocketData.GameEndData = { player: currentPlayer, success: true };
                        gameManager.pauseGame(code.gameId);
                        broadcastToUsersInGame(code.gameId, SocketEvent.gameEnded, gameEndData);
                      } else if (codes===(4)) {
                        let gameEndData: SocketData.GameEndData = { player: currentPlayer, success: false };
                        gameManager.pauseGame(code.gameId);
                        broadcastToUsersInGame(code.gameId, SocketEvent.gameEnded, gameEndData);
                        gameManager.child.stdin.end();
                      }
                      console.log('return:'+ codes);
                    });
                    gameManager.child.on('close', (code) => console.log('Exit code: ' + code));
                    /*  var stdinStream = new Stream.PassThrough();
                    stdinStream.write(arg+'\0');// Add data to the internal queue for users of the stream to consume
                    stdinStream.pipe(gameManager.child.stdin);
                    stdinStream = new Stream.PassThrough();
                    stdinStream.write(arg+'\0');// Add data to the internal queue for users of the stream to consume
                    stdinStream.pipe(gameManager.child.stdin);
                    gameManager.child.stdin.*/


                    console.log('Guess button and arg value : ' + arg );}
                  );
                });
            });

        }
    });

    // QUIZ EDITION
    //get Quiz popups for quiz editor 
    socket.on(SocketEvent.getQuizPopups, (type: QuizPopupType) => {
        socket.emit(SocketEvent.quizPopups, gameManager.getQuizPopups(type));
    });

    //update Quiz popups from quiz editor
    socket.on(SocketEvent.updateQuizPopups, (type: QuizPopupType, popups: QuizPopup[]) => {
        if (popups) {
            gameManager.updateQuizPopups(type, popups);
            socket.emit(SocketEvent.quizPopups, gameManager.getQuizPopups(type));
        }
    });

    socket.on(SocketEvent.abandonGame, (gameId: string) => {
        if (gameId) {
            let data: SocketData.GameEndData = {
                player: new User(),
                success: false
            }
            broadcastToUsersInGame(gameId, SocketEvent.gameEnded, data);
            gameManager.abandonGame(gameId);
            console.log(`Game ${gameId} abandoned by moderator`);
            broadcastAvailableGames();
        }
    });

    socket.on(SocketEvent.sokobanLevelComplete, (sokobanLevelData: SocketData.SokobanLevelData) => {
        if (sokobanLevelData) {
            let moderatorSocket = io.sockets.connected[sokobanLevelData.gameId];
            let segment = gameManager.passphraseSegments[sokobanLevelData.index];
            socket.emit(SocketEvent.sokobanLevelCompletePlayer, segment);
            if (moderatorSocket) {
                let currentPlayer = gameManager.getPlayerInGame(sokobanLevelData.gameId, socket.id);
                moderatorSocket.emit(SocketEvent.sokobanLevelCompleteModerator, {
                    level: sokobanLevelData.level,
                    playerName: currentPlayer ? currentPlayer.name : "",
                    segment: segment
                });
            }
        }
    });

    socket.on(SocketEvent.fubukiLevelComplete, (fubukiLevelData: SocketData.FubukiLevelData) => {
        let moderatorSocket = io.sockets.connected[fubukiLevelData.gameId];
        let segment = gameManager.passphraseSegments[fubukiLevelData.index];
        socket.emit(SocketEvent.fubukiLevelCompletePlayer, segment);
        if (moderatorSocket) {
            let currentPlayer = gameManager.getPlayerInGame(fubukiLevelData.gameId, socket.id);
            moderatorSocket.emit(SocketEvent.fubukiLevelCompleteModerator, {
                level: fubukiLevelData.level,
                playerName: currentPlayer ? currentPlayer.name : "",
                segment: segment
            });
        }
    })

    socket.on(SocketEvent.fubukiLevelFailed, () => {
        socket.emit(SocketEvent.fubukiLevelFailed);
    })

    socket.on(SocketEvent.checkPassphrase, (data: CheckType) => {
        if (data) {
            let submittedPassphrase: string = data.passphrase ? gameManager.normalize(data.passphrase).toLowerCase().trim() : "";
            let correctPassphrase: string = gameManager.passphrase[data.gameType];
            let normalizedCorrectPassphrase: string = correctPassphrase ? gameManager.normalize(correctPassphrase).toLowerCase() : "";
            
            if (submittedPassphrase === normalizedCorrectPassphrase) {
                gameManager.pauseGame(data.gameId);
                let gameEndData: SocketData.GameEndData = new SocketData.GameEndData();
                gameEndData.player = data.player;
                gameEndData.success = true;
                broadcastToUsersInGame(data.gameId, SocketEvent.gameEnded, gameEndData);
                console.log(`Game ${data.gameId} ended with success`);
            }
            else {
                socket.emit(SocketEvent.wrongPassphrase);
            }
        }
    });

    socket.on(SocketEvent.startIntroForEveryone, (gameId: string) => {
        broadcastToUsersInGame(gameId, SocketEvent.startIntroForEveryone, {});
    });
}

function broadcastAvailableGames() {
    broadcastToAllUsers(SocketEvent.availableGames, gameManager.getAvailableGameList());
}

function broadcastPlayerListInGame(gameId: string) {
    broadcastToUsersInGame(gameId, SocketEvent.playerList, gameManager.getPlayerListInGame(gameId));
}


function broadcastToAllUsers(event: string, data) {
    io.emit(event, data);
}

function broadcastToPlayersInGame(gameId: string, event: string, ...data) {
    let game = gameManager.getGame(gameId);
    if (game && game.players) {
        Object.keys(game.players).forEach((playerId: string) => {
            let playerSocket = io.sockets.connected[playerId];
            if (playerSocket) {
                playerSocket.emit(event, ...data);
            }
        })
    }
}

function broadcastToUsersInGame(gameId: string, event: string, ...data) {
    let moderatorSocket = io.sockets.connected[gameId];
    if (moderatorSocket) {
        moderatorSocket.emit(event, ...data);
    }
    broadcastToPlayersInGame(gameId, event, ...data);
}

function notifyModeratorOfPlayerJoining(gameId: string, player: User) {
    let moderatorSocket = io.sockets.connected[gameId];
    if (moderatorSocket) {
        moderatorSocket.emit(SocketEvent.newPlayerForModerator, player);
    }
}

function notifyModeratorOfPlayerLeaving(gameId: string, player: User) {
    let moderatorSocket = io.sockets.connected[gameId];
    if (moderatorSocket) {
        moderatorSocket.emit(SocketEvent.leavingPlayerForModerator, player);
    }

}
