import * as http from 'http';
import { ChatMessage } from "../api/Game";

export class ChatService {

    constructor() {
    }

    chatbotUrl = "http://localhost:8003";

    replaceAll(str: string, search, replacement) {
        var target = str;
        return target.replace(new RegExp(search, 'g'), replacement);
    }

    sendMessage(gameId: string, message: string, listener: Function) {
        //format the user input
        message = this.replaceAll(message, ' ', '+');
        message = this.replaceAll(message, '%20', '+');

        try{
            http.get(this.chatbotUrl +'/smartbot?service=SMARTBOT&text=' + message, 
            incoming => {
                let data = '';
                incoming.on('data', (d) => {
                    data+=d;
                });
    
                incoming.on('end', () => {
                    let responseMsg: ChatMessage = {
                        gameId: gameId,
                        content: data.toString(),
                        sender: { name: "Singularity", id: "", email: "", type: 1 },
                        time: new Date()
                    };
                    listener(responseMsg);
                });
            });
        }
        catch(error){
            console.log(error)
        }
    }
}