import * as path from 'path';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as serveStatic from 'serve-static';

// Creates and configures an ExpressJS web server.
class App {


  // ref to Express instance
  public express: express.Application;

  //Run configuration methods on the Express instance.
  constructor() {
    this.express = express();
    this.middleware();
    this.routes();
  }

  // Configure Express middleware.
  private middleware(): void {
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }

  // Configure API endpoints.
  private routes(): void {

    // Point static path to dist
    this.express.use(express.static(path.join(__dirname, "..", "dist")));

    //enable CORS for all
    this.express.use(function (req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    });

    let router = express.Router();
    this.express.get('*', (req, res) => {
      res.sendFile(path.join(__dirname, "..", "dist", 'index.html'));

    });
    this.express.use('/', router);
  }
}

export default new App().express;