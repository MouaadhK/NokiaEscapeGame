import * as ot from './lib/ot/index';
import { Socket } from 'net';

export class SharedEditor {
    socketIOServer: any;
    code: string = ""

    constructor(code: string, gameId: string) {
        this.code = code;
        this.socketIOServer = new ot.EditorSocketIOServer(this.code, [], gameId, function (socket, cb) {
            cb(true);
        });
    }

    addClient(socket: SocketIO.Socket, username: string) {
        this.socketIOServer.addClient(socket);
        this.socketIOServer.setName(socket, username);
    }
};
