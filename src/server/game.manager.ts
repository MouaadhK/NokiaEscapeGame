import {CompilationResult, Game, GameTypes, QuizPopup, QuizPopupAssignment, QuizPopupType, Time, User} from '../api/Game';
import * as fs from 'fs';
import * as path from 'path';
import * as md5 from 'js-md5';
import {exec, spawn} from 'child_process';
import * as storage from 'node-persist';
import {ncp} from 'ncp';
import * as os from 'os';
import {SharedEditor} from './shared-editing.service';

export class GameManager {
    availableGames = new Map<string, Game>();
    connectedPlayers = new Map<string, Map<string, User>>();
    sharedEditors = new Map<string, SharedEditor>();
    fileManager = new GameFilesManager();

    gameTimers = new Map<string, any>();

    quizPopupsStorageKey = "quizPopus";

    gameQuizPopups = new Map<string, Map<number, QuizPopupAssignment>>();

    passphrase = new Map<GameTypes, string>();

    child = spawn("ls");

    constructor() {
        storage.initSync({ dir: path.join(__dirname, "..", "storage") });
        this.passphrase[GameTypes.Junior] = "Nous créons la technologie pour connecter le monde";
        this.passphrase[GameTypes.Regular] = "We create the technology to connect the world";
        this.passphrase[GameTypes.Teen] = "32";
        //this.passphrase[GamesTypes.Teen]=Math.random() * (100)
        this.passphraseSegments = this.slicePassphrase(this.passphrase[GameTypes.Junior], this.numberOfLevels);
    }
    numberOfLevels = 4;
    passphraseSegments: string[] = [];

    getAvailableGameList() {
        return Array.from(this.availableGames.values());
    }

    addNewGame(game: Game) {
        if (game && game.id && game.type) {
            this.availableGames.set(game.id, game);
            let passphrase = this.passphrase[game.type] ? this.passphrase[game.type] : "";
          if(game.type === GameTypes.Regular){
            this.fileManager.codeDir = path.join(__dirname, "..", "md5Code");
            this.fileManager.codeFileRelativePath = path.join("md5.hpp");}
            if(game.type === GameTypes.Teen){
            this.fileManager.codeDir = path.join(__dirname, "..", "guessanumberCode");
            this.fileManager.codeFileRelativePath = path.join("guessanumber.cpp");}
            if (game.type !== GameTypes.Junior) {
                this.fileManager.createGameDir(game.id);
                this.sharedEditors[game.id] = new SharedEditor(this.getCodeFileContent(), game.id);
            }
        }
    }

    registerSharedEditorClient(name: string, gameId: string, socket: SocketIO.Socket) {
        let sharedEditor: SharedEditor = this.sharedEditors[gameId]
        if (sharedEditor) {
            sharedEditor.addClient(socket, name);
        }
    }

    updateGame(updatedGame: Game): boolean {
        if (updatedGame && updatedGame.id) {
            if (this.availableGames.has(updatedGame.id)) {
                this.availableGames.set(updatedGame.id, updatedGame);
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }

    decrementPlayerCountInGame(gameId: string) {
        let updatedGame = this.availableGames.get(gameId);
        if (updatedGame) {
            if (updatedGame.currentPlayerCount > 0) {
                updatedGame.currentPlayerCount--;
            }
            updatedGame.isJoinable = (updatedGame.currentPlayerCount < updatedGame.maxPlayerCount) &&
                updatedGame.status == "Staging";
            this.updateGame(updatedGame);
        }
        else {
            console.log("Game " + gameId + " not found!");

        }

    }

    incrementPlayerCountInGame(gameId: string) {
        let updatedGame = this.availableGames.get(gameId);
        if (updatedGame) {
            updatedGame.currentPlayerCount++;

            updatedGame.isJoinable = (updatedGame.currentPlayerCount < updatedGame.maxPlayerCount);

            this.updateGame(updatedGame);
        }
        else {
            console.log("Game " + gameId + " not found!");

        }

    }

    removeGame(gameId: string) {
        let game = this.getGame(gameId);
        if (game && game.type !== GameTypes.Junior) {
            this.fileManager.removeGameDir(gameId);
            this.sharedEditors.delete(gameId);
        }
        return this.availableGames.delete(gameId);
    }

    getGame(gameId: string) {
        return this.availableGames.get(gameId);
    }


    isEmptyGame(gameId) {
        let game = this.availableGames.get(gameId);
        if (game) {
            return game.currentPlayerCount == 0 && game.isModeratorConnected == false;
        }
        else {
            return false;
        }
    }

    addPlayerInGame(gameId: string, player: User) {
        let gamePlayers = this.connectedPlayers.get(gameId);
        if (!gamePlayers) {
            gamePlayers = new Map<string, User>();
        }
        if (player.id) {
            gamePlayers.set(player.id, player);
            this.connectedPlayers.set(gameId, gamePlayers)
            this.incrementPlayerCountInGame(gameId);

            let game = this.getGame(gameId);
            if (game) {
                // game.players = game.players ? game.players : [];
                game.players[player.id] = player;
            }

        }
    }

    getPlayerInGame(gameId: string, userId: string): User {
        return this.connectedPlayers.get(gameId).get(userId);
    }

    removePlayerFromGame(gameId: string, userId: string) {
        let game = this.getGame(gameId);
        if (game && game.players) {
            let player = this.getPlayerInGame(gameId, userId)
            let found = this.connectedPlayers.get(gameId).delete(userId);
            delete game.players[userId];
            if (found) {
                this.decrementPlayerCountInGame(gameId);
            }
            return found;
        }
    }

    getPlayerListInGame(gameId: string) {
        let playersInGame = this.connectedPlayers.get(gameId);
        if (playersInGame) {
            return Array.from(playersInGame.values());
        }
        else {
            return [];
        }
    }

    pauseGame(gameId: string) {
        clearInterval(this.gameTimers.get(gameId));
    }

    abandonGame(gameId: string) {
        this.pauseGame(gameId);
        this.removeGame(gameId);
    }

    countDown(gameId: string, callback: Function) {
        let interval = setInterval(() => {
            let game = this.availableGames.get(gameId);
            if (game) {
                Time.decrementByOneSecond(game.remainingTime);
                callback(this.availableGames.get(gameId).remainingTime);
            }
        }, 1000);
        this.gameTimers.set(gameId, interval);
    }

    stripCompilationLog(log)
    {
        log = log.replace(/((?:(?:^.*?)|(?:))\.\/)/, '');
        log = log.replace(/(?:\^)(\s)/, '\n');
        return log;
    }

    CompileCode(gameId: string, playerId: string, content: string, listener: Function) {
        let playerDirPath = this.fileManager.createPlayerDirInGame(gameId, playerId, (err) => {
            this.fileManager.writeCodeToFile(playerDirPath, content);
            exec("sh compile.sh", { cwd: playerDirPath }, (err, stdout, stderr) => {
                let compilationResult = new CompilationResult();

                stdout = this.stripCompilationLog(stdout);
                stderr = this.stripCompilationLog(stderr);

                compilationResult = {
                    gameId: gameId,
                    player: null,
                    binaryWasCreated: !(err || stderr),
                    stderr: stderr,
                    stdout: (err || stderr) ? "" : "Compiled successfully"
                }
                if (listener && typeof listener == "function") {
                    listener(compilationResult);
                }

            });
        });
    }

    getCodeFileContent() {
        return this.fileManager.getCodeFileContent();
    }

    runCode(arg: string, gameId: string, playerId: string, listener: Function) {
        let cmd = "";
        os.platform() == "win32" ? cmd = "encrypt.exe " : cmd = "./encrypt ";
        let playerDirPath = path.join(this.fileManager.basePath, gameId, playerId);
        this.child = spawn('./encrypt',[],{ cwd: playerDirPath }); //TODO platform detection not working in linux
    }

    codeExecutionOutputIsCorrect(gameType: GameTypes, output: string, quote: string) {
        let passphrase = this.passphrase[gameType] ? this.passphrase[gameType] : "";
        return output == md5(this.normalize(passphrase));
    }

    getQuizPopups(type: QuizPopupType): QuizPopup[] {
        let key = this.quizPopupsStorageKey + type;
        let popups = storage.getItemSync(key);
        return popups ? popups : [];
    }

    updateQuizPopups(type: QuizPopupType, popups: QuizPopup[]) {
        let key = this.quizPopupsStorageKey + type;
        storage.setItemSync(key, popups);
    }

    prepareGameQuizPopups(type: QuizPopupType, gameId: string) {
        let players = this.getPlayerListInGame(gameId);
        let popups: QuizPopup[] = this.getQuizPopups(type);
        let gameDuration = new Time(this.getGame(gameId).duration.minutes, this.getGame(gameId).duration.seconds);
        let quizAssignments = new Map<number, QuizPopupAssignment>();

        for (let player of players) {
            let randomPopupIndex = this.generateRandomInt(0, popups.length);
            let randomPopupTime;
            do {
                //@todo: exclude first 2 and last 5 mins of the game from the range
              if(this.availableGames.get(gameId).type === GameTypes.Teen){
                randomPopupTime = this.generateRandomInt((60 * 1), gameDuration.valueOf() - (60 * 2));
              }
              else{
                randomPopupTime = this.generateRandomInt((60 * 8), gameDuration.valueOf() - (60 * 2));}
            }
            while (quizAssignments.has(randomPopupTime))
            quizAssignments[randomPopupTime] = { player: player, popup: popups[randomPopupIndex] };

            popups.splice(randomPopupIndex, 1);
        }
        this.gameQuizPopups[gameId] = quizAssignments;
    }

    getPopupScheduledForNowInGame(gameId: string, now: Time) {
        let nowTime = new Time(now.minutes, now.seconds);
        return this.gameQuizPopups[gameId][nowTime.valueOf()];
    }

    areGameQuizPopupsReady(gameId: string) {
        return this.gameQuizPopups.has(gameId);
    }

    generateRandomInt(min: number, maxExcl: number) {
        return min + Math.floor(Math.random() * (maxExcl - min));
    }

    normalize(phrase: string) {
        return phrase.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().trim();
    }

    getMask(phrase: string) {
        let ret: string = '';
        for (let i: number = 0; i < phrase.length; i++) {
            if (phrase.charAt(i) == ' ') {
                ret += ' ';
            }
            else {
                ret += '_';
            }
        }
        return ret;
    }

    slicePassphrase(passphrase: string, nbFragment: number) {
        var passphraseSegments = [];
        if (passphrase) {
            let words = passphrase.split(" ");
            var segmentLength = (words.length * 1.0) / nbFragment;
            for (var i = 0; i < nbFragment - 1; i++) {
                passphraseSegments.push(words.splice(0, Math.round(segmentLength - 0.01)).join(' '));
            }
            passphraseSegments.push(words.join(' '));

        }
        return passphraseSegments;
    }

};

class GameFilesManager {

    basePath: string = path.join(__dirname, "..", "gameData");

    codeDir = path.join(__dirname, "..", "md5Code");
    codeFileRelativePath = path.join("md5.hpp");

    createGameDir(gameId: string) {
        let dirPath = path.join(this.basePath, gameId);
        fs.mkdir(dirPath, (err) => {
            if (err)
                console.log(err);
            else
                console.log(dirPath + " created");

        });
    }

    createPlayerDirInGame(gameId: string, playerId: string, callback: (err: Error) => void) {
        let playerDirPath = path.join(this.basePath, gameId, playerId);
        if (!fs.existsSync(playerDirPath)) {
            fs.mkdirSync(playerDirPath);
        }
        ncp(this.codeDir, playerDirPath, { clobber: false }, callback);
        return playerDirPath;
    }

    getCodeFileContent() {
        let codeContent: string;
        return fs.readFileSync(path.join(this.codeDir, this.codeFileRelativePath)).toString().replace(/(\r)/gm, "");
    }

    writeCodeToFile(playerDirPath: string, content: string) {
        let filePath = path.join(playerDirPath, this.codeFileRelativePath);
        fs.writeFileSync(filePath, content);
    }

    removeGameDir(gameId: string) {
        console.log("Removing game: " + gameId + " from " + this.basePath);
        exec("rm -Rf " + gameId, { cwd: this.basePath }, (err, stdout, stderr) => {
            console.log(err + stderr + stdout);
        });
    }
}
