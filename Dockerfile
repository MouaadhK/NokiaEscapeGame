FROM node:carbon

#Labeling the docker image
LABEL version="4.1"
LABEL description="Nokia Escape Game Docker Image - Second Release"
LABEL maintainer "aracely.pacheco@nokia.com"

# Create app directory
RUN mkdir -p /NokiaEscapeGame
WORKDIR /NokiaEscapeGame

#Define Proxy (if necessary)
#ENV HTTP_PROXY "http://135.245.192.7:8000"
#ENV HTTPS_PROXY "http://135.245.192.7:8000"

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)

COPY . .
RUN npm config set registry https://registry.npmjs.org/
RUN npm config set proxy http://135.245.192.7:8000
RUN npm install 
ENV PATH /NokiaEscapeGame/node_modules/.bin:$PATH
ENV NODE_PATH /NokiaEscapeGame/node_modules/:$NODE_PATH
RUN ng build --prod --aot false 
RUN tsc

#Correct the WebSocket library (avoiding reconnections)
ENV AUX "${s}this.on('error', function(a,b) {console.error('[WebSocket Error\!]',a,b)})"
RUN sed -i "/this.extensions = {}/a \    $AUX" /NokiaEscapeGame/node_modules/ws/lib/WebSocket.js

#Lunch the application
EXPOSE 8080
CMD [ "node", "deploy/server/index.js" ]

